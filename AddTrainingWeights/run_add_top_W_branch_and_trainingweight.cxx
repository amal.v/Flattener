#include "add_top_W_branch_and_trainingweight.cxx"

void parseDir(TString dirname);

void run_add_top_W_branch_and_trainingweight(){

  // Signal
  TString input_dir_name_signal_top = "/atlas/data2/userdata/eakilli/Grid_output_MC15c_JETM8_Tagging_p2613_2july_flat/Reweighed_in_10GeV/Zprime";
  TString input_dir_name_signal_W = "/atlas/data2/userdata/eakilli/Grid_output_MC15c_JETM8_Tagging_p2613_2july_flat/Reweighed_in_10GeV/Wprime";
  // Background
  TString input_dir_name_background = "/atlas/data2/userdata/eakilli/Grid_output_MC15c_JETM8_Tagging_p2613_2july_flat/Reweighed_in_10GeV/Background";


  parseDir(input_dir_name_signal_W);
  //parseDir(input_dir_name_background);
  //parseDir(input_dir_name_signal_top);

}


void parseDir(TString dirname){

  TString output_dir_name = "/atlas/data2/userdata/eakilli/agile_file/new_2july_reweighed/Reweighed_in_10GeV/";
  TString output_name;

  cout << "Operating in " << dirname << endl;
  TString file_directory = dirname;

  TSystemDirectory dir(dirname, dirname);
  TList *files = dir.GetListOfFiles();

 if (files) {

    cout << "Found files." << endl;
    TSystemFile *file ;
    TString input_file_name;
    Int_t flag = 0;

    TIter next(files);

    while ( (file=(TSystemFile*)next() )) {

      input_file_name = file->GetName();
      if (input_file_name.Contains("root")) flag = 1;
      if (flag != 1) continue;

      output_name = input_file_name;
      cout << "Processing file: " << input_file_name << endl;
      add_top_W_branch_and_trainingweight(dirname+"/"+input_file_name,output_dir_name+output_name);

     }
  }
}
