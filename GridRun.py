import os

filelist = ['user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.merge.dumperout_290417_tree.root',
            'user.avaidya.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.merge.dumperout_290417_tree.root']



for i in range(len(filelist)):

    file = filelist[i]

    number = 10*i
    if number == 0:
        number = "00"

    command = "GridRun " + str(number) + " " + file
    os.system("rm -rf submitDir")
    print command
    os.system(command)
    i += 1
