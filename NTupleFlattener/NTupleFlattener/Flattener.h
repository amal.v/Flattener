#ifndef NTupleFlattener_Flattener_H
#define NTupleFlattener_Flattener_H

#include <EventLoop/Algorithm.h>
#include <EventLoop/OutputStream.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoopAlgs/NTupleSvc.h>

#include <TTree.h>
#include <TFile.h>
#include <TString.h>
#include <string>
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"

//#include "ShowerDeconstruction/ShowerDeconstruction.h"

class Flattener : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  int m_eventCounter; //!

//  std::string outfilename = "test_output";

  std::string jet_signal_flavor;
  int i_jet_signal_flavor;

  std::vector< TH2D* > h2_CaloResolution_map;
  std::vector< TH2D* > h2_TAresolution_map;
  std::vector< TH2D* > h2_TAbinned_correlation_map;

  std::vector< TH2D* > h2_CaloResolution_mapWprime;
  std::vector< TH2D* > h2_TAresolution_mapWprime;
  std::vector< TH2D* > h2_TAbinned_correlation_mapWprime;

  std::string str_top =  "top";
  std::string str_W = "W";
  //std::string str_background = "background";

  std::string str_background_jz0w = "00";
  std::string str_background_jz1w = "10";
  std::string str_background_jz2w = "20";
  std::string str_background_jz3w = "30";
  std::string str_background_jz4w = "40";
  std::string str_background_jz5w = "50";
  std::string str_background_jz6w = "60";
  std::string str_background_jz7w = "70";
  std::string str_background_jz8w = "80";
  std::string str_background_jz9w = "90";
  std::string str_background_jz10w = "100";
  std::string str_background_jz11w = "110";
  std::string str_background_jz12w = "120";

  // Values from the table here: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JetEtmissMC15#Pythia8_dijet
  
  float JZ0W_xsec = 7.8420e+07;
  float JZ1W_xsec = 7.8420e+07;
  float JZ2W_xsec = 2.4334E+06;
  float JZ3W_xsec = 2.6454E+04;
  float JZ4W_xsec = 2.5464E+02;
  float JZ5W_xsec = 4.5536E+00;
  float JZ6W_xsec = 2.5752E-01;
  float JZ7W_xsec = 1.6214E-02;
  float JZ8W_xsec = 6.2505E-04;
  float JZ9W_xsec = 1.9640E-05;
  float JZ10W_xsec = 0.0000011962;
  float JZ11W_xsec = 0.000000042258;
  float JZ12W_xsec = 0.0000000010367;

  float JZ0W_filteff = 1.0240E+00;
  float JZ1W_filteff = 6.7198E-04;
  float JZ2W_filteff = 3.3264E-04;
  float JZ3W_filteff = 3.1953E-04;
  float JZ4W_filteff = 5.3009E-04;
  float JZ5W_filteff = 9.2325E-04;
  float JZ6W_filteff = 9.4016E-04;
  float JZ7W_filteff = 3.9282E-04;
  float JZ8W_filteff = 1.0162E-02;
  float JZ9W_filteff = 1.2054E-02;
  float JZ10W_filteff =  5.8935E-03;
  float JZ11W_filteff =  2.7015E-03;
  float JZ12W_filteff = 4.2502E-04;

  // Total number of processed events (not weighted for Pythia, Herwig dijet)
  float JZ0W_numevents = 1.9994e+06;
  float JZ1W_numevents = 1.999e+06;
  float JZ2W_numevents = 1.9946e+06;
  float JZ3W_numevents = 7.8845e+06;
  float JZ4W_numevents = 7.9798e+06;
  float JZ5W_numevents = 7.9776e+06;
  float JZ6W_numevents = 1.8934e+06;
  float JZ7W_numevents = 1.7702e+06;
  float JZ8W_numevents = 1.7432e+06;
  float JZ9W_numevents = 1.8132e+06;
  float JZ10W_numevents = 1995999;
  float JZ11W_numevents = 1993199;
  float JZ12W_numevents = 1974598;

  //TTree* treein; //!
  TFile *outputFile;//!
  TTree *treeout; //!

  // Define pT, eta cuts
  bool apply_pt_eta_cuts = true;
  bool pass_pt_eta_cuts = false;
  // Pre-recommendation values
  double min_pt = 200;
  double max_pt = 2500;
  double max_abs_eta = 2;

  // Use Shower Deconstruction or not
  bool m_shower_deconstruction = false;
  bool m_saved_clusters = false;
  bool m_saved_trackjets = false;

  /*
  ShowerDeconstruction *sd1; //!
  ShowerDeconstruction *sd2; //!
  ShowerDeconstruction *sd3; //!
  ShowerDeconstruction *sd4; //!
  */

  // some branches might be missing in the input n-tuple
  // use these variables to check on blocks of branches, 
  // and if they are missing don't try to access them to 
  // avoid memory access errors.
  // FIXME: for simplicity assume if one branch is found, all are present
  UInt_t m_found_nSubstructure_branches = 0; //!
  UInt_t m_found_nSftDrop_branches = 0; //!
  UInt_t m_found_nShowerDeconstruction_branches = 0; //!
  UInt_t m_found_signalMatching = 0; //!

public:
  // this is a standard constructor
  Flattener ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

//  void GetInputBranches();
  void ClearInputBranches();
  void DefineOutputBranches(TTree *treeout);
  void ClearOutputBranches();

  float GetWeight_SigToBkg_Pt_W(float pt);
  float GetWeight_SigToBkg_Pt_top(float pt);

  float GetWeight_SigToBkg_Pt_W_dR(float pt);
  float GetWeight_SigToBkg_Pt_top_dR(float pt);

  float GetWeight_Bkg(float mc_weight, float xsec, float filteff, float numevents);
  float AddWeightXsecFilterEffNumEvents (float xsec, float filteff, float numevents);


private:
/*
////////////

// Output tree

////////////
*/

   // Event information
   Int_t           EventInfo_runNumber;
   Long64_t        EventInfo_eventNumber;
   Int_t           EventInfo_lumiBlock;
   UInt_t          EventInfo_coreFlags;
   Int_t           EventInfo_mcEventNumber;
   Int_t           EventInfo_mcChannelNumber;
   Float_t         EventInfo_mcEventWeight;
   Int_t           EventInfo_ntruthJets;
   Int_t           EventInfo_nfatjets;

  // Add here the pileup information
  Float_t EventInfo_weight_pileup;
  Int_t EventInfo_NPV;
  Float_t EventInfo_actualInteractionsPerCrossing;
  Float_t EventInfo_averageInteractionsPerCrossing;

   // Information from the jets
   // Define the jet based variables (not anymore the vectors)
   // Truth information:
   float   fjet_truthJet_E;
   float   fjet_truthJet_pt;
   float   fjet_truthJet_phi;
   float   fjet_truthJet_eta;
   float   fjet_truthJet_m;
   int     fjet_truthJet_nthLeading;
   // Fat Jets' kinematic, substructure variables
   float   fjet_E;
   float   fjet_m;
   float   fjet_pt;
   float   fjet_phi;
   float   fjet_eta;
   int     fjet_nthLeading;
   int     fjet_fatjets_dRmatched_reco_truth;
   int     fjet_fatjet_dRmatched_particle_flavor;
   int     fjet_fatjet_ghost_assc_flavor;
   int     fjet_dRmatched_maxEParton_flavor;
   int     fjet_dRmatched_topBChild;
   int     fjet_dRmatched_nQuarkChildren;
   float   fjet_Tau1_wta;
   float   fjet_Tau2_wta;
   float   fjet_Tau3_wta;
   float   fjet_Tau21_wta;
   float   fjet_Tau32_wta;
   float   fjet_ECF1;
   float   fjet_ECF2;
   float   fjet_ECF3;
   float   fjet_e2;
   float   fjet_e3;
   float   fjet_C2;
   float   fjet_D2;
   float   fjet_Angularity;
   float   fjet_Aplanarity;
   float   fjet_Dip12;
   float   fjet_FoxWolfram20;
   float   fjet_KtDR;
   float   fjet_PlanarFlow;
   float   fjet_Sphericity;
   float   fjet_Split12;
   float   fjet_Split23;
   float   fjet_ThrustMaj;
   float   fjet_ThrustMin;
   float   fjet_ZCut12;
   float   fjet_Qw;
   float   fjet_Mu12;

   float   fjet_TrackAssistedMassUnCalibrated;
   float   fjet_TrackAssistedMassCalibrated;
   float   fjet_CaloTACombinedMass;
   float   fjet_CaloTACombinedMassUncorrelated;
   float   fjet_CaloTACombinedMassWprime;
   float   fjet_CaloTACombinedMassUncorrelatedWprime;
   float   fjet_eta_detector; 
   float   fjet_JetpTCorrByCalibratedTAMass;
   float   fjet_JetpTCorrByCombinedMass;
   float   fjet_pTResponseCombMassCorrpT;
   int fjet_NUngroomedSubjets;
   int fjet_numUngroomedCons;
   int fjet_Ntrk500;
   int fjet_Ntrk1000;


   // additional truth mathcing info 
   int   fjet_truth_nthLeading;
   float fjet_truth_dRmatched_maxEParton_flavor;
   float fjet_truth_dRmatched_particle_flavor;
   float fjet_truth_dRmatched_particle_dR;
   int   fjet_truth_dRmatched_topBChild;
   int   fjet_truth_dRmatched_nWZChildren;

   float fjet_dRmatched_particle_dR;
   int   fjet_dRmatched_nMatchedWZChildren;
   
   float     fjet_truth_dRmatched_topBChild_dR;
   float     fjet_truth_dRmatched_WZChild1_dR;
   float     fjet_truth_dRmatched_WZChild2_dR;
   float     fjet_dRmatched_topBChild_dR;
   float     fjet_dRmatched_WZChild1_dR;
   float     fjet_dRmatched_WZChild2_dR;
   float     fjet_SDt_CA15_uncalib;
   float     fjet_SDt_CA15_force3;
   float     fjet_SDt_CA15_combined;


  // Constituents, subjet information
  int     fjet_numConstituents;
  int     fjet_NTrimSubjets;

   float   fjet_logChiSD_w1;
   float   fjet_logChiSD_w2;
   float   fjet_logChiSD_t1;
   float   fjet_logChiSD_t2;

  

   //Add float for SD (ready from the dumper)
  float fjet_SDw_win20_btag0;
  float fjet_SDw_win20_btag1;
  float fjet_SDw_win25_btag0;
  float fjet_SDw_win25_btag1;
  float fjet_SDz_win20_btag0;
  float fjet_SDz_win20_btag1;
  float fjet_SDz_win25_btag0;
  float fjet_SDz_win25_btag1;
  float fjet_SDt_win50_btag0;
  float fjet_SDt_win50_btag1;
  float fjet_SDt_win55_btag0;
  float fjet_SDt_win55_btag1;

  float fjet_SDt_Dcut1;
  float fjet_SDt_Dcut2;

  //Add float for new ECFS

  float fjet_M2beta1;
  float fjet_N2beta1;
  float fjet_D2alpha1beta2;
  float fjet_N3beta2;

  // Add float for soft drop
  float fjet_MSoftDropBm20Zp001;
  float fjet_MSoftDropBm20Zp005;
  float fjet_MSoftDropBm20Zp010;
  float fjet_MSoftDropBm20Zp050;
  float fjet_MSoftDropBm20Zp100;
  float fjet_MSoftDropBm10Zp001;
  float fjet_MSoftDropBm10Zp005;
  float fjet_MSoftDropBm10Zp010;
  float fjet_MSoftDropBm10Zp050;
  float fjet_MSoftDropBm10Zp100;
  float fjet_MSoftDropBm05Zp001;
  float fjet_MSoftDropBm05Zp005;
  float fjet_MSoftDropBm05Zp010;
  float fjet_MSoftDropBm05Zp050;
  float fjet_MSoftDropBm05Zp100;
  float fjet_MSoftDropBp00Zp001;
  float fjet_MSoftDropBp00Zp005;
  float fjet_MSoftDropBp00Zp010;
  float fjet_MSoftDropBp00Zp050;
  float fjet_MSoftDropBp00Zp100;
  /*
  float fjet_MSoftDropBp05Zp001;
  float fjet_MSoftDropBp05Zp005;
  float fjet_MSoftDropBp05Zp010;
  float fjet_MSoftDropBp05Zp050;
  float fjet_MSoftDropBp05Zp100;
  float fjet_MSoftDropBp10Zp001;
  float fjet_MSoftDropBp10Zp005;
  float fjet_MSoftDropBp10Zp010;
  float fjet_MSoftDropBp10Zp050;
  float fjet_MSoftDropBp10Zp100;
  float fjet_MSoftDropBp20Zp001;
  float fjet_MSoftDropBp20Zp005;
  float fjet_MSoftDropBp20Zp010;
  float fjet_MSoftDropBp20Zp050;
  float fjet_MSoftDropBp20Zp100;
  */

  // HEPTopTagger
  float flat_HTT_CA15_pt;
  float flat_HTT_CA15_eta;
  float flat_HTT_CA15_phi;
  float flat_HTT_CA15_m;
  int   flat_HTT_CA15_nthLeading;
  int   flat_HTT_CA15_dRmatched_reco_truth;
  float flat_HTT_CA15_dRmatched_maxEParton_flavor;
  float flat_HTT_CA15_dRmatched_particle_flavor;
  float flat_HTT_CA15_dRmatched_particle_dR;
  int   flat_HTT_CA15_dRmatched_topBChild;
  int   flat_HTT_CA15_dRmatched_nMatchedWZChildren;
  
  float fjet_SDt_CA15_Dcut1;
  float fjet_SDt_CA15_Dcut2;

  int flat_HTT_CA15_trk_bjets_n;
  float     flat_HTT_CA15_dRmatched_topBChild_dR;
  float     flat_HTT_CA15_dRmatched_WZChild1_dR;
  float     flat_HTT_CA15_dRmatched_WZChild2_dR;

  std::vector< float > flat_HTT_pt_arr;
  std::vector< float > flat_HTT_eta_arr; 
  std::vector< float > flat_HTT_phi_arr;
  std::vector< float > flat_HTT_m_arr;      

  std::vector< float > flat_HTT_m12_arr; 
  std::vector< float > flat_HTT_m13_arr;
  std::vector< float > flat_HTT_m23_arr;
  std::vector< float > flat_HTT_m123_arr;
  std::vector< float > flat_HTT_m23m123_arr; 
  std::vector< float > flat_HTT_atan1312_arr; 

  std::vector< int >   flat_HTT_nTopCands_arr; 
  std::vector< int >  flat_HTT_tagged_arr; 
 
  std::vector< float > flat_HTT_dRW1W2_arr; 
  std::vector< float > flat_HTT_dRW1B_arr; 
  std::vector< float > flat_HTT_dRW2B_arr;
  std::vector< float > flat_HTT_dRmaxW1W2B_arr; 
  std::vector< float > flat_HTT_cosThetaW1W2_arr; 
  std::vector< float > flat_HTT_cosTheta12_arr; 


  // Weight branches
  float fjet_xsec_filteff_numevents;
  float fjet_testing_weight_pt;
  float fjet_testing_weight_pt_dR;


/*
////////////

// Input tree

////////////
*/


  // Declaration of input leaf types
   // Event information
   Int_t           runNumber= 0;
   Long64_t        eventNumber= 0;
   Int_t           lumiBlock= 0;
   UInt_t          coreFlags= 0;
   Int_t           mcEventNumber= 0;
   Int_t           mcChannelNumber= 0;
   Float_t         mcEventWeight= 0;
  // Add pileup information
   Float_t weight_pileup = 0;
   Int_t NPV = 0;
   Float_t actualInteractionsPerCrossing = 0;
   Float_t averageInteractionsPerCrossing = 0;

   Int_t           ntruth_fatjets= 0;
   std::vector<float>   *fatjet_truth_E= 0;
   std::vector<float>   *fatjet_truth_pt= 0;
   std::vector<float>   *fatjet_truth_phi= 0;
   std::vector<float>   *fatjet_truth_eta= 0;
   std::vector<float>   *fatjet_truth_m= 0;
   std::vector<int>     *fatjet_truth_nthLeading= 0;
   Int_t           nfatjets= 0;
   std::vector<float>   *fatjet_E= 0;
   std::vector<float>   *fatjet_m= 0;
   std::vector<float>   *fatjet_pt= 0;
   std::vector<float>   *fatjet_phi= 0;
   std::vector<float>   *fatjet_eta= 0;
   std::vector<int>     *fatjet_nthLeading= 0;
   std::vector<int>     *fatjets_dRmatched_reco_truth= 0;
   std::vector<int>     *fatjet_dRmatched_particle_flavor= 0;
   std::vector<int>     *fatjet_ghost_assc_flavor= 0;
   std::vector<int>     *fatjet_dRmatched_maxEParton_flavor= 0;
   std::vector<int>     *fatjet_dRmatched_topBChild= 0;
   std::vector<int>     *fatjet_dRmatched_nQuarkChildren= 0;
   std::vector<float> *fatjet_truth_dRmatched_particle_flavor= 0;
   std::vector<float> *fatjet_truth_dRmatched_particle_dR= 0;
   std::vector<int>   *fatjet_truth_dRmatched_topBChild= 0;
   std::vector<int>   *fatjet_truth_dRmatched_nWZChildren= 0;
   std::vector<float> *fatjet_truth_dRmatched_maxEParton_flavor=0;

   std::vector<float> *fatjet_dRmatched_particle_dR= 0;
   std::vector<int>   *fatjet_dRmatched_nMatchedWZChildren= 0;

   std::vector<float>   *fatjet_truth_dRmatched_topBChild_dR = 0;
   std::vector<float>   *fatjet_truth_dRmatched_WZChild1_dR = 0;
   std::vector<float>   *fatjet_truth_dRmatched_WZChild2_dR = 0;
   std::vector<float>   *fatjet_dRmatched_topBChild_dR = 0;
   std::vector<float>   *fatjet_dRmatched_WZChild1_dR = 0;
   std::vector<float>   *fatjet_dRmatched_WZChild2_dR = 0;
   std::vector<float>   *fatjet_SDt_CA15_uncalib = 0;
   std::vector<float>   *fatjet_SDt_CA15_force3 = 0;
   std::vector<float>   *fatjet_SDt_CA15_combined = 0;


   std::vector<float>   *fatjet_Tau1_wta= 0;
   std::vector<float>   *fatjet_Tau2_wta= 0;
   std::vector<float>   *fatjet_Tau3_wta= 0;
   std::vector<float>   *fatjet_Tau21_wta= 0;
   std::vector<float>   *fatjet_Tau32_wta= 0;
   std::vector<float>   *fatjet_ECF1= 0;
   std::vector<float>   *fatjet_ECF2= 0;
   std::vector<float>   *fatjet_ECF3= 0;
   std::vector<float>   *fatjet_C2= 0;
   std::vector<float>   *fatjet_D2= 0;
   std::vector<float>   *fatjet_Angularity= 0;
   std::vector<float>   *fatjet_Aplanarity= 0;
   std::vector<float>   *fatjet_Dip12= 0;
   std::vector<float>   *fatjet_FoxWolfram20= 0;
   std::vector<float>   *fatjet_KtDR= 0;
   std::vector<float>   *fatjet_PlanarFlow= 0;
   std::vector<float>   *fatjet_Sphericity= 0;
   std::vector<float>   *fatjet_Split12= 0;
   std::vector<float>   *fatjet_Split23= 0;
   std::vector<float>   *fatjet_ThrustMaj= 0;
   std::vector<float>   *fatjet_ThrustMin= 0;
   std::vector<float>   *fatjet_ZCut12 = 0;
   std::vector<float>   *fatjet_Qw = 0;
   std::vector<float>   *fatjet_Mu12 = 0;

   std::vector<float>   *fatjet_TrackAssistedMassUnCalibrated = 0;
   std::vector<float>   *fatjet_TrackAssistedMassCalibrated = 0;
   std::vector<float>   *fatjet_CaloTACombinedMass = 0;

   std::vector<int>     *fatjet_numConstituents = 0;
   std::vector<int>     *fatjet_NTrimSubjets = 0;

   std::vector<float>   *trackJet_E = 0;
   std::vector<float>   *trackJet_pt = 0;
   std::vector<float>   *trackJet_eta = 0;
   std::vector<float>   *trackJet_phi = 0;
   std::vector<float>   *trackJet_MV2c20 = 0;

   std::vector<float> *fatjet_eta_detector = 0;
   std::vector<float> *fatjet_JetpTCorrByCalibratedTAMass= 0;
   std::vector<int> *fatjet_NUngroomedSubjets= 0;
   std::vector<int> *fatjet_numUngroomedCons= 0;
   std::vector<int> *fatjet_Ntrk500=0;
   std::vector<int> *fatjet_Ntrk1000=0;


   // NEW ECFS
   std::vector<float> *fatjet_M2beta1=0;
   std::vector<float> *fatjet_N2beta1=0;
   std::vector<float> *fatjet_D2alpha1beta2=0;
   std::vector<float> *fatjet_N3beta2=0;

  // SD
   std::vector<float> *fatjet_SDw_win20_btag0=0;
   std::vector<float> *fatjet_SDw_win20_btag1=0;
   std::vector<float> *fatjet_SDw_win25_btag0=0;
   std::vector<float> *fatjet_SDw_win25_btag1=0;
   std::vector<float> *fatjet_SDz_win20_btag0=0;
   std::vector<float> *fatjet_SDz_win20_btag1=0;
   std::vector<float> *fatjet_SDz_win25_btag0=0;
   std::vector<float> *fatjet_SDz_win25_btag1=0;
   std::vector<float> *fatjet_SDt_win50_btag0=0;
   std::vector<float> *fatjet_SDt_win50_btag1=0;
   std::vector<float> *fatjet_SDt_win55_btag0=0;
   std::vector<float> *fatjet_SDt_win55_btag1=0;

   std::vector<float> *fatjet_SDt_Dcut1= 0;
   std::vector<float> *fatjet_SDt_Dcut2= 0;
 

  // Soft Drop
std::vector<float> *fatjet_MSoftDropBm20Zp001=0;
std::vector<float> *fatjet_MSoftDropBm20Zp005=0;
std::vector<float> *fatjet_MSoftDropBm20Zp010=0;
std::vector<float> *fatjet_MSoftDropBm20Zp050=0;
std::vector<float> *fatjet_MSoftDropBm20Zp100=0;
std::vector<float> *fatjet_MSoftDropBm10Zp001=0;
std::vector<float> *fatjet_MSoftDropBm10Zp005=0;
std::vector<float> *fatjet_MSoftDropBm10Zp010=0;
std::vector<float> *fatjet_MSoftDropBm10Zp050=0;
std::vector<float> *fatjet_MSoftDropBm10Zp100=0;
std::vector<float> *fatjet_MSoftDropBm05Zp001=0;
std::vector<float> *fatjet_MSoftDropBm05Zp005=0;
std::vector<float> *fatjet_MSoftDropBm05Zp010=0;
std::vector<float> *fatjet_MSoftDropBm05Zp050=0;
std::vector<float> *fatjet_MSoftDropBm05Zp100=0;
std::vector<float> *fatjet_MSoftDropBp00Zp001=0;
std::vector<float> *fatjet_MSoftDropBp00Zp005=0;
std::vector<float> *fatjet_MSoftDropBp00Zp010=0;
std::vector<float> *fatjet_MSoftDropBp00Zp050=0;
std::vector<float> *fatjet_MSoftDropBp00Zp100=0;
/*
std::vector<float> *fatjet_MSoftDropBp05Zp001=0;
std::vector<float> *fatjet_MSoftDropBp05Zp005=0;
std::vector<float> *fatjet_MSoftDropBp05Zp010=0;
std::vector<float> *fatjet_MSoftDropBp05Zp050=0;
std::vector<float> *fatjet_MSoftDropBp05Zp100=0;
std::vector<float> *fatjet_MSoftDropBp10Zp001=0;
std::vector<float> *fatjet_MSoftDropBp10Zp005=0;
std::vector<float> *fatjet_MSoftDropBp10Zp010=0;
std::vector<float> *fatjet_MSoftDropBp10Zp050=0;
std::vector<float> *fatjet_MSoftDropBp10Zp100=0;
std::vector<float> *fatjet_MSoftDropBp20Zp001=0;
std::vector<float> *fatjet_MSoftDropBp20Zp005=0;
std::vector<float> *fatjet_MSoftDropBp20Zp010=0;
std::vector<float> *fatjet_MSoftDropBp20Zp050=0;
std::vector<float> *fatjet_MSoftDropBp20Zp100=0;
*/

  // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_mcEventNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mcEventWeight;   //!
   TBranch        *b_weight_pileup; //!
   TBranch        *b_NPV; //!
   TBranch        *b_actualInteractionsPerCrossing; //!
   TBranch        *b_averageInteractionsPerCrossing; //!
   TBranch        *b_ntruth_fatjets;   //!
   TBranch        *b_fatjet_truth_E;   //!
   TBranch        *b_fatjet_truth_pt;   //!
   TBranch        *b_fatjet_truth_phi;   //!
   TBranch        *b_fatjet_truth_eta;   //!
   TBranch        *b_fatjet_truth_m;   //!
   TBranch        *b_fatjet_truth_nthLeading;   //!
   TBranch        *b_nfatjets;   //!
   TBranch        *b_fatjet_E;   //!
   TBranch        *b_fatjet_m;   //!
   TBranch        *b_fatjet_pt;   //!
   TBranch        *b_fatjet_phi;   //!
   TBranch        *b_fatjet_eta;   //!
   TBranch        *b_fatjet_nthLeading;   //!
   TBranch        *b_fatjets_dRmatched_reco_truth;   //!
   TBranch        *b_fatjet_dRmatched_particle_flavor;   //!
   TBranch        *b_fatjet_ghost_assc_flavor;   //!
   TBranch        *b_fatjet_dRmatched_maxEParton_flavor;   //!
   TBranch        *b_fatjet_dRmatched_topBChild;   //!
   TBranch        *b_fatjet_dRmatched_nQuarkChildren;   //!
   TBranch        *b_fatjet_Tau1_wta;   //!
   TBranch        *b_fatjet_Tau2_wta;   //!
   TBranch        *b_fatjet_Tau3_wta;   //!
   TBranch        *b_fatjet_Tau21_wta;   //!
   TBranch        *b_fatjet_Tau32_wta;   //!
   TBranch        *b_fatjet_ECF1;   //!
   TBranch        *b_fatjet_ECF2;   //!
   TBranch        *b_fatjet_ECF3;   //!
   TBranch        *b_fatjet_C2;   //!
   TBranch        *b_fatjet_D2;   //!
   TBranch        *b_fatjet_Angularity;   //!
   TBranch        *b_fatjet_Aplanarity;   //!
   TBranch        *b_fatjet_Dip12;   //!
   TBranch        *b_fatjet_FoxWolfram20;   //!
   TBranch        *b_fatjet_KtDR;   //!
   TBranch        *b_fatjet_PlanarFlow;   //!
   TBranch        *b_fatjet_Sphericity;   //!
   TBranch        *b_fatjet_Split12;   //!
   TBranch        *b_fatjet_Split23;   //!
   TBranch        *b_fatjet_ThrustMaj;   //!
   TBranch        *b_fatjet_ThrustMin;   //!
   TBranch        *b_fatjet_ZCut12;   //!
   TBranch        *b_fatjet_Qw; //!
   TBranch        *b_fatjet_Mu12; //!

   TBranch        *b_fatjet_TrackAssistedMassUnCalibrated; //!
   TBranch        *b_fatjet_TrackAssistedMassCalibrated; //!
   TBranch        *b_fatjet_CaloTACombinedMass; //!
   TBranch        *b_fatjet_CaloTACombinedMassUncorrelated; //!

   TBranch        *b_fatjet_numConstituents; //!
   TBranch       *b_fatjet_NTrimSubjets;  //!
   TBranch        *b_fatjet_Ntrk500; //!
   TBranch        *b_fatjet_Ntrk1000; //!
   TBranch        *b_fatjet_eta_detector; //!

   TBranch       *b_fatjet_M2beta1; //!
   TBranch       *b_fatjet_N2beta1; //!
   TBranch       *b_fatjet_D2alpha1beta2; //!
   TBranch       *b_fatjet_N3beta2; //!
   TBranch  *b_fatjet_truth_dRmatched_topBChild_dR; //!
   TBranch  *b_fatjet_truth_dRmatched_WZChild1_dR; //!
   TBranch  *b_fatjet_truth_dRmatched_WZChild2_dR; //!
   TBranch  *b_fatjet_dRmatched_topBChild_dR; //!
   TBranch  *b_fatjet_dRmatched_WZChild1_dR; //!
   TBranch  *b_fatjet_dRmatched_WZChild2_dR; //!
   TBranch  *b_fatjet_SDt_CA15_uncalib; //!
   TBranch  *b_fatjet_SDt_CA15_force3; //!
   TBranch  *b_fatjet_SDt_CA15_combined; //!
   

  TBranch *b_fatjet_MSoftDropBm20Zp001; //!
TBranch *b_fatjet_MSoftDropBm20Zp005; //!
TBranch *b_fatjet_MSoftDropBm20Zp010; //!
TBranch *b_fatjet_MSoftDropBm20Zp050; //!
TBranch *b_fatjet_MSoftDropBm20Zp100; //!
TBranch *b_fatjet_MSoftDropBm10Zp001; //!
TBranch *b_fatjet_MSoftDropBm10Zp005; //!
TBranch *b_fatjet_MSoftDropBm10Zp010; //!
TBranch *b_fatjet_MSoftDropBm10Zp050; //!
TBranch *b_fatjet_MSoftDropBm10Zp100; //!
TBranch *b_fatjet_MSoftDropBm05Zp001; //!
TBranch *b_fatjet_MSoftDropBm05Zp005; //!
TBranch *b_fatjet_MSoftDropBm05Zp010; //!
TBranch *b_fatjet_MSoftDropBm05Zp050; //!
TBranch *b_fatjet_MSoftDropBm05Zp100; //!
TBranch *b_fatjet_MSoftDropBp00Zp001; //!
TBranch *b_fatjet_MSoftDropBp00Zp005; //!
TBranch *b_fatjet_MSoftDropBp00Zp010; //!
TBranch *b_fatjet_MSoftDropBp00Zp050; //!
TBranch *b_fatjet_MSoftDropBp00Zp100; //!
TBranch *b_fatjet_MSoftDropBp05Zp001; //!
TBranch *b_fatjet_MSoftDropBp05Zp005; //!
TBranch *b_fatjet_MSoftDropBp05Zp010; //!
TBranch *b_fatjet_MSoftDropBp05Zp050; //!
TBranch *b_fatjet_MSoftDropBp05Zp100; //!
TBranch *b_fatjet_MSoftDropBp10Zp001; //!
TBranch *b_fatjet_MSoftDropBp10Zp005; //!
TBranch *b_fatjet_MSoftDropBp10Zp010; //!
TBranch *b_fatjet_MSoftDropBp10Zp050; //!
TBranch *b_fatjet_MSoftDropBp10Zp100; //!
TBranch *b_fatjet_MSoftDropBp20Zp001; //!
TBranch *b_fatjet_MSoftDropBp20Zp005; //!
TBranch *b_fatjet_MSoftDropBp20Zp010; //!
TBranch *b_fatjet_MSoftDropBp20Zp050; //!
TBranch *b_fatjet_MSoftDropBp20Zp100; //!
TBranch *b_fatjet_SDw_win20_btag0; //!
TBranch *b_fatjet_SDw_win20_btag1; //!
TBranch *b_fatjet_SDw_win25_btag0; //!
TBranch *b_fatjet_SDw_win25_btag1; //!
TBranch *b_fatjet_SDz_win20_btag0; //!
TBranch *b_fatjet_SDz_win20_btag1; //!
TBranch *b_fatjet_SDz_win25_btag0; //!
TBranch *b_fatjet_SDz_win25_btag1; //!
TBranch *b_fatjet_SDt_win50_btag0; //!
TBranch *b_fatjet_SDt_win50_btag1; //!
TBranch *b_fatjet_SDt_win55_btag0; //!
 TBranch *b_fatjet_SDt_win55_btag1; //!
 TBranch *b_fatjet_SDw_win20_cal20_high; //!
 TBranch *b_fatjet_SDw_win20_cal20_low; //!
 TBranch *b_fatjet_SDw_win20_uncal10_high; //!
 TBranch *b_fatjet_SDw_win20_uncal10_low; //!
 TBranch *b_fatjet_SDw_win20_force3_high; //!
 TBranch *b_fatjet_SDw_win20_force3_low; //!

 TBranch *b_fatjet_SDt_win50_cal20_high; //!
 TBranch *b_fatjet_SDt_win50_cal20_low; //!
 TBranch *b_fatjet_SDt_win50_uncal10_high; //!
 TBranch *b_fatjet_SDt_win50_uncal10_low; //!
 TBranch *b_fatjet_SDt_win50_force3_high; //!
 TBranch *b_fatjet_SDt_win50_force3_low; //!

 TBranch *b_fatjet_SDt_win50_cal20_btag1; //!
 TBranch *b_fatjet_SDt_win50_uncal10_btag1; //!
 TBranch *b_fatjet_SDt_win50_force3_btag1; //!


   TBranch   *b_trackJet_E; //!
   TBranch   *b_trackJet_pt; //!
   TBranch   *b_trackJet_eta; //!
   TBranch   *b_trackJet_phi; //!
   TBranch   *b_trackJet_MV2c20; //!

   TBranch *b_fatjet_truth_dRmatched_maxEParton_flavor; //!
   TBranch *b_fatjet_truth_dRmatched_particle_flavor; //!
   TBranch *b_fatjet_truth_dRmatched_particle_dR; //!
   TBranch   *b_fatjet_truth_dRmatched_topBChild; //!
   TBranch   *b_fatjet_truth_dRmatched_nWZChildren; //!
   TBranch *b_fatjet_dRmatched_particle_dR; //!
   TBranch   *b_fatjet_dRmatched_nMatchedWZChildren; //!


   TBranch *b_fatjet_JetpTCorrByCalibratedTAMass; //!
   TBranch *b_fatjet_NUngroomedSubjets; //!
   TBranch *b_fatjet_numUngroomedCons; //!

   TBranch *b_fatjet_SDt_Dcut1; //!
   TBranch *b_fatjet_SDt_Dcut2; //!

   // Cluster information:
   // Input tree:
   std::vector<std::vector<float>>   *fatjet_assoc_cluster_E = 0;
   std::vector<std::vector<float>>   *fatjet_assoc_cluster_pt = 0;
   std::vector<std::vector<float>>   *fatjet_assoc_cluster_phi = 0;
   std::vector<std::vector<float>>   *fatjet_assoc_cluster_eta = 0;

   // List of input branches
   TBranch *b_fatjet_assoc_cluster_E; //!
   TBranch *b_fatjet_assoc_cluster_pt; //!
   TBranch *b_fatjet_assoc_cluster_phi; //!
   TBranch *b_fatjet_assoc_cluster_eta; //!

 // ===================================================================
 // HEPTopTagger
   static const int m_nHTTConfigs = 11; //!
   const std::string m_HTTConfigs[m_nHTTConfigs] = {"normal", "def", "tight", "loose", "sloose", "Acut", "Ptord", "defR03", "defR02", "defNf6", "noAShape"}; //!

   // use HTT or not
   bool m_HEPTopTagger = false; //!
   UInt_t m_found_nHTTConfig_branches[m_nHTTConfigs]; //!
   bool m_extraTruth = false; //!                                                                                                             

   std::vector<float> *HTT_CA15_pt=0; //!
   std::vector<float> *HTT_CA15_eta=0; //!
   std::vector<float> *HTT_CA15_phi=0; //!
   std::vector<float> *HTT_CA15_m=0; //!
   std::vector<float> *fatjet_SDt_CA15_Dcut1= 0; //!
   std::vector<float> *fatjet_SDt_CA15_Dcut2= 0; //!
   
   std::vector<int>   *HTT_CA15_nthLeading= 0; //!
   std::vector<int>   *HTT_CA15_dRmatched_reco_truth= 0; //!
   std::vector<float> *HTT_CA15_dRmatched_maxEParton_flavor= 0; //!
   std::vector<float> *HTT_CA15_dRmatched_particle_flavor= 0; //!
   std::vector<float> *HTT_CA15_dRmatched_particle_dR= 0; //!
   std::vector<int>   *HTT_CA15_dRmatched_topBChild= 0; //!
   std::vector<int>   *HTT_CA15_dRmatched_nMatchedWZChildren= 0; //!
   std::vector<float>   *HTT_CA15_dRmatched_topBChild_dR = 0; //!
   std::vector<float>   *HTT_CA15_dRmatched_WZChild1_dR = 0; //!
   std::vector<float>   *HTT_CA15_dRmatched_WZChild2_dR = 0; //!

   std::vector<int> *HTT_CA15_trk_bjets_n=0; //!

   std::vector< std::vector<float>* > HTT_pt_arr; //!
   std::vector< std::vector<float>* > HTT_eta_arr; //!        
   std::vector< std::vector<float>* > HTT_phi_arr; //!
   std::vector< std::vector<float>* > HTT_m_arr; //!     

   std::vector< std::vector<float>* > HTT_m12_arr; //!
   std::vector< std::vector<float>* > HTT_m13_arr; //!
   std::vector< std::vector<float>* > HTT_m23_arr; //!
   std::vector< std::vector<float>* > HTT_m123_arr; //!
   std::vector< std::vector<float>* > HTT_m23m123_arr; //!
   std::vector< std::vector<float>* > HTT_atan1312_arr; //!

   std::vector< std::vector<int>* >   HTT_nTopCands_arr; //!
   std::vector< std::vector<bool>* >  HTT_tagged_arr; //!
 //!
   std::vector< std::vector<float>* > HTT_dRW1W2_arr; //!
   std::vector< std::vector<float>* > HTT_dRW1B_arr; //!
   std::vector< std::vector<float>* > HTT_dRW2B_arr; //!
   std::vector< std::vector<float>* > HTT_dRmaxW1W2B_arr; //!
   std::vector< std::vector<float>* > HTT_cosThetaW1W2_arr; //!
   std::vector< std::vector<float>* > HTT_cosTheta12_arr; //!


   // branches
   TBranch *b_HTT_CA15_pt; //!
   TBranch *b_HTT_CA15_eta; //!
   TBranch *b_HTT_CA15_phi; //!
   TBranch *b_HTT_CA15_m; //!

   TBranch *b_HTT_CA15_trk_bjets_n; //!
   TBranch *b_fatjet_SDt_CA15_Dcut1; //!
   TBranch *b_fatjet_SDt_CA15_Dcut2; //!


   TBranch   *b_HTT_CA15_nthLeading; //!
   TBranch   *b_HTT_CA15_dRmatched_reco_truth; //!
   TBranch *b_HTT_CA15_dRmatched_maxEParton_flavor; //!
   TBranch *b_HTT_CA15_dRmatched_particle_flavor; //!
   TBranch *b_HTT_CA15_dRmatched_particle_dR; //!
   TBranch   *b_HTT_CA15_dRmatched_topBChild; //!
   TBranch   *b_HTT_CA15_dRmatched_nMatchedWZChildren; //!
   TBranch  *b_HTT_CA15_dRmatched_topBChild_dR; //!
   TBranch  *b_HTT_CA15_dRmatched_WZChild1_dR; //!
   TBranch  *b_HTT_CA15_dRmatched_WZChild2_dR; //!

   std::vector<TBranch* > b_HTT_pt_arr; //!           
   std::vector<TBranch* > b_HTT_eta_arr; //!        
   std::vector<TBranch* > b_HTT_phi_arr; //!
   std::vector<TBranch* > b_HTT_m_arr; //!     
 
   
   std::vector<TBranch* > b_HTT_m12_arr; //!
   std::vector<TBranch* > b_HTT_m13_arr; //!
   std::vector<TBranch* > b_HTT_m23_arr; //!
   std::vector<TBranch* > b_HTT_m123_arr; //!
   std::vector<TBranch* > b_HTT_m23m123_arr; //!
   std::vector<TBranch* > b_HTT_atan1312_arr; //!

   std::vector<TBranch* > b_HTT_nTopCands_arr; //!
   std::vector<TBranch* > b_HTT_tagged_arr; //!

   std::vector<TBranch* > b_HTT_dRW1W2_arr; //!
   std::vector<TBranch* > b_HTT_dRW1B_arr; //!
   std::vector<TBranch* > b_HTT_dRW2B_arr; //!
   std::vector<TBranch* > b_HTT_dRmaxW1W2B_arr; //!
   std::vector<TBranch* > b_HTT_cosThetaW1W2_arr; //!
   std::vector<TBranch* > b_HTT_cosTheta12_arr; //!



 // HEPTopTagger
 // ===================================================================

public:
   double calculateCombinedMass( double mCalo, double mTA, double recopt, bool Wprime);
   double unCorrelatedcalculateCombinedMass( double mCalo, double mTA, double recopt, bool Wprime);

  // this is needed to distribute the algorithm to the workers
  ClassDef(Flattener, 1);
};

#endif
