#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <NTupleFlattener/Flattener.h>

#include <fastjet/ClusterSequenceArea.hh>
#include <fastjet/PseudoJet.hh>
#include <fastjet/JetDefinition.hh>
#include <fastjet/AreaDefinition.hh>


#include <TLorentzVector.h>
#include <TFile.h>
#include "TH1D.h"
#include "TH2D.h"

//#include "ShowerDeconstruction/ShowerDeconstruction.h"
// this is needed to distribute the algorithm to the workers
ClassImp(Flattener)



Flattener :: Flattener ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode Flattener :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Flattener :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  //outputFile->cd();
//  outputFile = wk()->getOutputFile (outfilename);

  outputFile = wk()->getOutputFile ("tree");
  treeout = new TTree ("FlatSubstructureJetTree", "FlatSubstructureJetTree");
  treeout->SetDirectory(outputFile);


  this->DefineOutputBranches(treeout);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Flattener :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Flattener :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  TTree* treein = wk()->tree();
  treein->SetBranchStatus("*",0);
  // Set the branch status for the ones we want
  //treein->SetBranchStatus("",1);
  treein->SetBranchStatus("runNumber", "1"); 
  treein->SetBranchStatus("eventNumber", "1"); 
  treein->SetBranchStatus("lumiBlock", "1");
  treein->SetBranchStatus("coreFlags", "1");
  treein->SetBranchStatus("mcEventNumber", "1");
  treein->SetBranchStatus("mcChannelNumber", "1"); 
  treein->SetBranchStatus("mcEventWeight", "1");
  treein->SetBranchStatus("ntruth_fatjets", "1"); 
  // Add pileup information here
  treein->SetBranchStatus("weight_pileup","1");
  treein->SetBranchStatus("NPV","1");
  treein->SetBranchStatus("actualInteractionsPerCrossing","1");
  treein->SetBranchStatus("averageInteractionsPerCrossing","1");

  treein->SetBranchStatus("fatjet_truth_E", "1"); 
  treein->SetBranchStatus("fatjet_truth_pt", "1"); 
  treein->SetBranchStatus("fatjet_truth_phi", "1");
  treein->SetBranchStatus("fatjet_truth_eta", "1");
  treein->SetBranchStatus("fatjet_truth_m", "1");
  treein->SetBranchStatus("fatjet_truth_nthLeading", "1");
  treein->SetBranchStatus("nfatjet", "1"); 
  treein->SetBranchStatus("fatjet_E", "1"); 
  //treein->SetBranchStatus("fatjet_m", "1"); 
  treein->SetBranchStatus("fatjet_pt", "1"); 
  treein->SetBranchStatus("fatjet_phi", "1");
  treein->SetBranchStatus("fatjet_eta", "1");
  treein->SetBranchStatus("fatjet_nthLeading", "1");
  treein->SetBranchStatus("fatjet_dRmatched_reco_truth", "1"); 
  treein->SetBranchStatus("fatjet_dRmatched_particle_flavor", "1"); 
  treein->SetBranchStatus("fatjet_ghost_assc_flavor", "1");
 
  treein->SetBranchStatus("fatjet_dRmatched_maxEParton_flavor", "1"); 
  treein->SetBranchStatus("fatjet_dRmatched_topBChild", "1"); 
  //treein->SetBranchStatus("fatjet_dRmatched_nQuarkChildren", "1"); 
  treein->SetBranchStatus("fatjet_truth_nthLeading", "1");
  treein->SetBranchStatus("fatjet_truth_dRmatched_maxEParton_flavor", "1");
  treein->SetBranchStatus("fatjet_truth_dRmatched_particle_flavor", "1");
  treein->SetBranchStatus("fatjet_truth_dRmatched_particle_dR", "1");
  treein->SetBranchStatus("fatjet_truth_dRmatched_topBChild", "1");
  treein->SetBranchStatus("fatjet_truth_dRmatched_nWZChildren", "1");
  treein->SetBranchStatus("fatjet_dRmatched_particle_dR", "1");
  treein->SetBranchStatus("fatjet_dRmatched_nMatchedWZChildren", "1");
  treein->SetBranchStatus("fatjet_JetpTCorrByCalibratedTAMass", "1");
  treein->SetBranchStatus("fatjet_NUngroomedSubjets", "1");
  treein->SetBranchStatus("fatjet_numUngroomedCons", "1");
  treein->SetBranchStatus("fatjet_Ntrk500",  "1" );
  treein->SetBranchStatus("fatjet_Ntrk1000", "1" );

  treein->SetBranchStatus("fatjet_eta_detector", "1");

  treein->SetBranchStatus("fatjet_Tau1_wta", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_Tau2_wta", "1", &m_found_nSubstructure_branches); 
  treein->SetBranchStatus("fatjet_Tau3_wta", "1", &m_found_nSubstructure_branches); 
  treein->SetBranchStatus("fatjet_Tau21_wta", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_Tau32_wta", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_ECF1", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_ECF2", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_ECF3", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_C2", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_D2", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_Angularity", "1", &m_found_nSubstructure_branches); 
  treein->SetBranchStatus("fatjet_Aplanarity", "1", &m_found_nSubstructure_branches); 
  treein->SetBranchStatus("fatjet_Dip12", "1", &m_found_nSubstructure_branches); 
  treein->SetBranchStatus("fatjet_FoxWolfram20", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_KtDR", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_PlanarFlow", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_Sphericity", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_Split12", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_Split23", "1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_ThrustMaj", "1", &m_found_nSubstructure_branches); 
  treein->SetBranchStatus("fatjet_ThrustMin", "1", &m_found_nSubstructure_branches); 
  treein->SetBranchStatus("fatjet_ZCut12", "1", &m_found_nSubstructure_branches); 
  treein->SetBranchStatus("fatjet_Qw","1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_Mu12","1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_TrackAssistedMassUnCalibrated", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_TrackAssistedMassCalibrated", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_CaloTACombinedMass", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_numConstituents","1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_NTrimSubjets","1", &m_found_nSubstructure_branches);

  treein->SetBranchStatus("fatjet_M2beta1","1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_N2beta1","1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_D2alpha1beta2","1", &m_found_nSubstructure_branches);
  treein->SetBranchStatus("fatjet_N3beta2","1", &m_found_nSubstructure_branches);

  treein->SetBranchStatus("fatjet_truth_dRmatched_topBChild_dR", "1", &m_found_signalMatching);
  treein->SetBranchStatus("fatjet_truth_dRmatched_WZChild1_dR", "1", &m_found_signalMatching);
  treein->SetBranchStatus("fatjet_truth_dRmatched_WZChild2_dR", "1", &m_found_signalMatching);
  treein->SetBranchStatus("fatjet_dRmatched_topBChild_dR", "1", &m_found_signalMatching);
  treein->SetBranchStatus("fatjet_dRmatched_WZChild1_dR", "1", &m_found_signalMatching);
  treein->SetBranchStatus("fatjet_dRmatched_WZChild2_dR", "1", &m_found_signalMatching);

  if (m_found_signalMatching == 1) m_extraTruth = true; 
  if(m_found_nSubstructure_branches == 0)
    std::cout << "Couldn't find input branches fatjet_Tau1_wta ... fatjet_NTrimSubjets and won't fill in output tree.\n";

  // one could also just use a wildcard here. that way m_found_nSftDrop_branches would become more meaningful
  treein->SetBranchStatus("fatjet_MSoftDropBm20Zp001","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm20Zp005","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm20Zp010","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm20Zp050","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm20Zp100","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm10Zp001","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm10Zp005","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm10Zp010","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm10Zp050","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm10Zp100","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm05Zp001","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm05Zp005","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm05Zp010","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm05Zp050","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBm05Zp100","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp00Zp001","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp00Zp005","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp00Zp010","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp00Zp050","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp00Zp100","1", &m_found_nSftDrop_branches);
  /*
  treein->SetBranchStatus("fatjet_MSoftDropBp05Zp001","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp05Zp005","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp05Zp010","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp05Zp050","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp05Zp100","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp10Zp001","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp10Zp005","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp10Zp010","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp10Zp050","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp10Zp100","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp20Zp001","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp20Zp005","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp20Zp010","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp20Zp050","1", &m_found_nSftDrop_branches);
  treein->SetBranchStatus("fatjet_MSoftDropBp20Zp100","1", &m_found_nSftDrop_branches);
  */

  if(m_found_nSftDrop_branches == 0)
    std::cout << "Couldn't find input branches fatjet_MSoftDropBm20Zp001 ... fatjet_MSoftDropBp20Zp100 and won't fill in output tree.\n";

  // maybe use a wildcard here, then m_found_nShowerDeconstruction_branches also becomes more meaningfull
  treein->SetBranchStatus("fatjet_SDw_win20_btag0", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDw_win20_btag1", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDw_win25_btag0", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDw_win25_btag1", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDz_win20_btag0", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDz_win20_btag1", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDz_win25_btag0", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDz_win25_btag1", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDt_win50_btag0", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDt_win50_btag1", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDt_win55_btag0", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDt_win55_btag1", "1", &m_found_nShowerDeconstruction_branches);

  treein->SetBranchStatus("fatjet_SDt_Dcut1", "1", &m_found_nShowerDeconstruction_branches);
  treein->SetBranchStatus("fatjet_SDt_Dcut2", "1", &m_found_nShowerDeconstruction_branches);

 
  if(m_found_nShowerDeconstruction_branches == 0)
    std::cout << "Couldn't find input branches fatjet_SDw_win20_btag0 ... fatjet_SDw_win20_btag1 and won't fill in output tree.\n";

  // Set the branch addresses
  treein->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
  treein->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
  treein->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
  treein->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
  treein->SetBranchAddress("mcEventNumber", &mcEventNumber, &b_mcEventNumber);
  treein->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
  treein->SetBranchAddress("mcEventWeight", &mcEventWeight, &b_mcEventWeight);
  // Add pileup information
  treein->SetBranchAddress("weight_pileup",&weight_pileup,&b_weight_pileup);
  treein->SetBranchAddress("NPV",&NPV,&b_NPV);
  treein->SetBranchAddress("actualInteractionsPerCrossing",&actualInteractionsPerCrossing,&b_actualInteractionsPerCrossing);
  treein->SetBranchAddress("averageInteractionsPerCrossing",&averageInteractionsPerCrossing,&b_averageInteractionsPerCrossing);
  treein->SetBranchAddress("ntruth_fatjets", &ntruth_fatjets, &b_ntruth_fatjets);
  treein->SetBranchAddress("fatjet_truth_E", &fatjet_truth_E, &b_fatjet_truth_E);
  treein->SetBranchAddress("fatjet_truth_pt", &fatjet_truth_pt, &b_fatjet_truth_pt);
  treein->SetBranchAddress("fatjet_truth_phi", &fatjet_truth_phi, &b_fatjet_truth_phi);
  treein->SetBranchAddress("fatjet_truth_eta", &fatjet_truth_eta, &b_fatjet_truth_eta);
  treein->SetBranchAddress("fatjet_truth_m", &fatjet_truth_m, &b_fatjet_truth_m);
  treein->SetBranchAddress("fatjet_truth_nthLeading", &fatjet_truth_nthLeading, &b_fatjet_truth_nthLeading);
  treein->SetBranchAddress("nfatjet", &nfatjets, &b_nfatjets);
  treein->SetBranchAddress("fatjet_E", &fatjet_E, &b_fatjet_E);
  //treein->SetBranchAddress("fatjet_m", &fatjet_m, &b_fatjet_m);
  treein->SetBranchAddress("fatjet_pt", &fatjet_pt, &b_fatjet_pt);
  treein->SetBranchAddress("fatjet_phi", &fatjet_phi, &b_fatjet_phi);
  treein->SetBranchAddress("fatjet_eta", &fatjet_eta, &b_fatjet_eta);
 
  treein->SetBranchAddress("fatjet_truth_nthLeading", &fatjet_truth_nthLeading, &b_fatjet_truth_nthLeading);
  treein->SetBranchAddress("fatjet_truth_dRmatched_maxEParton_flavor", &fatjet_truth_dRmatched_maxEParton_flavor, &b_fatjet_truth_dRmatched_maxEParton_flavor);
  treein->SetBranchAddress("fatjet_truth_dRmatched_particle_flavor", &fatjet_truth_dRmatched_particle_flavor, &b_fatjet_truth_dRmatched_particle_flavor);
  treein->SetBranchAddress("fatjet_truth_dRmatched_particle_dR", &fatjet_truth_dRmatched_particle_dR, &b_fatjet_truth_dRmatched_particle_dR);
  treein->SetBranchAddress("fatjet_truth_dRmatched_topBChild", &fatjet_truth_dRmatched_topBChild, &b_fatjet_truth_dRmatched_topBChild);
  treein->SetBranchAddress("fatjet_truth_dRmatched_nWZChildren", &fatjet_truth_dRmatched_nWZChildren, &b_fatjet_truth_dRmatched_nWZChildren);
  treein->SetBranchAddress("fatjet_dRmatched_particle_dR", &fatjet_dRmatched_particle_dR, &b_fatjet_dRmatched_particle_dR);
  treein->SetBranchAddress("fatjet_dRmatched_nMatchedWZChildren", &fatjet_dRmatched_nMatchedWZChildren, &b_fatjet_dRmatched_nMatchedWZChildren);
  treein->SetBranchAddress("fatjet_JetpTCorrByCalibratedTAMass", &fatjet_JetpTCorrByCalibratedTAMass, &b_fatjet_JetpTCorrByCalibratedTAMass);
  treein->SetBranchAddress("fatjet_NUngroomedSubjets", &fatjet_NUngroomedSubjets, &b_fatjet_NUngroomedSubjets);
  treein->SetBranchAddress("fatjet_numUngroomedCons", &fatjet_numUngroomedCons, &b_fatjet_numUngroomedCons);
  treein->SetBranchAddress("fatjet_Ntrk500", &fatjet_Ntrk500, &b_fatjet_Ntrk500);
  treein->SetBranchAddress("fatjet_Ntrk1000", &fatjet_Ntrk1000, &b_fatjet_Ntrk1000);

  // truth matching
  treein->SetBranchAddress("fatjet_nthLeading", &fatjet_nthLeading, &b_fatjet_nthLeading);
  treein->SetBranchAddress("fatjet_dRmatched_reco_truth", &fatjets_dRmatched_reco_truth, &b_fatjets_dRmatched_reco_truth);
  treein->SetBranchAddress("fatjet_dRmatched_particle_flavor", &fatjet_dRmatched_particle_flavor, &b_fatjet_dRmatched_particle_flavor);
  treein->SetBranchAddress("fatjet_ghost_assc_flavor", &fatjet_ghost_assc_flavor, &b_fatjet_ghost_assc_flavor);

  treein->SetBranchAddress("fatjet_dRmatched_maxEParton_flavor", &fatjet_dRmatched_maxEParton_flavor, &b_fatjet_dRmatched_maxEParton_flavor);
  treein->SetBranchAddress("fatjet_dRmatched_topBChild", &fatjet_dRmatched_topBChild, &b_fatjet_dRmatched_topBChild);

  if (m_extraTruth == true ){
    
    treein->SetBranchAddress("fatjet_truth_dRmatched_topBChild_dR", &fatjet_truth_dRmatched_topBChild_dR, &b_fatjet_truth_dRmatched_topBChild_dR);
    treein->SetBranchAddress("fatjet_truth_dRmatched_WZChild1_dR", &fatjet_truth_dRmatched_WZChild1_dR, &b_fatjet_truth_dRmatched_WZChild1_dR);
    treein->SetBranchAddress("fatjet_truth_dRmatched_WZChild2_dR", &fatjet_truth_dRmatched_WZChild2_dR, &b_fatjet_truth_dRmatched_WZChild2_dR);
    treein->SetBranchAddress("fatjet_dRmatched_topBChild_dR", &fatjet_dRmatched_topBChild_dR, &b_fatjet_dRmatched_topBChild_dR);
    treein->SetBranchAddress("fatjet_dRmatched_WZChild1_dR", &fatjet_dRmatched_WZChild1_dR, &b_fatjet_dRmatched_WZChild1_dR);
    treein->SetBranchAddress("fatjet_dRmatched_WZChild2_dR", &fatjet_dRmatched_WZChild2_dR, &b_fatjet_dRmatched_WZChild2_dR);

  }
  
  //treein->SetBranchAddress("fatjet_dRmatched_nQuarkChildren", &fatjet_dRmatched_nQuarkChildren, &b_fatjet_dRmatched_nQuarkChildren);



  treein->SetBranchAddress("fatjet_eta_detector", &fatjet_eta_detector, &b_fatjet_eta_detector);

  if(m_found_nSubstructure_branches){
    treein->SetBranchAddress("fatjet_Tau1_wta", &fatjet_Tau1_wta, &b_fatjet_Tau1_wta);
    treein->SetBranchAddress("fatjet_Tau2_wta", &fatjet_Tau2_wta, &b_fatjet_Tau2_wta);
    treein->SetBranchAddress("fatjet_Tau3_wta", &fatjet_Tau3_wta, &b_fatjet_Tau3_wta);
    treein->SetBranchAddress("fatjet_Tau21_wta", &fatjet_Tau21_wta, &b_fatjet_Tau21_wta);
    treein->SetBranchAddress("fatjet_Tau32_wta", &fatjet_Tau32_wta, &b_fatjet_Tau32_wta);
    treein->SetBranchAddress("fatjet_ECF1", &fatjet_ECF1, &b_fatjet_ECF1);
    treein->SetBranchAddress("fatjet_ECF2", &fatjet_ECF2, &b_fatjet_ECF2);
    treein->SetBranchAddress("fatjet_ECF3", &fatjet_ECF3, &b_fatjet_ECF3);
    treein->SetBranchAddress("fatjet_C2", &fatjet_C2, &b_fatjet_C2);
    treein->SetBranchAddress("fatjet_D2", &fatjet_D2, &b_fatjet_D2);
    treein->SetBranchAddress("fatjet_Angularity", &fatjet_Angularity, &b_fatjet_Angularity);
    treein->SetBranchAddress("fatjet_Aplanarity", &fatjet_Aplanarity, &b_fatjet_Aplanarity);
    treein->SetBranchAddress("fatjet_Dip12", &fatjet_Dip12, &b_fatjet_Dip12);
    treein->SetBranchAddress("fatjet_FoxWolfram20", &fatjet_FoxWolfram20, &b_fatjet_FoxWolfram20);
    treein->SetBranchAddress("fatjet_KtDR", &fatjet_KtDR, &b_fatjet_KtDR);
    treein->SetBranchAddress("fatjet_PlanarFlow", &fatjet_PlanarFlow, &b_fatjet_PlanarFlow);
    treein->SetBranchAddress("fatjet_Sphericity", &fatjet_Sphericity, &b_fatjet_Sphericity);
    treein->SetBranchAddress("fatjet_Split12", &fatjet_Split12, &b_fatjet_Split12);
    treein->SetBranchAddress("fatjet_Split23", &fatjet_Split23, &b_fatjet_Split23);
    treein->SetBranchAddress("fatjet_ThrustMaj", &fatjet_ThrustMaj, &b_fatjet_ThrustMaj);
    treein->SetBranchAddress("fatjet_ThrustMin", &fatjet_ThrustMin, &b_fatjet_ThrustMin);
    treein->SetBranchAddress("fatjet_ZCut12", &fatjet_ZCut12, &b_fatjet_ZCut12);
    treein->SetBranchAddress("fatjet_Qw",&fatjet_Qw,&b_fatjet_Qw);
    treein->SetBranchAddress("fatjet_Mu12",&fatjet_Mu12,&b_fatjet_Mu12);

    treein->SetBranchAddress("fatjet_TrackAssistedMassUnCalibrated",&fatjet_TrackAssistedMassUnCalibrated,&b_fatjet_TrackAssistedMassUnCalibrated);
    treein->SetBranchAddress("fatjet_TrackAssistedMassCalibrated",&fatjet_TrackAssistedMassCalibrated,&b_fatjet_TrackAssistedMassCalibrated);
    treein->SetBranchAddress("fatjet_CaloTACombinedMass",&fatjet_CaloTACombinedMass,&b_fatjet_CaloTACombinedMass);

    treein->SetBranchAddress("fatjet_numConstituents",&fatjet_numConstituents,&b_fatjet_numConstituents);
    treein->SetBranchAddress("fatjet_NTrimSubjets",&fatjet_NTrimSubjets,&b_fatjet_NTrimSubjets);
    
    treein->SetBranchAddress("fatjet_M2beta1",       &fatjet_M2beta1,       &b_fatjet_M2beta1);
    treein->SetBranchAddress("fatjet_N2beta1",       &fatjet_N2beta1,       &b_fatjet_N2beta1);
    treein->SetBranchAddress("fatjet_D2alpha1beta2", &fatjet_D2alpha1beta2, &b_fatjet_D2alpha1beta2);
    treein->SetBranchAddress("fatjet_N3beta2",       &fatjet_N3beta2,       &b_fatjet_N3beta2);

  }

  if(m_found_nSftDrop_branches){
    treein->SetBranchAddress("fatjet_MSoftDropBm20Zp001",&fatjet_MSoftDropBm20Zp001,  &b_fatjet_MSoftDropBm20Zp001   );
    treein->SetBranchAddress("fatjet_MSoftDropBm20Zp005",&fatjet_MSoftDropBm20Zp005,  &b_fatjet_MSoftDropBm20Zp005   );
    treein->SetBranchAddress("fatjet_MSoftDropBm20Zp010",&fatjet_MSoftDropBm20Zp010,  &b_fatjet_MSoftDropBm20Zp010   );
    treein->SetBranchAddress("fatjet_MSoftDropBm20Zp050",&fatjet_MSoftDropBm20Zp050,  &b_fatjet_MSoftDropBm20Zp050   );
    treein->SetBranchAddress("fatjet_MSoftDropBm20Zp100",&fatjet_MSoftDropBm20Zp100,  &b_fatjet_MSoftDropBm20Zp100   );
    treein->SetBranchAddress("fatjet_MSoftDropBm10Zp001",&fatjet_MSoftDropBm10Zp001,  &b_fatjet_MSoftDropBm10Zp001   );
    treein->SetBranchAddress("fatjet_MSoftDropBm10Zp005",&fatjet_MSoftDropBm10Zp005,  &b_fatjet_MSoftDropBm10Zp005   );
    treein->SetBranchAddress("fatjet_MSoftDropBm10Zp010",&fatjet_MSoftDropBm10Zp010,  &b_fatjet_MSoftDropBm10Zp010   );
    treein->SetBranchAddress("fatjet_MSoftDropBm10Zp050",&fatjet_MSoftDropBm10Zp050,  &b_fatjet_MSoftDropBm10Zp050   );
    treein->SetBranchAddress("fatjet_MSoftDropBm10Zp100",&fatjet_MSoftDropBm10Zp100,  &b_fatjet_MSoftDropBm10Zp100   );
    treein->SetBranchAddress("fatjet_MSoftDropBm05Zp001",&fatjet_MSoftDropBm05Zp001,  &b_fatjet_MSoftDropBm05Zp001   );
    treein->SetBranchAddress("fatjet_MSoftDropBm05Zp005",&fatjet_MSoftDropBm05Zp005,  &b_fatjet_MSoftDropBm05Zp005   );
    treein->SetBranchAddress("fatjet_MSoftDropBm05Zp010",&fatjet_MSoftDropBm05Zp010,  &b_fatjet_MSoftDropBm05Zp010   );
    treein->SetBranchAddress("fatjet_MSoftDropBm05Zp050",&fatjet_MSoftDropBm05Zp050,  &b_fatjet_MSoftDropBm05Zp050   );
    treein->SetBranchAddress("fatjet_MSoftDropBm05Zp100",&fatjet_MSoftDropBm05Zp100,  &b_fatjet_MSoftDropBm05Zp100   );
    treein->SetBranchAddress("fatjet_MSoftDropBp00Zp001",&fatjet_MSoftDropBp00Zp001,  &b_fatjet_MSoftDropBp00Zp001   );
    treein->SetBranchAddress("fatjet_MSoftDropBp00Zp005",&fatjet_MSoftDropBp00Zp005,  &b_fatjet_MSoftDropBp00Zp005   );
    treein->SetBranchAddress("fatjet_MSoftDropBp00Zp010",&fatjet_MSoftDropBp00Zp010,  &b_fatjet_MSoftDropBp00Zp010   );
    treein->SetBranchAddress("fatjet_MSoftDropBp00Zp050",&fatjet_MSoftDropBp00Zp050,  &b_fatjet_MSoftDropBp00Zp050   );
    treein->SetBranchAddress("fatjet_MSoftDropBp00Zp100",&fatjet_MSoftDropBp00Zp100,  &b_fatjet_MSoftDropBp00Zp100   );
    /*
    treein->SetBranchAddress("fatjet_MSoftDropBp05Zp001",&fatjet_MSoftDropBp05Zp001,  &b_fatjet_MSoftDropBp05Zp001   );
    treein->SetBranchAddress("fatjet_MSoftDropBp05Zp005",&fatjet_MSoftDropBp05Zp005,  &b_fatjet_MSoftDropBp05Zp005   );
    treein->SetBranchAddress("fatjet_MSoftDropBp05Zp010",&fatjet_MSoftDropBp05Zp010,  &b_fatjet_MSoftDropBp05Zp010   );
    treein->SetBranchAddress("fatjet_MSoftDropBp05Zp050",&fatjet_MSoftDropBp05Zp050,  &b_fatjet_MSoftDropBp05Zp050   );
    treein->SetBranchAddress("fatjet_MSoftDropBp05Zp100",&fatjet_MSoftDropBp05Zp100,  &b_fatjet_MSoftDropBp05Zp100   );
    treein->SetBranchAddress("fatjet_MSoftDropBp10Zp001",&fatjet_MSoftDropBp10Zp001,  &b_fatjet_MSoftDropBp10Zp001   );
    treein->SetBranchAddress("fatjet_MSoftDropBp10Zp005",&fatjet_MSoftDropBp10Zp005,  &b_fatjet_MSoftDropBp10Zp005   );
    treein->SetBranchAddress("fatjet_MSoftDropBp10Zp010",&fatjet_MSoftDropBp10Zp010,  &b_fatjet_MSoftDropBp10Zp010   );
    treein->SetBranchAddress("fatjet_MSoftDropBp10Zp050",&fatjet_MSoftDropBp10Zp050,  &b_fatjet_MSoftDropBp10Zp050   );
    treein->SetBranchAddress("fatjet_MSoftDropBp10Zp100",&fatjet_MSoftDropBp10Zp100,  &b_fatjet_MSoftDropBp10Zp100   );
    treein->SetBranchAddress("fatjet_MSoftDropBp20Zp001",&fatjet_MSoftDropBp20Zp001,  &b_fatjet_MSoftDropBp20Zp001   );
    treein->SetBranchAddress("fatjet_MSoftDropBp20Zp005",&fatjet_MSoftDropBp20Zp005,  &b_fatjet_MSoftDropBp20Zp005   );
    treein->SetBranchAddress("fatjet_MSoftDropBp20Zp010",&fatjet_MSoftDropBp20Zp010,  &b_fatjet_MSoftDropBp20Zp010   );
    treein->SetBranchAddress("fatjet_MSoftDropBp20Zp050",&fatjet_MSoftDropBp20Zp050,  &b_fatjet_MSoftDropBp20Zp050   );
    treein->SetBranchAddress("fatjet_MSoftDropBp20Zp100",&fatjet_MSoftDropBp20Zp100,  &b_fatjet_MSoftDropBp20Zp100   );
    */

  }
  
  if(m_found_nShowerDeconstruction_branches){
    treein->SetBranchAddress("fatjet_SDw_win20_btag0", &fatjet_SDw_win20_btag0, &b_fatjet_SDw_win20_btag0);
    treein->SetBranchAddress("fatjet_SDw_win20_btag1", &fatjet_SDw_win20_btag1, &b_fatjet_SDw_win20_btag1);
    treein->SetBranchAddress("fatjet_SDw_win25_btag0", &fatjet_SDw_win25_btag0, &b_fatjet_SDw_win25_btag0);
    treein->SetBranchAddress("fatjet_SDw_win25_btag1", &fatjet_SDw_win25_btag1, &b_fatjet_SDw_win25_btag1);
    treein->SetBranchAddress("fatjet_SDz_win20_btag0", &fatjet_SDz_win20_btag0, &b_fatjet_SDz_win20_btag0);
    treein->SetBranchAddress("fatjet_SDz_win20_btag1", &fatjet_SDz_win20_btag1, &b_fatjet_SDz_win20_btag1);
    treein->SetBranchAddress("fatjet_SDz_win25_btag0", &fatjet_SDz_win25_btag0, &b_fatjet_SDz_win25_btag0);
    treein->SetBranchAddress("fatjet_SDz_win25_btag1", &fatjet_SDz_win25_btag1, &b_fatjet_SDz_win25_btag1);
    treein->SetBranchAddress("fatjet_SDt_win50_btag0", &fatjet_SDt_win50_btag0, &b_fatjet_SDt_win50_btag0);
    treein->SetBranchAddress("fatjet_SDt_win50_btag1", &fatjet_SDt_win50_btag1, &b_fatjet_SDt_win50_btag1);
    treein->SetBranchAddress("fatjet_SDt_win55_btag0", &fatjet_SDt_win55_btag0, &b_fatjet_SDt_win55_btag0);
    treein->SetBranchAddress("fatjet_SDt_win55_btag1", &fatjet_SDt_win55_btag1, &b_fatjet_SDt_win55_btag1);
  
    treein->SetBranchAddress("fatjet_SDt_Dcut1", &fatjet_SDt_Dcut1, &b_fatjet_SDt_Dcut1);
    treein->SetBranchAddress("fatjet_SDt_Dcut2", &fatjet_SDt_Dcut2, &b_fatjet_SDt_Dcut2);

  }

  // For SD: Added the clusters, track jets

  if (m_saved_clusters) {
    treein->SetBranchStatus("fatjet_assoc_cluster_E",1);
    treein->SetBranchStatus("fatjet_assoc_cluster_pt",1);
    treein->SetBranchStatus("fatjet_assoc_cluster_eta",1);
    treein->SetBranchStatus("fatjet_assoc_cluster_phi",1);

    treein->SetBranchAddress("fatjet_assoc_cluster_E",&fatjet_assoc_cluster_E,&b_fatjet_assoc_cluster_E);
    treein->SetBranchAddress("fatjet_assoc_cluster_pt",&fatjet_assoc_cluster_pt,&b_fatjet_assoc_cluster_pt);
    treein->SetBranchAddress("fatjet_assoc_cluster_eta",&fatjet_assoc_cluster_eta,&b_fatjet_assoc_cluster_eta);
    treein->SetBranchAddress("fatjet_assoc_cluster_phi",&fatjet_assoc_cluster_phi,&b_fatjet_assoc_cluster_phi);
  }

  if (m_saved_trackjets) {
    treein->SetBranchStatus("trackJet_E", 1);
    treein->SetBranchStatus("trackJet_pt", 1);
    treein->SetBranchStatus("trackJet_phi", 1);
    treein->SetBranchStatus("trackJet_eta", 1);
    treein->SetBranchStatus("trackJet_MV2c20", 1);

    treein->SetBranchAddress("trackJet_E", &trackJet_E ,&b_trackJet_E );
    treein->SetBranchAddress("trackJet_pt", &trackJet_pt ,&b_trackJet_pt );
    treein->SetBranchAddress("trackJet_phi", &trackJet_phi ,&b_trackJet_phi );
    treein->SetBranchAddress("trackJet_eta", &trackJet_eta ,&b_trackJet_eta );
    treein->SetBranchAddress("trackJet_MV2c20", &trackJet_MV2c20 ,&b_trackJet_MV2c20 );
  }

  // ===================================================================
  // HEPTopTagger
  HTT_pt_arr.resize(m_nHTTConfigs, 0);
  HTT_eta_arr.resize(m_nHTTConfigs, 0);
  HTT_phi_arr.resize(m_nHTTConfigs, 0);
  HTT_m_arr.resize(m_nHTTConfigs, 0);

  HTT_m12_arr.resize(m_nHTTConfigs, 0);
  HTT_m13_arr.resize(m_nHTTConfigs, 0);
  HTT_m23_arr.resize(m_nHTTConfigs, 0);
  HTT_m123_arr.resize(m_nHTTConfigs, 0);
  HTT_m23m123_arr.resize(m_nHTTConfigs, 0);
  HTT_atan1312_arr.resize(m_nHTTConfigs, 0);

  HTT_nTopCands_arr.resize(m_nHTTConfigs, 0);
  HTT_tagged_arr.resize(m_nHTTConfigs, 0);

  HTT_dRW1W2_arr.resize(m_nHTTConfigs, 0);
  HTT_dRW1B_arr.resize(m_nHTTConfigs, 0);
  HTT_dRW2B_arr.resize(m_nHTTConfigs, 0);
  HTT_dRmaxW1W2B_arr.resize(m_nHTTConfigs, 0);
  HTT_cosThetaW1W2_arr.resize(m_nHTTConfigs, 0);
  HTT_cosTheta12_arr.resize(m_nHTTConfigs, 0);

  b_HTT_pt_arr.resize(m_nHTTConfigs, 0);
  b_HTT_eta_arr.resize(m_nHTTConfigs, 0);
  b_HTT_phi_arr.resize(m_nHTTConfigs, 0);
  b_HTT_m_arr.resize(m_nHTTConfigs, 0);

  b_HTT_m12_arr.resize(m_nHTTConfigs, 0);
  b_HTT_m13_arr.resize(m_nHTTConfigs, 0);
  b_HTT_m23_arr.resize(m_nHTTConfigs, 0);
  b_HTT_m123_arr.resize(m_nHTTConfigs, 0);
  b_HTT_m23m123_arr.resize(m_nHTTConfigs, 0);
  b_HTT_atan1312_arr.resize(m_nHTTConfigs, 0);

  b_HTT_nTopCands_arr.resize(m_nHTTConfigs, 0);
  b_HTT_tagged_arr.resize(m_nHTTConfigs, 0);

  b_HTT_dRW1W2_arr.resize(m_nHTTConfigs, 0);
  b_HTT_dRW1B_arr.resize(m_nHTTConfigs, 0);
  b_HTT_dRW2B_arr.resize(m_nHTTConfigs, 0);
  b_HTT_dRmaxW1W2B_arr.resize(m_nHTTConfigs, 0);
  b_HTT_cosThetaW1W2_arr.resize(m_nHTTConfigs, 0);
  b_HTT_cosTheta12_arr.resize(m_nHTTConfigs, 0);

  for(int i_conf=0; i_conf<m_nHTTConfigs; i_conf++){
    treein->SetBranchStatus( Form( "HTT_*_%s", m_HTTConfigs[i_conf].c_str()), 1, &m_found_nHTTConfig_branches[i_conf] );
    if(m_found_nHTTConfig_branches[i_conf] == 18){
      m_HEPTopTagger = true;
      treein->SetBranchAddress( Form( "HTT_pt_%s",            m_HTTConfigs[i_conf].c_str()), &HTT_pt_arr[i_conf],           &b_HTT_pt_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_eta_%s",           m_HTTConfigs[i_conf].c_str()), &HTT_eta_arr[i_conf],          &b_HTT_eta_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_phi_%s",           m_HTTConfigs[i_conf].c_str()), &HTT_phi_arr[i_conf],          &b_HTT_phi_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_m_%s",             m_HTTConfigs[i_conf].c_str()), &HTT_m_arr[i_conf],            &b_HTT_m_arr[i_conf]);

      treein->SetBranchAddress( Form( "HTT_m12_%s",           m_HTTConfigs[i_conf].c_str()), &HTT_m12_arr[i_conf],          &b_HTT_m12_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_m13_%s",           m_HTTConfigs[i_conf].c_str()), &HTT_m13_arr[i_conf],          &b_HTT_m13_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_m23_%s",           m_HTTConfigs[i_conf].c_str()), &HTT_m23_arr[i_conf],          &b_HTT_m23_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_m123_%s",          m_HTTConfigs[i_conf].c_str()), &HTT_m123_arr[i_conf],         &b_HTT_m123_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_m23m123_%s",       m_HTTConfigs[i_conf].c_str()), &HTT_m23m123_arr[i_conf],      &b_HTT_m23m123_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_atan1312_%s",      m_HTTConfigs[i_conf].c_str()), &HTT_atan1312_arr[i_conf],     &b_HTT_atan1312_arr[i_conf]);

      treein->SetBranchAddress( Form( "HTT_nTopCands_%s",     m_HTTConfigs[i_conf].c_str()), &HTT_nTopCands_arr[i_conf],    &b_HTT_nTopCands_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_tagged_%s",        m_HTTConfigs[i_conf].c_str()), &HTT_tagged_arr[i_conf],       &b_HTT_tagged_arr[i_conf]);

      treein->SetBranchAddress( Form( "HTT_dRW1W2_%s",        m_HTTConfigs[i_conf].c_str()), &HTT_dRW1W2_arr[i_conf],       &b_HTT_dRW1W2_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_dRW1B_%s",         m_HTTConfigs[i_conf].c_str()), &HTT_dRW1B_arr[i_conf],        &b_HTT_dRW1B_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_dRW2B_%s",         m_HTTConfigs[i_conf].c_str()), &HTT_dRW2B_arr[i_conf],        &b_HTT_dRW2B_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_dRmaxW1W2B_%s",    m_HTTConfigs[i_conf].c_str()), &HTT_dRmaxW1W2B_arr[i_conf],    &b_HTT_dRmaxW1W2B_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_cosThetaW1W2_%s",  m_HTTConfigs[i_conf].c_str()), &HTT_cosThetaW1W2_arr[i_conf], &b_HTT_cosThetaW1W2_arr[i_conf]);
      treein->SetBranchAddress( Form( "HTT_cosTheta12_%s",    m_HTTConfigs[i_conf].c_str()), &HTT_cosTheta12_arr[i_conf],   &b_HTT_cosTheta12_arr[i_conf]);
    }
  }
  
  if(m_HEPTopTagger){
    treein->SetBranchStatus("HTT_CA15_*", 1);
  
    treein->SetBranchStatus("fatjet_SDt_CA15_Dcut1", "1");
    treein->SetBranchStatus("fatjet_SDt_CA15_Dcut2", "1");
    treein->SetBranchStatus("fatjet_SDt_CA15_uncalib", "1");
    treein->SetBranchStatus("fatjet_SDt_CA15_force3", "1");
    treein->SetBranchStatus("fatjet_SDt_CA15_combined", "1");

    if ( m_extraTruth == true ){
      treein->SetBranchStatus("HTT_CA15_dRmatched_topBChild_dR", "1");
      treein->SetBranchStatus("HTT_CA15_dRmatched_WZChild1_dR", "1");
      treein->SetBranchStatus("HTT_CA15_dRmatched_WZChild2_dR", "1");

      treein->SetBranchAddress("HTT_CA15_dRmatched_topBChild_dR", &HTT_CA15_dRmatched_topBChild_dR, &b_HTT_CA15_dRmatched_topBChild_dR);
      treein->SetBranchAddress("HTT_CA15_dRmatched_WZChild1_dR", &HTT_CA15_dRmatched_WZChild1_dR, &b_HTT_CA15_dRmatched_WZChild1_dR);
      treein->SetBranchAddress("HTT_CA15_dRmatched_WZChild2_dR", &HTT_CA15_dRmatched_WZChild2_dR, &b_HTT_CA15_dRmatched_WZChild2_dR);

    }


    treein->SetBranchAddress("HTT_CA15_pt",           &HTT_CA15_pt,           &b_HTT_CA15_pt);
    treein->SetBranchAddress("HTT_CA15_eta",          &HTT_CA15_eta,          &b_HTT_CA15_eta);
    treein->SetBranchAddress("HTT_CA15_phi",          &HTT_CA15_phi,          &b_HTT_CA15_phi);
    treein->SetBranchAddress("HTT_CA15_m",            &HTT_CA15_m,            &b_HTT_CA15_m);
    treein->SetBranchAddress("HTT_CA15_trk_bjets_n",  &HTT_CA15_trk_bjets_n,  &b_HTT_CA15_trk_bjets_n);

    treein->SetBranchAddress("fatjet_SDt_CA15_uncalib", &fatjet_SDt_CA15_uncalib, &b_fatjet_SDt_CA15_uncalib);
    treein->SetBranchAddress("fatjet_SDt_CA15_force3", &fatjet_SDt_CA15_force3, &b_fatjet_SDt_CA15_force3);
    treein->SetBranchAddress("fatjet_SDt_CA15_combined", &fatjet_SDt_CA15_combined, &b_fatjet_SDt_CA15_combined);
    
    treein->SetBranchAddress("fatjet_SDt_CA15_Dcut1", &fatjet_SDt_CA15_Dcut1, &b_fatjet_SDt_CA15_Dcut1);
    treein->SetBranchAddress("fatjet_SDt_CA15_Dcut2", &fatjet_SDt_CA15_Dcut2, &b_fatjet_SDt_CA15_Dcut2);
    treein->SetBranchAddress("HTT_CA15_nthLeading", &HTT_CA15_nthLeading, &b_HTT_CA15_nthLeading);
    treein->SetBranchAddress("HTT_CA15_dRmatched_reco_truth", &HTT_CA15_dRmatched_reco_truth, &b_HTT_CA15_dRmatched_reco_truth);
    treein->SetBranchAddress("HTT_CA15_dRmatched_maxEParton_flavor", &HTT_CA15_dRmatched_maxEParton_flavor, &b_HTT_CA15_dRmatched_maxEParton_flavor);
    treein->SetBranchAddress("HTT_CA15_dRmatched_particle_flavor", &HTT_CA15_dRmatched_particle_flavor, &b_HTT_CA15_dRmatched_particle_flavor);
    treein->SetBranchAddress("HTT_CA15_dRmatched_particle_dR", &HTT_CA15_dRmatched_particle_dR, &b_HTT_CA15_dRmatched_particle_dR);
    treein->SetBranchAddress("HTT_CA15_dRmatched_topBChild", &HTT_CA15_dRmatched_topBChild, &b_HTT_CA15_dRmatched_topBChild);
    treein->SetBranchAddress("HTT_CA15_dRmatched_nMatchedWZChildren", &HTT_CA15_dRmatched_nMatchedWZChildren, &b_HTT_CA15_dRmatched_nMatchedWZChildren);

  }
  // HEPTopTagger
  // ===================================================================

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Flattener :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  // Get combined mass weights

  h2_CaloResolution_map.clear();
  h2_TAresolution_map.clear();
  h2_TAbinned_correlation_map.clear();
  h2_CaloResolution_mapWprime.clear();
  h2_TAresolution_mapWprime.clear();
  h2_TAbinned_correlation_mapWprime.clear();

  TString CaloFileName;
  CaloFileName = "$ROOTCOREBIN/data/NTupleFlattener/CaloResMapSmoothQCD.root";
  TString TAFileName; 
  TAFileName = "$ROOTCOREBIN/data/NTupleFlattener/TaResMapSmoothQCD.root";
  TString CorrFileName;
  CorrFileName = "$ROOTCOREBIN/data/NTupleFlattener/Correlations_TA_QCD_coarserbinning2_smoothed.root";
  TString CaloFileNameWprime;
  CaloFileNameWprime = "$ROOTCOREBIN/data/NTupleFlattener/CaloResMapSmoothWprime.root";
  TString TAFileNameWprime;
  TAFileNameWprime = "$ROOTCOREBIN/data/NTupleFlattener/TaResMapSmoothWprime.root";
  TString CorrFileNameWprime;
  CorrFileNameWprime = "$ROOTCOREBIN/data/NTupleFlattener/Final_smoothed_WCorrmap.root";


  TFile *CaloResFile;
  CaloResFile = TFile::Open(CaloFileName);
  TFile *TAResFile;
  TAResFile = TFile::Open(TAFileName);
  TFile *CorrFile;
  CorrFile = TFile::Open(CorrFileName);
  TFile *CaloResFileWprime;
  CaloResFileWprime = TFile::Open(CaloFileNameWprime);
  TFile *TAResFileWprime;
  TAResFileWprime = TFile::Open(TAFileNameWprime);
  TFile *CorrFileWprime;
  CorrFileWprime = TFile::Open(CorrFileNameWprime);


  if (!CaloResFile) std::cout << "COULD NOT OPEN CALO RES FILE" << std::endl; 
  if (!TAResFile) std::cout << "COULD NOT OPEN TA RES FILE" << std::endl;
  if (!CorrFile) std::cout << "COULD NOT OPEN CORRELATION FILE" << std::endl;

  TH2D* mapCaloRes = 0;
  TH2D* mapTARes = 0;
  TH2D* mapCorr = 0;
  TH2D* mapCaloResWprime = 0;
  TH2D* mapTAResWprime = 0;
  TH2D* mapCorrWprime = 0;

  TString caloName = "h2_fit_IQRMed_jms_vs_recopt_vs_recomOpt_etadetbin1_smoothed";
  TString taName = "h2_fit_IQRMed_jtrkms_vs_recopt_vs_recomOpt_etadetbin1_smoothed";
  TString corrName = "h2_correlation_response_smoothed";
  

  mapCaloRes = (TH2D*)CaloResFile->Get(caloName);
  mapTARes = (TH2D*)TAResFile->Get(taName);
  mapCorr = (TH2D*)CorrFile->Get(corrName);
  mapCaloResWprime = (TH2D*)CaloResFileWprime->Get(caloName);
  mapTAResWprime = (TH2D*)TAResFileWprime->Get(taName);
  mapCorrWprime = (TH2D*)CorrFileWprime->Get(corrName);

  if(!mapCaloRes) std::cout<<"ERROR: could not open calo-mass resolution map : "<< std::endl; 
  if(!mapTARes) std::cout<<"ERROR: could not open ta-mass resolution map : "<<std::endl;
  if(!mapCorr) std::cout<<"ERROR: could not open correlation map : "<<std::endl;

  h2_CaloResolution_map.push_back(mapCaloRes);
  h2_TAresolution_map.push_back(mapTARes);
  h2_TAbinned_correlation_map.push_back(mapCorr);
  h2_CaloResolution_mapWprime.push_back(mapCaloResWprime);
  h2_TAresolution_mapWprime.push_back(mapTAResWprime);
  h2_TAbinned_correlation_mapWprime.push_back(mapCorrWprime);

  if (jet_signal_flavor.compare(str_top)==0) { i_jet_signal_flavor = 6; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_W)==0) {i_jet_signal_flavor = 24; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz0w)==0) {i_jet_signal_flavor = 00; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz1w)==0) {i_jet_signal_flavor = 10; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz2w)==0) {i_jet_signal_flavor = 20; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz3w)==0) {i_jet_signal_flavor = 30; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz4w)==0) {i_jet_signal_flavor = 40; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz5w)==0) {i_jet_signal_flavor = 50; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz6w)==0) {i_jet_signal_flavor = 60; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz7w)==0) {i_jet_signal_flavor = 70; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz8w)==0) {i_jet_signal_flavor = 80; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz9w)==0) {i_jet_signal_flavor = 90; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz10w)==0) {i_jet_signal_flavor = 100; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz11w)==0) {i_jet_signal_flavor = 110; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}
  else if (jet_signal_flavor.compare(str_background_jz12w)==0) {i_jet_signal_flavor = 120; std::cout << "i_jet_signal_flavor:" << i_jet_signal_flavor << std::endl;}

  wk()->tree()->GetEntries();

  m_eventCounter = 0;
  Info("initialize()", "Number of events = %lli", wk()->tree()->GetEntries() ); // print long long int

  


  // Initialise shower deconstruction algorithms 
  //N.B REMOVED, LOOK AT PREVIOUS VERSIONS

  std::cout << "Finished initialise" << std::endl; 
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Flattener :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  this->ClearInputBranches();
  this->ClearOutputBranches();
  m_eventCounter++;
  if (m_eventCounter%10000==0) std::cout << "m_eventCounter: " << m_eventCounter << std::endl;

  int ievent = wk()->treeEntry();
  (wk()->tree())->GetEntry(ievent);

  // Put the event information
  EventInfo_runNumber =        runNumber;
  EventInfo_eventNumber =      eventNumber;
  EventInfo_lumiBlock =        lumiBlock;
  EventInfo_coreFlags =        coreFlags;
  EventInfo_mcEventNumber =    mcEventNumber;
  EventInfo_mcChannelNumber =  mcChannelNumber;
  EventInfo_mcEventWeight =    mcEventWeight;
  EventInfo_ntruthJets =       ntruth_fatjets;
  EventInfo_nfatjets =         nfatjets;

  // Add here the pileup information
  EventInfo_weight_pileup = weight_pileup;
  EventInfo_NPV = NPV;
  EventInfo_actualInteractionsPerCrossing = actualInteractionsPerCrossing;
  EventInfo_averageInteractionsPerCrossing = averageInteractionsPerCrossing;

  // Add n_truth_matched jets?


  // To hold track jet information  
  std::vector<int> tracktag;
  std::vector<fastjet::PseudoJet> TrackJets;
  

  // Loop over jets
  for (unsigned int ijet=0; ijet<fatjet_pt->size();ijet++){
    // Leading and sub-leading jets. Are the jets ordered in pT? Check later... Should be...
    //if (ijet>=2) continue;

    // Use all the jets for now

    // Ece - question: cut on truth or reco? Update: Truth
    // Ece - question: truth mathched? Update: Keep only reco-truth jet matched ones
    // Ece - question: additional cuts, matching?
    // Don't consider the jet if it is not matched to a truth jet...

    // Truth information
    // Matching, flavor
    fjet_fatjets_dRmatched_reco_truth = fatjets_dRmatched_reco_truth -> at(ijet);

    fjet_fatjet_dRmatched_particle_flavor = fatjet_dRmatched_particle_flavor -> at(ijet);
    fjet_fatjet_ghost_assc_flavor = fatjet_ghost_assc_flavor -> at(ijet);
    fjet_truth_nthLeading = fatjet_truth_nthLeading->at(ijet);
    fjet_truth_dRmatched_maxEParton_flavor = fatjet_truth_dRmatched_maxEParton_flavor->at(ijet);
    fjet_truth_dRmatched_particle_flavor = fatjet_truth_dRmatched_particle_flavor->at(ijet);
    fjet_truth_dRmatched_particle_dR = fatjet_truth_dRmatched_particle_dR->at(ijet);
    fjet_truth_dRmatched_topBChild = fatjet_truth_dRmatched_topBChild->at(ijet);
    fjet_truth_dRmatched_nWZChildren = fatjet_truth_dRmatched_nWZChildren->at(ijet);
    fjet_dRmatched_particle_dR = fatjet_dRmatched_particle_dR->at(ijet);
    fjet_dRmatched_nMatchedWZChildren = fatjet_dRmatched_nMatchedWZChildren->at(ijet);
    fjet_JetpTCorrByCalibratedTAMass = fatjet_JetpTCorrByCalibratedTAMass->at(ijet);
    fjet_NUngroomedSubjets = fatjet_NUngroomedSubjets->at(ijet);
    fjet_numUngroomedCons = fatjet_numUngroomedCons->at(ijet);
    fjet_Ntrk500 = fatjet_Ntrk500->at(ijet);
    fjet_Ntrk1000 = fatjet_Ntrk1000->at(ijet);

    if (m_extraTruth == true) {
      fjet_truth_dRmatched_topBChild_dR = fatjet_truth_dRmatched_topBChild_dR->at(ijet);
      fjet_truth_dRmatched_WZChild1_dR = fatjet_truth_dRmatched_WZChild1_dR->at(ijet);
      fjet_truth_dRmatched_WZChild2_dR = fatjet_truth_dRmatched_WZChild2_dR->at(ijet);
      fjet_dRmatched_topBChild_dR = fatjet_dRmatched_topBChild_dR->at(ijet);
      fjet_dRmatched_WZChild1_dR = fatjet_dRmatched_WZChild1_dR->at(ijet);
      fjet_dRmatched_WZChild2_dR = fatjet_dRmatched_WZChild2_dR->at(ijet);

    }


    // Keep only reco-truth jet matched ones
    if (fjet_fatjets_dRmatched_reco_truth!=1) continue;

    pass_pt_eta_cuts = false;
    if(apply_pt_eta_cuts) pass_pt_eta_cuts = ( (fatjet_truth_pt->at(ijet)/1000 >= min_pt) && (fatjet_truth_pt->at(ijet)/1000 < max_pt)  && (fabs(fatjet_truth_eta->at(ijet))) < max_abs_eta);
    else pass_pt_eta_cuts = true;

    // Make the selection later
    if(pass_pt_eta_cuts==false) continue;

     try{ // FIXME: branches are new, might not be there yet. can be removed in future(?!)
      fjet_dRmatched_maxEParton_flavor = fatjet_dRmatched_maxEParton_flavor -> at(ijet);
      fjet_dRmatched_topBChild = fatjet_dRmatched_topBChild -> at(ijet);
      //fjet_dRmatched_nQuarkChildren = fatjet_dRmatched_nQuarkChildren -> at(ijet);
      fjet_truthJet_nthLeading = fatjet_truth_nthLeading->at(ijet);
      fjet_nthLeading = fatjet_nthLeading->at(ijet);

    }
    catch(...){ }
     
    // No matching cut here yet

    // Kinematics:
    // Add the truth kinematics later
     fjet_truthJet_E = fatjet_truth_E->at(ijet);
    fjet_truthJet_pt = fatjet_truth_pt->at(ijet);
    fjet_truthJet_eta = fatjet_truth_eta->at(ijet);
    fjet_truthJet_phi = fatjet_truth_phi->at(ijet);
    fjet_truthJet_m = fatjet_truth_m->at(ijet);
    
    // Kinematics and substructure
    fjet_E    =           fatjet_E->at(ijet);
    //fjet_m    =           fatjet_m;
    fjet_pt    =          fatjet_pt->at(ijet);
    fjet_phi    =         fatjet_phi->at(ijet);
    fjet_eta    =         fatjet_eta->at(ijet);

    TLorentzVector forMass;
    forMass.SetPtEtaPhiE( fjet_pt, fjet_eta, fjet_phi, fjet_E);
    double reco_mass = forMass.M();
    if (reco_mass < 0 ) reco_mass = 0; 

    fjet_m = reco_mass;
    
    fjet_eta_detector = fatjet_eta_detector->at(ijet);
    
    if(m_found_nSubstructure_branches){
      fjet_Tau1_wta    =    fatjet_Tau1_wta->at(ijet);
      fjet_Tau2_wta    =    fatjet_Tau2_wta->at(ijet);
      fjet_Tau3_wta    =    fatjet_Tau3_wta->at(ijet);
      fjet_Tau21_wta    =   fatjet_Tau21_wta->at(ijet);
      fjet_Tau32_wta    =   fatjet_Tau32_wta->at(ijet);
      fjet_ECF1    =        fatjet_ECF1->at(ijet);
      fjet_ECF2    =        fatjet_ECF2->at(ijet);
      fjet_ECF3    =        fatjet_ECF3->at(ijet);
      
      fjet_e2      = fjet_ECF2 / (fjet_ECF1*fjet_ECF1);
      fjet_e3      = fjet_ECF3 / (fjet_ECF1*fjet_ECF1*fjet_ECF1);

      fjet_C2    =          fatjet_C2->at(ijet);
      fjet_D2    =          fatjet_D2->at(ijet);
      fjet_Angularity    =  fatjet_Angularity->at(ijet);
      fjet_Aplanarity    =  fatjet_Aplanarity->at(ijet);
      fjet_Dip12    =       fatjet_Dip12->at(ijet);
      fjet_FoxWolfram20   = fatjet_FoxWolfram20->at(ijet);
      fjet_KtDR    =        fatjet_KtDR->at(ijet);
      fjet_PlanarFlow    =  fatjet_PlanarFlow->at(ijet);
      fjet_Sphericity    =  fatjet_Sphericity->at(ijet);
      fjet_Split12    =     fatjet_Split12->at(ijet);
      fjet_Split23    =     fatjet_Split23->at(ijet);
      fjet_ThrustMaj    =   fatjet_ThrustMaj->at(ijet);
      fjet_ThrustMin    =   fatjet_ThrustMin->at(ijet);
      fjet_ZCut12    =      fatjet_ZCut12->at(ijet);
      fjet_Qw     =         fatjet_Qw->at(ijet);
      fjet_Mu12        =    fatjet_Mu12->at(ijet);

      fjet_TrackAssistedMassUnCalibrated   = fatjet_TrackAssistedMassUnCalibrated->at(ijet);
      fjet_TrackAssistedMassCalibrated   = fatjet_TrackAssistedMassCalibrated->at(ijet);
      

      // use new combined mass weights 
      double combinedMass = calculateCombinedMass( fjet_m , fjet_TrackAssistedMassCalibrated, fjet_pt, false );
      double Uncorrcombined = unCorrelatedcalculateCombinedMass( fjet_m , fjet_TrackAssistedMassCalibrated, fjet_pt, false );
      double combinedMassWprime = calculateCombinedMass( fjet_m , fjet_TrackAssistedMassCalibrated, fjet_pt, true );
      double UncorrcombinedWprime = unCorrelatedcalculateCombinedMass( fjet_m , fjet_TrackAssistedMassCalibrated, fjet_pt, true );

      fjet_CaloTACombinedMass   =                  combinedMass; //fatjet_CaloTACombinedMass->at(ijet);
      fjet_CaloTACombinedMassUncorrelated =        Uncorrcombined;
      fjet_CaloTACombinedMassWprime =              combinedMassWprime;
      fjet_CaloTACombinedMassUncorrelatedWprime =  UncorrcombinedWprime;

      if ( fjet_CaloTACombinedMassUncorrelated > fjet_E ) fjet_JetpTCorrByCombinedMass = fjet_pt;
      else {
	fjet_JetpTCorrByCombinedMass = sqrt(fjet_E*fjet_E - fjet_CaloTACombinedMassUncorrelated*fjet_CaloTACombinedMassUncorrelated) / cosh ( fjet_eta );
      }
      
      fjet_pTResponseCombMassCorrpT = fjet_JetpTCorrByCombinedMass / fjet_truthJet_pt;
      

      fjet_numConstituents = fatjet_numConstituents->at(ijet);
      fjet_NTrimSubjets = fatjet_NTrimSubjets->at(ijet);

      fjet_M2beta1       = fatjet_M2beta1->at(ijet);
      fjet_N2beta1       = fatjet_N2beta1->at(ijet);
      fjet_D2alpha1beta2 = fatjet_D2alpha1beta2->at(ijet);
      fjet_N3beta2       = fatjet_N3beta2->at(ijet);

    }



    if(m_found_nSftDrop_branches){
      fjet_MSoftDropBm20Zp001 = fatjet_MSoftDropBm20Zp001->at(ijet);
      fjet_MSoftDropBm20Zp005 = fatjet_MSoftDropBm20Zp005->at(ijet);
      fjet_MSoftDropBm20Zp010 = fatjet_MSoftDropBm20Zp010->at(ijet);
      fjet_MSoftDropBm20Zp050 = fatjet_MSoftDropBm20Zp050->at(ijet);
      fjet_MSoftDropBm20Zp100 = fatjet_MSoftDropBm20Zp100->at(ijet);
      fjet_MSoftDropBm10Zp001 = fatjet_MSoftDropBm10Zp001->at(ijet);
      fjet_MSoftDropBm10Zp005 = fatjet_MSoftDropBm10Zp005->at(ijet);
      fjet_MSoftDropBm10Zp010 = fatjet_MSoftDropBm10Zp010->at(ijet);
      fjet_MSoftDropBm10Zp050 = fatjet_MSoftDropBm10Zp050->at(ijet);
      fjet_MSoftDropBm10Zp100 = fatjet_MSoftDropBm10Zp100->at(ijet);
      fjet_MSoftDropBm05Zp001 = fatjet_MSoftDropBm05Zp001->at(ijet);
      fjet_MSoftDropBm05Zp005 = fatjet_MSoftDropBm05Zp005->at(ijet);
      fjet_MSoftDropBm05Zp010 = fatjet_MSoftDropBm05Zp010->at(ijet);
      fjet_MSoftDropBm05Zp050 = fatjet_MSoftDropBm05Zp050->at(ijet);
      fjet_MSoftDropBm05Zp100 = fatjet_MSoftDropBm05Zp100->at(ijet);
      fjet_MSoftDropBp00Zp001 = fatjet_MSoftDropBp00Zp001->at(ijet);
      fjet_MSoftDropBp00Zp005 = fatjet_MSoftDropBp00Zp005->at(ijet);
      fjet_MSoftDropBp00Zp010 = fatjet_MSoftDropBp00Zp010->at(ijet);
      fjet_MSoftDropBp00Zp050 = fatjet_MSoftDropBp00Zp050->at(ijet);
      fjet_MSoftDropBp00Zp100 = fatjet_MSoftDropBp00Zp100->at(ijet);
      /*
      fjet_MSoftDropBp05Zp001 = fatjet_MSoftDropBp05Zp001->at(ijet);
      fjet_MSoftDropBp05Zp005 = fatjet_MSoftDropBp05Zp005->at(ijet);
      fjet_MSoftDropBp05Zp010 = fatjet_MSoftDropBp05Zp010->at(ijet);
      fjet_MSoftDropBp05Zp050 = fatjet_MSoftDropBp05Zp050->at(ijet);
      fjet_MSoftDropBp05Zp100 = fatjet_MSoftDropBp05Zp100->at(ijet);
      fjet_MSoftDropBp10Zp001 = fatjet_MSoftDropBp10Zp001->at(ijet);
      fjet_MSoftDropBp10Zp005 = fatjet_MSoftDropBp10Zp005->at(ijet);
      fjet_MSoftDropBp10Zp010 = fatjet_MSoftDropBp10Zp010->at(ijet);
      fjet_MSoftDropBp10Zp050 = fatjet_MSoftDropBp10Zp050->at(ijet);
      fjet_MSoftDropBp10Zp100 = fatjet_MSoftDropBp10Zp100->at(ijet);
      fjet_MSoftDropBp20Zp001 = fatjet_MSoftDropBp20Zp001->at(ijet);
      fjet_MSoftDropBp20Zp005 = fatjet_MSoftDropBp20Zp005->at(ijet);
      fjet_MSoftDropBp20Zp010 = fatjet_MSoftDropBp20Zp010->at(ijet);
      fjet_MSoftDropBp20Zp050 = fatjet_MSoftDropBp20Zp050->at(ijet);
      fjet_MSoftDropBp20Zp100 = fatjet_MSoftDropBp20Zp100->at(ijet);
      */

    }
    
    if(m_found_nShowerDeconstruction_branches){
      fjet_SDw_win20_btag0 = fatjet_SDw_win20_btag0->at(ijet);
      fjet_SDw_win20_btag1 = fatjet_SDw_win20_btag1->at(ijet);
      fjet_SDw_win25_btag0 = fatjet_SDw_win25_btag0->at(ijet);
      fjet_SDw_win25_btag1 = fatjet_SDw_win25_btag1->at(ijet);
      fjet_SDz_win20_btag0 = fatjet_SDz_win20_btag0->at(ijet);
      fjet_SDz_win20_btag1 = fatjet_SDz_win20_btag1->at(ijet);
      fjet_SDz_win25_btag0 = fatjet_SDz_win25_btag0->at(ijet);
      fjet_SDz_win25_btag1 = fatjet_SDz_win25_btag1->at(ijet);
      fjet_SDt_win50_btag0 = fatjet_SDt_win50_btag0->at(ijet);
      fjet_SDt_win50_btag1 = fatjet_SDt_win50_btag1->at(ijet);
      fjet_SDt_win55_btag0 = fatjet_SDt_win55_btag0->at(ijet);
      fjet_SDt_win55_btag1 = fatjet_SDt_win55_btag1->at(ijet);
      fjet_SDt_Dcut1 = fatjet_SDt_Dcut1->at(ijet);
      fjet_SDt_Dcut2 = fatjet_SDt_Dcut2->at(ijet);

    }


  // Weights
  if (i_jet_signal_flavor == 6){
    fjet_xsec_filteff_numevents = 1;
    fjet_testing_weight_pt = GetWeight_SigToBkg_Pt_top(fjet_truthJet_pt/1000.0);
    fjet_testing_weight_pt_dR = GetWeight_SigToBkg_Pt_top_dR(fjet_truthJet_pt/1000.0);
  }

 else if (i_jet_signal_flavor == 24) {
    fjet_xsec_filteff_numevents = 1;
    fjet_testing_weight_pt = GetWeight_SigToBkg_Pt_W(fjet_truthJet_pt/1000.0);
    fjet_testing_weight_pt_dR = GetWeight_SigToBkg_Pt_W_dR(fjet_truthJet_pt/1000.0);
  }


 else if (i_jet_signal_flavor == 00){
   fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ0W_xsec, JZ0W_filteff, JZ0W_numevents);
   fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ0W_xsec, JZ0W_filteff, JZ0W_numevents);
 }

  else if (i_jet_signal_flavor == 10){
   fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ1W_xsec, JZ1W_filteff, JZ1W_numevents);
   fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ1W_xsec, JZ1W_filteff, JZ1W_numevents);
  }

  else if (i_jet_signal_flavor == 20){
   fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ2W_xsec, JZ2W_filteff, JZ2W_numevents);
   fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ2W_xsec, JZ2W_filteff, JZ2W_numevents);
  }

  else if (i_jet_signal_flavor == 30){
   fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ3W_xsec, JZ3W_filteff, JZ3W_numevents);
   fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ3W_xsec, JZ3W_filteff, JZ3W_numevents);
  }

  else if (i_jet_signal_flavor == 40){
   fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ4W_xsec, JZ4W_filteff, JZ4W_numevents);
   fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ4W_xsec, JZ4W_filteff, JZ4W_numevents);
  }

  else if (i_jet_signal_flavor == 50){
   fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ5W_xsec, JZ5W_filteff, JZ5W_numevents);
   fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ5W_xsec, JZ5W_filteff, JZ5W_numevents);
  }

  else if (i_jet_signal_flavor == 60){
   fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ6W_xsec, JZ6W_filteff, JZ6W_numevents);
   fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ6W_xsec, JZ6W_filteff, JZ6W_numevents);
  }

  else if (i_jet_signal_flavor == 70){
   fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ7W_xsec, JZ7W_filteff, JZ7W_numevents);
   fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ7W_xsec, JZ7W_filteff, JZ7W_numevents);
  }

  else if (i_jet_signal_flavor == 80){
   fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ8W_xsec, JZ8W_filteff, JZ8W_numevents);
   fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ8W_xsec, JZ8W_filteff, JZ8W_numevents);
  }

  else if (i_jet_signal_flavor == 90){
   fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ9W_xsec, JZ9W_filteff, JZ9W_numevents);
   fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ9W_xsec, JZ9W_filteff, JZ9W_numevents);
  }

  else if (i_jet_signal_flavor == 100){
    fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ10W_xsec, JZ10W_filteff, JZ10W_numevents);
    fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ10W_xsec, JZ10W_filteff, JZ10W_numevents);
  }

  else if (i_jet_signal_flavor == 110){
    fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ11W_xsec, JZ11W_filteff, JZ11W_numevents);
    fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ11W_xsec, JZ11W_filteff, JZ11W_numevents);
  }

  else if (i_jet_signal_flavor == 120){
    fjet_xsec_filteff_numevents = AddWeightXsecFilterEffNumEvents(JZ12W_xsec, JZ12W_filteff, JZ12W_numevents);
    fjet_testing_weight_pt = GetWeight_Bkg(EventInfo_mcEventWeight , JZ12W_xsec, JZ12W_filteff, JZ12W_numevents);
  }

  /*

    removed SD stuff

  */

  // ===================================================================
  // HEPTopTagger
  if(m_HEPTopTagger){
    
    flat_HTT_CA15_pt = HTT_CA15_pt->at(ijet);
    flat_HTT_CA15_eta = HTT_CA15_eta->at(ijet);
    flat_HTT_CA15_phi = HTT_CA15_phi->at(ijet);
    flat_HTT_CA15_m = HTT_CA15_m->at(ijet);
    flat_HTT_CA15_trk_bjets_n = 0;//HTT_CA15_trk_bjets_n->at(ijet);

    if (fatjet_SDt_CA15_Dcut1->size() < (ijet + 1)){
      fjet_SDt_CA15_Dcut1 = -100;
      fjet_SDt_CA15_Dcut2 = -100;
      fjet_SDt_CA15_uncalib = -100;
      fjet_SDt_CA15_force3 = -100;
      fjet_SDt_CA15_combined = -100;

    }
    else {
      fjet_SDt_CA15_Dcut1 = fatjet_SDt_CA15_Dcut1->at(ijet);
      fjet_SDt_CA15_Dcut2 = fatjet_SDt_CA15_Dcut2->at(ijet);
      fjet_SDt_CA15_uncalib = fatjet_SDt_CA15_uncalib->at(ijet);
      fjet_SDt_CA15_force3 = fatjet_SDt_CA15_force3->at(ijet);
      fjet_SDt_CA15_combined = fatjet_SDt_CA15_combined->at(ijet);

    }

    flat_HTT_CA15_nthLeading = HTT_CA15_nthLeading->at(ijet);
    flat_HTT_CA15_dRmatched_reco_truth = HTT_CA15_dRmatched_reco_truth->at(ijet);
    flat_HTT_CA15_dRmatched_maxEParton_flavor = HTT_CA15_dRmatched_maxEParton_flavor->at(ijet);
    flat_HTT_CA15_dRmatched_particle_flavor = HTT_CA15_dRmatched_particle_flavor->at(ijet);
    flat_HTT_CA15_dRmatched_particle_dR = HTT_CA15_dRmatched_particle_dR->at(ijet);
    flat_HTT_CA15_dRmatched_topBChild = HTT_CA15_dRmatched_topBChild->at(ijet);
    flat_HTT_CA15_dRmatched_nMatchedWZChildren = HTT_CA15_dRmatched_nMatchedWZChildren->at(ijet);

    if (m_extraTruth){
      flat_HTT_CA15_dRmatched_topBChild_dR = HTT_CA15_dRmatched_topBChild_dR->at(ijet);
      flat_HTT_CA15_dRmatched_WZChild1_dR = HTT_CA15_dRmatched_WZChild1_dR->at(ijet);
      flat_HTT_CA15_dRmatched_WZChild2_dR = HTT_CA15_dRmatched_WZChild2_dR->at(ijet);
    }

    for(int i_conf=0; i_conf<m_nHTTConfigs; i_conf++){
      if(m_found_nHTTConfig_branches[i_conf] == 18){

	flat_HTT_pt_arr[i_conf] = HTT_pt_arr[i_conf]->at(ijet);
        flat_HTT_eta_arr[i_conf] = HTT_eta_arr[i_conf]->at(ijet);
        flat_HTT_phi_arr[i_conf] = HTT_phi_arr[i_conf]->at(ijet);
        flat_HTT_m_arr[i_conf] = HTT_m_arr[i_conf]->at(ijet); 

        flat_HTT_m12_arr[i_conf] = HTT_m12_arr[i_conf]->at(ijet); 
        flat_HTT_m13_arr[i_conf] = HTT_m13_arr[i_conf]->at(ijet);
        flat_HTT_m23_arr[i_conf] = HTT_m23_arr[i_conf]->at(ijet);
        flat_HTT_m123_arr[i_conf] = HTT_m123_arr[i_conf]->at(ijet);
        flat_HTT_m23m123_arr[i_conf] = HTT_m23m123_arr[i_conf]->at(ijet); 
        flat_HTT_atan1312_arr[i_conf] = HTT_atan1312_arr[i_conf]->at(ijet); 

        flat_HTT_nTopCands_arr[i_conf] = HTT_nTopCands_arr[i_conf]->at(ijet); 
        flat_HTT_tagged_arr[i_conf] = HTT_tagged_arr[i_conf]->at(ijet); 

        flat_HTT_dRW1W2_arr[i_conf] = HTT_dRW1W2_arr[i_conf]->at(ijet); 
        flat_HTT_dRW1B_arr[i_conf] = HTT_dRW1B_arr[i_conf]->at(ijet); 
        flat_HTT_dRW2B_arr[i_conf] = HTT_dRW2B_arr[i_conf]->at(ijet);
        flat_HTT_dRmaxW1W2B_arr[i_conf] = HTT_dRmaxW1W2B_arr[i_conf]->at(ijet); 
        flat_HTT_cosThetaW1W2_arr[i_conf] = HTT_cosThetaW1W2_arr[i_conf]->at(ijet); 
        flat_HTT_cosTheta12_arr[i_conf] = HTT_cosTheta12_arr[i_conf]->at(ijet); 

	
      }
    }
  }
  // HEPTopTagger
  // ===================================================================


    treeout->Fill();
  }
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode Flattener :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Flattener :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Flattener :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  return EL::StatusCode::SUCCESS;
}


void  Flattener :: DefineOutputBranches(TTree *treeout){

  
  
   // Event information
   treeout->Branch("EventInfo_runNumber",&EventInfo_runNumber);
   treeout->Branch("EventInfo_eventNumber",&EventInfo_eventNumber);
   treeout->Branch("EventInfo_lumiBlock",&EventInfo_lumiBlock);
   treeout->Branch("EventInfo_coreFlags",&EventInfo_coreFlags);
   treeout->Branch("EventInfo_mcEventNumber",&EventInfo_mcEventNumber);
   treeout->Branch("EventInfo_mcChannelNumber",&EventInfo_mcChannelNumber);
   treeout->Branch("EventInfo_mcEventWeight",&EventInfo_mcEventWeight);
   treeout->Branch("EventInfo_ntruthJets",&EventInfo_ntruthJets);
   treeout->Branch("EventInfo_nfatjets",&EventInfo_nfatjets);

   treeout->Branch("fjet_eta_detector", &fjet_eta_detector);

  // Add here the pileup information
   treeout->Branch("EventInfo_weight_pileup",&EventInfo_weight_pileup);
   treeout->Branch("EventInfo_NPV",&EventInfo_NPV);
   treeout->Branch("EventInfo_actualInteractionsPerCrossing",&EventInfo_actualInteractionsPerCrossing);
   treeout->Branch("EventInfo_averageInteractionsPerCrossing",&EventInfo_averageInteractionsPerCrossing);

   // Truth information
   treeout->Branch("fjet_truthJet_E",&fjet_truthJet_E);
   treeout->Branch("fjet_truthJet_pt",&fjet_truthJet_pt);
   treeout->Branch("fjet_truthJet_phi",&fjet_truthJet_phi);
   treeout->Branch("fjet_truthJet_eta",&fjet_truthJet_eta);
   treeout->Branch("fjet_truthJet_m",&fjet_truthJet_m);

   treeout->Branch("fjet_truthJet_nthLeading",&fjet_truthJet_nthLeading);


   // Truth matching information
   treeout->Branch("fjet_fatjets_dRmatched_reco_truth",&fjet_fatjets_dRmatched_reco_truth);
   treeout->Branch("fjet_fatjet_dRmatched_particle_flavor",&fjet_fatjet_dRmatched_particle_flavor);
   treeout->Branch("fjet_fatjet_ghost_assc_flavor",&fjet_fatjet_ghost_assc_flavor);
   
   treeout->Branch("fjet_dRmatched_maxEParton_flavor",&fjet_dRmatched_maxEParton_flavor);
   treeout->Branch("fjet_dRmatched_topBChild",&fjet_dRmatched_topBChild);
   treeout->Branch("fjet_truth_nthLeading",&fjet_truth_nthLeading);
   treeout->Branch("fjet_truth_dRmatched_maxEParton_flavor",&fjet_truth_dRmatched_maxEParton_flavor);
   treeout->Branch("fjet_truth_dRmatched_particle_flavor",&fjet_truth_dRmatched_particle_flavor);
   treeout->Branch("fjet_truth_dRmatched_particle_dR",&fjet_truth_dRmatched_particle_dR);
   treeout->Branch("fjet_truth_dRmatched_topBChild",&fjet_truth_dRmatched_topBChild);
   treeout->Branch("fjet_truth_dRmatched_nWZChildren",&fjet_truth_dRmatched_nWZChildren);
   treeout->Branch("fjet_dRmatched_particle_dR",&fjet_dRmatched_particle_dR);
   treeout->Branch("fjet_dRmatched_nMatchedWZChildren",&fjet_dRmatched_nMatchedWZChildren);
   
  
   treeout->Branch("fjet_truth_dRmatched_topBChild_dR",&fjet_truth_dRmatched_topBChild_dR);
   treeout->Branch("fjet_truth_dRmatched_WZChild1_dR",&fjet_truth_dRmatched_WZChild1_dR);
   treeout->Branch("fjet_truth_dRmatched_WZChild2_dR",&fjet_truth_dRmatched_WZChild2_dR);
   treeout->Branch("fjet_dRmatched_topBChild_dR",&fjet_dRmatched_topBChild_dR);
   treeout->Branch("fjet_dRmatched_WZChild1_dR",&fjet_dRmatched_WZChild1_dR);
   treeout->Branch("fjet_dRmatched_WZChild2_dR",&fjet_dRmatched_WZChild2_dR);
   

   treeout->Branch("fjet_JetpTCorrByCalibratedTAMass",&fjet_JetpTCorrByCalibratedTAMass);
   treeout->Branch("fjet_NUngroomedSubjets",&fjet_NUngroomedSubjets);
   treeout->Branch("fjet_numUngroomedCons",&fjet_numUngroomedCons);
   treeout->Branch("fjet_Ntrk500", &fjet_Ntrk500);
   treeout->Branch("fjet_Ntrk1000", &fjet_Ntrk1000);

   //treeout->Branch("fjet_dRmatched_nQuarkChildren",&fjet_dRmatched_nQuarkChildren);
   // Fat jet kinematics and substructure
   treeout->Branch("fjet_E",&fjet_E);
   treeout->Branch("fjet_pt",&fjet_pt);
   treeout->Branch("fjet_m",&fjet_m);
   treeout->Branch("fjet_phi",&fjet_phi);
   treeout->Branch("fjet_eta",&fjet_eta);
   treeout->Branch("fjet_nthLeading",&fjet_nthLeading);
   treeout->Branch("fjet_Tau1_wta",&fjet_Tau1_wta);
   treeout->Branch("fjet_Tau2_wta",&fjet_Tau2_wta);
   treeout->Branch("fjet_Tau3_wta",&fjet_Tau3_wta);
   treeout->Branch("fjet_Tau21_wta",&fjet_Tau21_wta);
   treeout->Branch("fjet_Tau32_wta",&fjet_Tau32_wta);
   treeout->Branch("fjet_ECF1",&fjet_ECF1);
   treeout->Branch("fjet_ECF2",&fjet_ECF2);
   treeout->Branch("fjet_ECF3",&fjet_ECF3);
   treeout->Branch("fjet_e2", &fjet_e2);
   treeout->Branch("fjet_e3", &fjet_e3);
   treeout->Branch("fjet_C2",&fjet_C2);
   treeout->Branch("fjet_D2",&fjet_D2);
   treeout->Branch("fjet_Angularity",&fjet_Angularity);
   treeout->Branch("fjet_Aplanarity",&fjet_Aplanarity);
   treeout->Branch("fjet_Dip12",&fjet_Dip12);
   treeout->Branch("fjet_FoxWolfram20",&fjet_FoxWolfram20);
   treeout->Branch("fjet_KtDR",&fjet_KtDR);
   treeout->Branch("fjet_PlanarFlow",&fjet_PlanarFlow);
   treeout->Branch("fjet_Sphericity",&fjet_Sphericity);
   treeout->Branch("fjet_Split12",&fjet_Split12);
   treeout->Branch("fjet_Split23",&fjet_Split23);
   treeout->Branch("fjet_ThrustMaj",&fjet_ThrustMaj);
   treeout->Branch("fjet_ThrustMin",&fjet_ThrustMin);
   treeout->Branch("fjet_ZCut12",&fjet_ZCut12);

   treeout->Branch("fjet_Qw",&fjet_Qw);
   treeout->Branch("fjet_Mu12",&fjet_Mu12);

   treeout->Branch("fjet_TrackAssistedMassUnCalibrated",&fjet_TrackAssistedMassUnCalibrated);
   treeout->Branch("fjet_TrackAssistedMassCalibrated",&fjet_TrackAssistedMassCalibrated);
   treeout->Branch("fjet_CaloTACombinedMass",&fjet_CaloTACombinedMass);
   treeout->Branch("fjet_CaloTACombinedMassUncorrelated",&fjet_CaloTACombinedMassUncorrelated);
   treeout->Branch("fjet_CaloTACombinedMassWprime",&fjet_CaloTACombinedMassWprime);
   treeout->Branch("fjet_CaloTACombinedMassUncorrelatedWprime",&fjet_CaloTACombinedMassUncorrelatedWprime);
   
   treeout->Branch("fjet_JetpTCorrByCombinedMass", &fjet_JetpTCorrByCombinedMass);
   treeout->Branch("fjet_pTResponseCombMassCorrpT", &fjet_pTResponseCombMassCorrpT);

   treeout->Branch("fjet_numConstituents", &fjet_numConstituents);
   treeout->Branch("fjet_NTrimSubjets", &fjet_NTrimSubjets);

   treeout->Branch("fjet_M2beta1", &fjet_M2beta1);
   treeout->Branch("fjet_N2beta1", &fjet_N2beta1);
   treeout->Branch("fjet_D2alpha1beta2", &fjet_D2alpha1beta2);
   treeout->Branch("fjet_N3beta2", &fjet_N3beta2);


 treeout->Branch("fjet_MSoftDropBm20Zp001",&fjet_MSoftDropBm20Zp001); 
 treeout->Branch("fjet_MSoftDropBm20Zp005",&fjet_MSoftDropBm20Zp005); 
 treeout->Branch("fjet_MSoftDropBm20Zp010",&fjet_MSoftDropBm20Zp010); 
 treeout->Branch("fjet_MSoftDropBm20Zp050",&fjet_MSoftDropBm20Zp050); 
 treeout->Branch("fjet_MSoftDropBm20Zp100",&fjet_MSoftDropBm20Zp100); 
 treeout->Branch("fjet_MSoftDropBm10Zp001",&fjet_MSoftDropBm10Zp001); 
 treeout->Branch("fjet_MSoftDropBm10Zp005",&fjet_MSoftDropBm10Zp005); 
 treeout->Branch("fjet_MSoftDropBm10Zp010",&fjet_MSoftDropBm10Zp010); 
 treeout->Branch("fjet_MSoftDropBm10Zp050",&fjet_MSoftDropBm10Zp050); 
 treeout->Branch("fjet_MSoftDropBm10Zp100",&fjet_MSoftDropBm10Zp100); 
 treeout->Branch("fjet_MSoftDropBm05Zp001",&fjet_MSoftDropBm05Zp001); 
 treeout->Branch("fjet_MSoftDropBm05Zp005",&fjet_MSoftDropBm05Zp005); 
 treeout->Branch("fjet_MSoftDropBm05Zp010",&fjet_MSoftDropBm05Zp010); 
 treeout->Branch("fjet_MSoftDropBm05Zp050",&fjet_MSoftDropBm05Zp050); 
 treeout->Branch("fjet_MSoftDropBm05Zp100",&fjet_MSoftDropBm05Zp100); 
 treeout->Branch("fjet_MSoftDropBp00Zp001",&fjet_MSoftDropBp00Zp001); 
 treeout->Branch("fjet_MSoftDropBp00Zp005",&fjet_MSoftDropBp00Zp005); 
 treeout->Branch("fjet_MSoftDropBp00Zp010",&fjet_MSoftDropBp00Zp010); 
 treeout->Branch("fjet_MSoftDropBp00Zp050",&fjet_MSoftDropBp00Zp050); 
 treeout->Branch("fjet_MSoftDropBp00Zp100",&fjet_MSoftDropBp00Zp100); 
 /*
 treeout->Branch("fjet_MSoftDropBp05Zp001",&fjet_MSoftDropBp05Zp001); 
 treeout->Branch("fjet_MSoftDropBp05Zp005",&fjet_MSoftDropBp05Zp005); 
 treeout->Branch("fjet_MSoftDropBp05Zp010",&fjet_MSoftDropBp05Zp010); 
 treeout->Branch("fjet_MSoftDropBp05Zp050",&fjet_MSoftDropBp05Zp050); 
 treeout->Branch("fjet_MSoftDropBp05Zp100",&fjet_MSoftDropBp05Zp100); 
 treeout->Branch("fjet_MSoftDropBp10Zp001",&fjet_MSoftDropBp10Zp001); 
 treeout->Branch("fjet_MSoftDropBp10Zp005",&fjet_MSoftDropBp10Zp005); 
 treeout->Branch("fjet_MSoftDropBp10Zp010",&fjet_MSoftDropBp10Zp010); 
 treeout->Branch("fjet_MSoftDropBp10Zp050",&fjet_MSoftDropBp10Zp050); 
 treeout->Branch("fjet_MSoftDropBp10Zp100",&fjet_MSoftDropBp10Zp100); 
 treeout->Branch("fjet_MSoftDropBp20Zp001",&fjet_MSoftDropBp20Zp001); 
 treeout->Branch("fjet_MSoftDropBp20Zp005",&fjet_MSoftDropBp20Zp005); 
 treeout->Branch("fjet_MSoftDropBp20Zp010",&fjet_MSoftDropBp20Zp010); 
 treeout->Branch("fjet_MSoftDropBp20Zp050",&fjet_MSoftDropBp20Zp050); 
 treeout->Branch("fjet_MSoftDropBp20Zp100",&fjet_MSoftDropBp20Zp100); 
 */

 treeout->Branch("fjet_SDw_win20_btag0",&fjet_SDw_win20_btag0);
 treeout->Branch("fjet_SDw_win20_btag1",&fjet_SDw_win20_btag1);
 treeout->Branch("fjet_SDw_win25_btag0",&fjet_SDw_win25_btag0);
 treeout->Branch("fjet_SDw_win25_btag1",&fjet_SDw_win25_btag1);
 treeout->Branch("fjet_SDz_win20_btag0",&fjet_SDz_win20_btag0);
 treeout->Branch("fjet_SDz_win20_btag1",&fjet_SDz_win20_btag1);
 treeout->Branch("fjet_SDz_win25_btag0",&fjet_SDz_win25_btag0);
 treeout->Branch("fjet_SDz_win25_btag1",&fjet_SDz_win25_btag1);
 treeout->Branch("fjet_SDt_win50_btag0",&fjet_SDt_win50_btag0);
 treeout->Branch("fjet_SDt_win50_btag1",&fjet_SDt_win50_btag1);
 treeout->Branch("fjet_SDt_win55_btag0",&fjet_SDt_win55_btag0);
 treeout->Branch("fjet_SDt_win55_btag1",&fjet_SDt_win55_btag1);
 treeout->Branch("fjet_SDt_Dcut1",&fjet_SDt_Dcut1);
 treeout->Branch("fjet_SDt_Dcut2",&fjet_SDt_Dcut2);



 treeout->Branch("fjet_xsec_filteff_numevents",&fjet_xsec_filteff_numevents);
 treeout->Branch("fjet_testing_weight_pt",     &fjet_testing_weight_pt);
 treeout->Branch("fjet_testing_weight_pt_dR",  &fjet_testing_weight_pt_dR);


  // ===================================================================
  // HEPTopTagger
  treeout->Branch("HTT_CA15_pt", &flat_HTT_CA15_pt);
  treeout->Branch("HTT_CA15_eta", &flat_HTT_CA15_eta);
  treeout->Branch("HTT_CA15_phi", &flat_HTT_CA15_phi);
  treeout->Branch("HTT_CA15_m", &flat_HTT_CA15_m);
  treeout->Branch("HTT_CA15_trk_bjets_n", &flat_HTT_CA15_trk_bjets_n);

  treeout->Branch("HTT_CA15_nthLeading",&flat_HTT_CA15_nthLeading);
  treeout->Branch("HTT_CA15_dRmatched_reco_truth",&flat_HTT_CA15_dRmatched_reco_truth);
  treeout->Branch("HTT_CA15_dRmatched_maxEParton_flavor",&flat_HTT_CA15_dRmatched_maxEParton_flavor);
  treeout->Branch("HTT_CA15_dRmatched_particle_flavor",&flat_HTT_CA15_dRmatched_particle_flavor);
  treeout->Branch("HTT_CA15_dRmatched_particle_dR",&flat_HTT_CA15_dRmatched_particle_dR);
  treeout->Branch("HTT_CA15_dRmatched_topBChild",&flat_HTT_CA15_dRmatched_topBChild);
  treeout->Branch("HTT_CA15_dRmatched_nMatchedWZChildren",&flat_HTT_CA15_dRmatched_nMatchedWZChildren);


  treeout->Branch("HTT_CA15_dRmatched_topBChild_dR",&flat_HTT_CA15_dRmatched_topBChild_dR);
  treeout->Branch("HTT_CA15_dRmatched_WZChild1_dR",&flat_HTT_CA15_dRmatched_WZChild1_dR);
  treeout->Branch("HTT_CA15_dRmatched_WZChild2_dR",&flat_HTT_CA15_dRmatched_WZChild2_dR);


  treeout->Branch("fjet_SDt_CA15_uncalib",&fjet_SDt_CA15_uncalib);
  treeout->Branch("fjet_SDt_CA15_force3",&fjet_SDt_CA15_force3);
  treeout->Branch("fjet_SDt_CA15_combined",&fjet_SDt_CA15_combined);
  treeout->Branch("fjet_SDt_CA15_Dcut1",&fjet_SDt_CA15_Dcut1);
  treeout->Branch("fjet_SDt_CA15_Dcut2",&fjet_SDt_CA15_Dcut2);

  flat_HTT_pt_arr.resize(m_nHTTConfigs,0);
  flat_HTT_eta_arr.resize(m_nHTTConfigs,0);
  flat_HTT_phi_arr.resize(m_nHTTConfigs,0);
  flat_HTT_m_arr.resize(m_nHTTConfigs,0); 

  flat_HTT_m12_arr.resize(m_nHTTConfigs,0);
  flat_HTT_m13_arr.resize(m_nHTTConfigs,0);
  flat_HTT_m23_arr.resize(m_nHTTConfigs,0);
  flat_HTT_m123_arr.resize(m_nHTTConfigs,0);
  flat_HTT_m23m123_arr.resize(m_nHTTConfigs,0); 
  flat_HTT_atan1312_arr.resize(m_nHTTConfigs,0); 

  flat_HTT_nTopCands_arr.resize(m_nHTTConfigs,0); 
  flat_HTT_tagged_arr.resize(m_nHTTConfigs,0); 

  flat_HTT_dRW1W2_arr.resize(m_nHTTConfigs,0); 
  flat_HTT_dRW1B_arr.resize(m_nHTTConfigs,0); 
  flat_HTT_dRW2B_arr.resize(m_nHTTConfigs,0);
  flat_HTT_dRmaxW1W2B_arr.resize(m_nHTTConfigs,0); 
  flat_HTT_cosThetaW1W2_arr.resize(m_nHTTConfigs,0); 
  flat_HTT_cosTheta12_arr.resize(m_nHTTConfigs,0); 


  for(int i_conf=0; i_conf<m_nHTTConfigs; i_conf++){
    treeout->Branch(Form( "HTT_pt_%s",           m_HTTConfigs[i_conf].c_str()), &flat_HTT_pt_arr[i_conf]);
    treeout->Branch(Form( "HTT_eta_%s",          m_HTTConfigs[i_conf].c_str()), &flat_HTT_eta_arr[i_conf]);
    treeout->Branch(Form( "HTT_phi_%s",          m_HTTConfigs[i_conf].c_str()), &flat_HTT_phi_arr[i_conf]);
    treeout->Branch(Form( "HTT_m_%s",            m_HTTConfigs[i_conf].c_str()), &flat_HTT_m_arr[i_conf]);

    treeout->Branch(Form( "HTT_m12_%s",          m_HTTConfigs[i_conf].c_str()), &flat_HTT_m12_arr[i_conf]);
    treeout->Branch(Form( "HTT_m13_%s",          m_HTTConfigs[i_conf].c_str()), &flat_HTT_m13_arr[i_conf]);
    treeout->Branch(Form( "HTT_m23_%s",          m_HTTConfigs[i_conf].c_str()), &flat_HTT_m23_arr[i_conf]);
    treeout->Branch(Form( "HTT_m123_%s",         m_HTTConfigs[i_conf].c_str()), &flat_HTT_m123_arr[i_conf]);
    treeout->Branch(Form( "HTT_m23m123_%s",      m_HTTConfigs[i_conf].c_str()), &flat_HTT_m23m123_arr[i_conf]);
    treeout->Branch(Form( "HTT_atan1312_%s",     m_HTTConfigs[i_conf].c_str()), &flat_HTT_atan1312_arr[i_conf]);

    treeout->Branch(Form( "HTT_nTopCands_%s",    m_HTTConfigs[i_conf].c_str()), &flat_HTT_nTopCands_arr[i_conf]);
    treeout->Branch(Form( "HTT_tagged_%s",       m_HTTConfigs[i_conf].c_str()), &flat_HTT_tagged_arr[i_conf]);

    treeout->Branch(Form( "HTT_dRW1W2_%s",       m_HTTConfigs[i_conf].c_str()), &flat_HTT_dRW1W2_arr[i_conf]);
    treeout->Branch(Form( "HTT_dRW1B_%s",        m_HTTConfigs[i_conf].c_str()), &flat_HTT_dRW1B_arr[i_conf]);
    treeout->Branch(Form( "HTT_dRW2B_%s",        m_HTTConfigs[i_conf].c_str()), &flat_HTT_dRW2B_arr[i_conf]);
    treeout->Branch(Form( "HTT_dRmaxW1W2B_%s",   m_HTTConfigs[i_conf].c_str()), &flat_HTT_dRmaxW1W2B_arr[i_conf]);
    treeout->Branch(Form( "HTT_cosThetaW1W2_%s", m_HTTConfigs[i_conf].c_str()), &flat_HTT_cosThetaW1W2_arr[i_conf]);
    treeout->Branch(Form( "HTT_cosTheta12_%s",   m_HTTConfigs[i_conf].c_str()), &flat_HTT_cosTheta12_arr[i_conf]);
  }
  // HEPTopTagger
  // ===================================================================



}


void Flattener :: ClearOutputBranches(){

  // Event information
  EventInfo_runNumber =        0;
  EventInfo_eventNumber =	  0;
  EventInfo_lumiBlock =        0;
  EventInfo_coreFlags =        0;
  EventInfo_mcEventNumber =    0;
  EventInfo_mcChannelNumber =  0;
  EventInfo_mcEventWeight =    0;
  EventInfo_ntruthJets =       0;
  EventInfo_nfatjets =         0;

  // Add here the pileup information
  EventInfo_weight_pileup =  0;
  EventInfo_NPV  = 0;
  EventInfo_actualInteractionsPerCrossing  = 0;
  EventInfo_averageInteractionsPerCrossing  = 0;

  // Truth information
  // Matching, flavor
  // Default is 0 for now
  fjet_fatjets_dRmatched_reco_truth = 0;
  fjet_fatjet_dRmatched_particle_flavor = 0;
  fjet_fatjet_ghost_assc_flavor = 0;

  fjet_dRmatched_maxEParton_flavor = 0;
  fjet_dRmatched_topBChild = 0;
  //fjet_dRmatched_nQuarkChildren = 0;
  // Kinematics:
  // Add the truth kinematics later
  fjet_truthJet_E = 0;
  fjet_truthJet_pt = 0;
  fjet_truthJet_phi = 0;
  fjet_truthJet_eta = 0;
  fjet_truthJet_m = 0;
  fjet_truthJet_nthLeading = -1;

  // Kinematics and substructure
  fjet_E    =          0;
  fjet_m    =          0;
  fjet_pt    =         0;
  fjet_phi    =        0;
  fjet_eta    =        0;
  fjet_nthLeading    = -1;

  fjet_Tau1_wta    =   0;
  fjet_Tau2_wta    =   0;
  fjet_Tau3_wta    =   0;
  fjet_Tau21_wta    =  0;
  fjet_Tau32_wta    =  0;
  fjet_ECF1    =       0;
  fjet_ECF2    =       0;
  fjet_ECF3    =       0;
  fjet_e2    =         0;
  fjet_e3    =         0;
  fjet_C2    =         0;
  fjet_D2    =         0;
  fjet_Angularity    = 0;
  fjet_Aplanarity    = 0;
  fjet_Dip12    =      0;
  fjet_FoxWolfram20   =0;
  fjet_KtDR    =       0;
  fjet_PlanarFlow    = 0;
  fjet_Sphericity    =  0;
  fjet_Split12    =     0;
  fjet_Split23    =     0;
  fjet_ThrustMaj    =   0;
  fjet_ThrustMin    =   0;
  fjet_ZCut12    =      0;

  fjet_truth_nthLeading= 0;
  fjet_truth_dRmatched_maxEParton_flavor= 0;
  fjet_truth_dRmatched_particle_flavor= 0;
  fjet_truth_dRmatched_particle_dR= 0;
  fjet_truth_dRmatched_topBChild= 0;
  fjet_truth_dRmatched_nWZChildren= 0;
  fjet_dRmatched_particle_dR= 0;
  fjet_dRmatched_nMatchedWZChildren= 0;
  fjet_JetpTCorrByCalibratedTAMass= 0;
  fjet_JetpTCorrByCombinedMass=0;
  fjet_pTResponseCombMassCorrpT=0;

  fjet_NUngroomedSubjets= 0;
  fjet_numUngroomedCons= 0;
  fjet_Ntrk500=0;
  fjet_Ntrk1000=0;

  fjet_Qw = 0;
  fjet_Mu12 = 0;

  fjet_M2beta1=0;
  fjet_N2beta1=0;
  fjet_D2alpha1beta2=0;
  fjet_N3beta2=0;

  fjet_TrackAssistedMassUnCalibrated = 0;
  fjet_TrackAssistedMassCalibrated =0;
  fjet_CaloTACombinedMass =0;
  fjet_CaloTACombinedMassUncorrelated =0;
  fjet_CaloTACombinedMassWprime =0;
  fjet_CaloTACombinedMassUncorrelatedWprime =0;

  fjet_truth_dRmatched_topBChild_dR= 0;
  fjet_truth_dRmatched_WZChild1_dR= 0;
  fjet_truth_dRmatched_WZChild2_dR= 0;
  fjet_dRmatched_topBChild_dR= 0;
  fjet_dRmatched_WZChild1_dR= 0;
  fjet_dRmatched_WZChild2_dR= 0;
  fjet_SDt_CA15_uncalib= 0;
  fjet_SDt_CA15_force3= 0;
  fjet_SDt_CA15_combined= 0;
  flat_HTT_CA15_dRmatched_topBChild_dR= 0;
  flat_HTT_CA15_dRmatched_WZChild1_dR= 0;
  flat_HTT_CA15_dRmatched_WZChild2_dR= 0;

fjet_MSoftDropBm20Zp001=0; 
fjet_MSoftDropBm20Zp005=0;
fjet_MSoftDropBm20Zp010=0;
fjet_MSoftDropBm20Zp050=0;
fjet_MSoftDropBm20Zp100=0;
fjet_MSoftDropBm10Zp001=0;
fjet_MSoftDropBm10Zp005=0;
fjet_MSoftDropBm10Zp010=0;
fjet_MSoftDropBm10Zp050=0;
fjet_MSoftDropBm10Zp100=0;
fjet_MSoftDropBm05Zp001=0;
fjet_MSoftDropBm05Zp005=0;
fjet_MSoftDropBm05Zp010=0;
fjet_MSoftDropBm05Zp050=0;
fjet_MSoftDropBm05Zp100=0;
fjet_MSoftDropBp00Zp001=0;
fjet_MSoftDropBp00Zp005=0;
fjet_MSoftDropBp00Zp010=0;
fjet_MSoftDropBp00Zp050=0;
fjet_MSoftDropBp00Zp100=0;
/*
fjet_MSoftDropBp05Zp001=0;
fjet_MSoftDropBp05Zp005=0;
fjet_MSoftDropBp05Zp010=0;
fjet_MSoftDropBp05Zp050=0;
fjet_MSoftDropBp05Zp100=0;
fjet_MSoftDropBp10Zp001=0;
fjet_MSoftDropBp10Zp005=0;
fjet_MSoftDropBp10Zp010=0;
fjet_MSoftDropBp10Zp050=0;
fjet_MSoftDropBp10Zp100=0;
fjet_MSoftDropBp20Zp001=0;
fjet_MSoftDropBp20Zp005=0;
fjet_MSoftDropBp20Zp010=0;
fjet_MSoftDropBp20Zp050=0;
fjet_MSoftDropBp20Zp100=0;
*/

fjet_SDw_win20_btag0=0;
fjet_SDw_win20_btag1=0;
fjet_SDw_win25_btag0=0;
fjet_SDw_win25_btag1=0;
fjet_SDz_win20_btag0=0;
fjet_SDz_win20_btag1=0;
fjet_SDz_win25_btag0=0;
fjet_SDz_win25_btag1=0;
fjet_SDt_win50_btag0=0;
fjet_SDt_win50_btag1=0;
fjet_SDt_win55_btag0=0;
fjet_SDt_win55_btag1=0;

 fjet_SDt_Dcut1= 0;
 fjet_SDt_Dcut2= 0;

fjet_xsec_filteff_numevents = 0;
fjet_testing_weight_pt = 0;
fjet_testing_weight_pt_dR = 0;

fjet_eta_detector = 0;




  // ===================================================================
  // HEPTopTagger
  flat_HTT_CA15_pt = 0;
  flat_HTT_CA15_eta = 0;
  flat_HTT_CA15_phi = 0;
  flat_HTT_CA15_m = 0;

  flat_HTT_CA15_trk_bjets_n = 0;

  fjet_SDt_CA15_Dcut1= 0;
  fjet_SDt_CA15_Dcut2= 0;
  flat_HTT_CA15_nthLeading= 0;
  flat_HTT_CA15_dRmatched_reco_truth= 0;
  flat_HTT_CA15_dRmatched_maxEParton_flavor= 0;
  flat_HTT_CA15_dRmatched_particle_flavor= 0;
  flat_HTT_CA15_dRmatched_particle_dR= 0;
  flat_HTT_CA15_dRmatched_topBChild= 0;
  flat_HTT_CA15_dRmatched_nMatchedWZChildren= 0;


  for(int i_conf=0; i_conf<m_nHTTConfigs; i_conf++){
    flat_HTT_pt_arr[i_conf] = 0;
    flat_HTT_eta_arr[i_conf] = 0;
    flat_HTT_phi_arr[i_conf] = 0;
    flat_HTT_m_arr[i_conf] = 0; 

    flat_HTT_m12_arr[i_conf] = 0;
    flat_HTT_m13_arr[i_conf] = 0;
    flat_HTT_m23_arr[i_conf] = 0;
    flat_HTT_m123_arr[i_conf] = 0;
    flat_HTT_m23m123_arr[i_conf] = 0; 
    flat_HTT_atan1312_arr[i_conf] = 0; 

    flat_HTT_nTopCands_arr[i_conf] = 0; 
    flat_HTT_tagged_arr[i_conf] = 0; 

    flat_HTT_dRW1W2_arr[i_conf] = 0; 
    flat_HTT_dRW1B_arr[i_conf] = 0; 
    flat_HTT_dRW2B_arr[i_conf] = 0;
    flat_HTT_dRmaxW1W2B_arr[i_conf] = 0; 
    flat_HTT_cosThetaW1W2_arr[i_conf] = 0; 
    flat_HTT_cosTheta12_arr[i_conf] = 0; 
  }
  // HEPTopTagger
  // ===================================================================

}

void Flattener :: ClearInputBranches(){
  runNumber = 0;
  eventNumber = 0;
  lumiBlock = 0;
  coreFlags = 0;
  mcEventNumber = 0;
  mcChannelNumber = 0;
  mcEventWeight = 0;
  ntruth_fatjets = 0;
  fatjet_truth_E->clear();
  fatjet_truth_pt->clear();
  fatjet_truth_phi->clear();
  fatjet_truth_eta->clear();
  fatjet_truth_m->clear();
  fatjet_truth_nthLeading->clear();
  nfatjets = 0;
  fatjet_E->clear();
  fatjet_m->clear();
  fatjet_pt->clear();
  fatjet_phi->clear();
  fatjet_eta->clear();
  fatjet_nthLeading->clear();
  fatjets_dRmatched_reco_truth->clear();
  fatjet_dRmatched_particle_flavor->clear();
  fatjet_ghost_assc_flavor->clear();
  fatjet_eta_detector->clear();

  fatjet_dRmatched_maxEParton_flavor->clear();
  fatjet_dRmatched_topBChild->clear();
  //fatjet_dRmatched_nQuarkChildren->clear();

  fatjet_truth_nthLeading->clear();
  fatjet_truth_dRmatched_maxEParton_flavor->clear();
  fatjet_truth_dRmatched_particle_flavor->clear();
  fatjet_truth_dRmatched_particle_dR->clear();
  fatjet_truth_dRmatched_topBChild->clear();
  fatjet_truth_dRmatched_nWZChildren->clear();
  fatjet_dRmatched_particle_dR->clear();
  fatjet_dRmatched_nMatchedWZChildren->clear();
  fatjet_JetpTCorrByCalibratedTAMass->clear();
  fatjet_NUngroomedSubjets->clear();
  fatjet_numUngroomedCons->clear();
  fatjet_Ntrk500->clear();
  fatjet_Ntrk1000->clear();


  if (m_extraTruth) {
    fatjet_truth_dRmatched_topBChild_dR->clear();
    fatjet_truth_dRmatched_WZChild1_dR->clear();
    fatjet_truth_dRmatched_WZChild2_dR->clear();
    fatjet_dRmatched_topBChild_dR->clear();
    fatjet_dRmatched_WZChild1_dR->clear();
    fatjet_dRmatched_WZChild2_dR->clear();
  }


  if(m_found_nSubstructure_branches){
    fatjet_Tau1_wta->clear();
    fatjet_Tau2_wta->clear();
    fatjet_Tau3_wta->clear();
    fatjet_Tau21_wta->clear();
    fatjet_Tau32_wta->clear();
    fatjet_ECF1->clear();
    fatjet_ECF2->clear();
    fatjet_ECF3->clear();
    fatjet_C2->clear();
    fatjet_D2->clear();
    fatjet_Angularity->clear();
    fatjet_Aplanarity->clear();
    fatjet_Dip12->clear();
    fatjet_FoxWolfram20->clear();
    fatjet_KtDR->clear();
    fatjet_PlanarFlow->clear();
    fatjet_Sphericity->clear();
    fatjet_Split12->clear();
    fatjet_Split23->clear();
    fatjet_ThrustMaj->clear();
    fatjet_ThrustMin->clear();
    fatjet_ZCut12->clear();

    fatjet_Qw->clear();
    fatjet_Mu12->clear();

    fatjet_TrackAssistedMassUnCalibrated    ->clear();
    fatjet_TrackAssistedMassCalibrated      ->clear();
    fatjet_CaloTACombinedMass               ->clear();

    fatjet_numConstituents->clear();
    fatjet_NTrimSubjets->clear();
  }

  if(m_found_nSftDrop_branches){  
    fatjet_MSoftDropBm20Zp001->clear(); 
    fatjet_MSoftDropBm20Zp005->clear();
    fatjet_MSoftDropBm20Zp010->clear();
    fatjet_MSoftDropBm20Zp050->clear();
    fatjet_MSoftDropBm20Zp100->clear();
    fatjet_MSoftDropBm10Zp001->clear();
    fatjet_MSoftDropBm10Zp005->clear();
    fatjet_MSoftDropBm10Zp010->clear();
    fatjet_MSoftDropBm10Zp050->clear();
    fatjet_MSoftDropBm10Zp100->clear();
    fatjet_MSoftDropBm05Zp001->clear();
    fatjet_MSoftDropBm05Zp005->clear();
    fatjet_MSoftDropBm05Zp010->clear();
    fatjet_MSoftDropBm05Zp050->clear();
    fatjet_MSoftDropBm05Zp100->clear();
    fatjet_MSoftDropBp00Zp001->clear();
    fatjet_MSoftDropBp00Zp005->clear();
    fatjet_MSoftDropBp00Zp010->clear();
    fatjet_MSoftDropBp00Zp050->clear();
    fatjet_MSoftDropBp00Zp100->clear();
    /*
    fatjet_MSoftDropBp05Zp001->clear();
    fatjet_MSoftDropBp05Zp005->clear();
    fatjet_MSoftDropBp05Zp010->clear();
    fatjet_MSoftDropBp05Zp050->clear();
    fatjet_MSoftDropBp05Zp100->clear();
    fatjet_MSoftDropBp10Zp001->clear();
    fatjet_MSoftDropBp10Zp005->clear();
    fatjet_MSoftDropBp10Zp010->clear();
    fatjet_MSoftDropBp10Zp050->clear();
    fatjet_MSoftDropBp10Zp100->clear();
    fatjet_MSoftDropBp20Zp001->clear();
    fatjet_MSoftDropBp20Zp005->clear();
    fatjet_MSoftDropBp20Zp010->clear();
    fatjet_MSoftDropBp20Zp050->clear();
    fatjet_MSoftDropBp20Zp100->clear();
    */

    fatjet_SDw_win20_btag0->clear();
    fatjet_SDw_win20_btag1->clear();
    fatjet_SDw_win25_btag0->clear();
    fatjet_SDw_win25_btag1->clear();
    fatjet_SDz_win20_btag0->clear();
    fatjet_SDz_win20_btag1->clear();
    fatjet_SDz_win25_btag0->clear();
    fatjet_SDz_win25_btag1->clear();
    fatjet_SDt_win50_btag0->clear();
    fatjet_SDt_win50_btag1->clear();
    fatjet_SDt_win55_btag0->clear();
    fatjet_SDt_win55_btag1->clear();
    
    fatjet_SDt_Dcut1->clear();
    fatjet_SDt_Dcut2->clear();
    
  }

  // Clusters:
  if (m_saved_clusters) {
  fatjet_assoc_cluster_E->clear();
  fatjet_assoc_cluster_pt->clear();
  fatjet_assoc_cluster_phi->clear();
  fatjet_assoc_cluster_eta->clear();
  }

  // Track jets:
  if (m_saved_trackjets) {
    trackJet_E->clear();
    trackJet_pt->clear();
    trackJet_eta->clear();
    trackJet_phi->clear();
    trackJet_MV2c20->clear();
  }

  // ===================================================================
  // HEPTopTagger
  if(m_HEPTopTagger){
    for(int i_conf=0; i_conf<m_nHTTConfigs; i_conf++){
      if(m_found_nHTTConfig_branches[i_conf] == 18){
        HTT_pt_arr[i_conf]->clear();
        HTT_eta_arr[i_conf]->clear();
        HTT_phi_arr[i_conf]->clear();
        HTT_m_arr[i_conf]->clear();

        HTT_m12_arr[i_conf]->clear();
        HTT_m13_arr[i_conf]->clear();
        HTT_m23_arr[i_conf]->clear();
        HTT_m123_arr[i_conf]->clear();
        HTT_m23m123_arr[i_conf]->clear();
        HTT_atan1312_arr[i_conf]->clear();

        HTT_nTopCands_arr[i_conf]->clear();
        HTT_tagged_arr[i_conf]->clear();

        HTT_dRW1W2_arr[i_conf]->clear();
        HTT_dRW1B_arr[i_conf]->clear();
        HTT_dRW2B_arr[i_conf]->clear();
        HTT_dRmaxW1W2B_arr[i_conf]->clear();
        HTT_cosThetaW1W2_arr[i_conf]->clear();
        HTT_cosTheta12_arr[i_conf]->clear();
      }
    }

    HTT_CA15_pt->clear();
    HTT_CA15_eta->clear();
    HTT_CA15_phi->clear();
    HTT_CA15_m->clear();
    HTT_CA15_trk_bjets_n->clear();
    fatjet_SDt_CA15_Dcut1->clear();
    fatjet_SDt_CA15_Dcut2->clear();
    HTT_CA15_nthLeading->clear();
    HTT_CA15_dRmatched_reco_truth->clear();
    HTT_CA15_dRmatched_maxEParton_flavor->clear();
    HTT_CA15_dRmatched_particle_flavor->clear();
    HTT_CA15_dRmatched_particle_dR->clear();
    HTT_CA15_dRmatched_topBChild->clear();
    HTT_CA15_dRmatched_nMatchedWZChildren->clear();

    fatjet_SDt_CA15_uncalib->clear();
    fatjet_SDt_CA15_force3->clear();
    fatjet_SDt_CA15_combined->clear();
    if (m_extraTruth){
      HTT_CA15_dRmatched_topBChild_dR->clear();
      HTT_CA15_dRmatched_WZChild1_dR->clear();
      HTT_CA15_dRmatched_WZChild2_dR->clear();
    }

  }
  // HEPTopTagger
  // ===================================================================

}


float Flattener::GetWeight_SigToBkg_Pt_W(float pt){
  float scale=1.0;
  if(pt<0)  scale=1.0;
  else if(pt<210.0)  scale=44.6646308899;
  else if(pt<220.0)  scale=31.8126392365;
  else if(pt<230.0)  scale=22.130865097;
  else if(pt<240.0)  scale=15.4040298462;
  else if(pt<250.0)  scale=11.0665178299;
  else if(pt<260.0)  scale=7.58827400208;
  else if(pt<270.0)  scale=5.4134645462;
  else if(pt<280.0)  scale=3.77124428749;
  else if(pt<290.0)  scale=2.74344348907;
  else if(pt<300.0)  scale=2.44161486626;
  else if(pt<310.0)  scale=2.42514038086;
  else if(pt<320.0)  scale=2.40626072884;
  else if(pt<330.0)  scale=2.2112531662;
  else if(pt<340.0)  scale=1.90702319145;
  else if(pt<350.0)  scale=1.60073077679;
  else if(pt<360.0)  scale=1.26907658577;
  else if(pt<370.0)  scale=1.03704082966;
  else if(pt<380.0)  scale=0.817616939545;
  else if(pt<390.0)  scale=0.648261487484;
  else if(pt<400.0)  scale=0.568672299385;
  else if(pt<410.0)  scale=0.523847997189;
  else if(pt<420.0)  scale=0.495100408792;
  else if(pt<430.0)  scale=0.445325940847;
  else if(pt<440.0)  scale=0.389115989208;
  else if(pt<450.0)  scale=0.328685611486;
  else if(pt<460.0)  scale=0.274982988834;
  else if(pt<470.0)  scale=0.226750805974;
  else if(pt<480.0)  scale=0.192569687963;
  else if(pt<490.0)  scale=0.155940875411;
  else if(pt<500.0)  scale=0.134051769972;
  else if(pt<510.0)  scale=0.123730748892;
  else if(pt<520.0)  scale=0.115091830492;
  else if(pt<530.0)  scale=0.106391273439;
  else if(pt<540.0)  scale=0.0936062410474;
  else if(pt<550.0)  scale=0.0821606218815;
  else if(pt<560.0)  scale=0.0756875574589;
  else if(pt<570.0)  scale=0.067931458354;
  else if(pt<580.0)  scale=0.060717780143;
  else if(pt<590.0)  scale=0.0545942187309;
  else if(pt<600.0)  scale=0.0481396578252;
  else if(pt<610.0)  scale=0.0440945401788;
  else if(pt<620.0)  scale=0.0411988422275;
  else if(pt<630.0)  scale=0.0365181192756;
  else if(pt<640.0)  scale=0.0325770936906;
  else if(pt<650.0)  scale=0.0292256474495;
  else if(pt<660.0)  scale=0.0270552672446;
  else if(pt<670.0)  scale=0.0249678678811;
  else if(pt<680.0)  scale=0.0225423201919;
  else if(pt<690.0)  scale=0.0202560145408;
  else if(pt<700.0)  scale=0.0183754134923;
  else if(pt<710.0)  scale=0.0169038791209;
  else if(pt<720.0)  scale=0.0157475750893;
  else if(pt<730.0)  scale=0.0143253589049;
  else if(pt<740.0)  scale=0.012772182934;
  else if(pt<750.0)  scale=0.0117348683998;
  else if(pt<760.0)  scale=0.0108209839091;
  else if(pt<770.0)  scale=0.0100398333743;
  else if(pt<780.0)  scale=0.0090492144227;
  else if(pt<790.0)  scale=0.00823537632823;
  else if(pt<800.0)  scale=0.00784222409129;
  else if(pt<810.0)  scale=0.00715659931302;
  else if(pt<820.0)  scale=0.00665158592165;
  else if(pt<830.0)  scale=0.00627692602575;
  else if(pt<840.0)  scale=0.00575579609722;
  else if(pt<850.0)  scale=0.00515741389245;
  else if(pt<860.0)  scale=0.00483652250841;
  else if(pt<870.0)  scale=0.00446766475216;
  else if(pt<880.0)  scale=0.00412038899958;
  else if(pt<890.0)  scale=0.0038468693383;
  else if(pt<900.0)  scale=0.00344049255364;
  else if(pt<910.0)  scale=0.00326730241068;
  else if(pt<920.0)  scale=0.00306025939062;
  else if(pt<930.0)  scale=0.00288300192915;
  else if(pt<940.0)  scale=0.00263126404025;
  else if(pt<950.0)  scale=0.00236863805912;
  else if(pt<960.0)  scale=0.00226184842177;
  else if(pt<970.0)  scale=0.00209881574847;
  else if(pt<980.0)  scale=0.00194205029402;
  else if(pt<990.0)  scale=0.00177791528404;
  else if(pt<1000.0)  scale=0.00168784288689;
  else if(pt<1010.0)  scale=0.0015806760639;
  else if(pt<1020.0)  scale=0.0015202058712;
  else if(pt<1030.0)  scale=0.00145908631384;
  else if(pt<1040.0)  scale=0.00138913211413;
  else if(pt<1050.0)  scale=0.00131006864831;
  else if(pt<1060.0)  scale=0.0012335855281;
  else if(pt<1070.0)  scale=0.00115621404257;
  else if(pt<1080.0)  scale=0.00109534338117;
  else if(pt<1090.0)  scale=0.00104231759906;
  else if(pt<1100.0)  scale=0.000991290784441;
  else if(pt<1110.0)  scale=0.000928531342652;
  else if(pt<1120.0)  scale=0.000880641513504;
  else if(pt<1130.0)  scale=0.00083254853962;
  else if(pt<1140.0)  scale=0.000803087139502;
  else if(pt<1150.0)  scale=0.000746429839637;
  else if(pt<1160.0)  scale=0.000708601786755;
  else if(pt<1170.0)  scale=0.000673864968121;
  else if(pt<1180.0)  scale=0.000638715806417;
  else if(pt<1190.0)  scale=0.000594182813074;
  else if(pt<1200.0)  scale=0.000561223539989;
  else if(pt<1210.0)  scale=0.000534279213753;
  else if(pt<1220.0)  scale=0.000513964216225;
  else if(pt<1230.0)  scale=0.000488571240567;
  else if(pt<1240.0)  scale=0.000460986397229;
  else if(pt<1250.0)  scale=0.000435310066678;
  else if(pt<1260.0)  scale=0.000407712883316;
  else if(pt<1270.0)  scale=0.00039836880751;
  else if(pt<1280.0)  scale=0.000382956117392;
  else if(pt<1290.0)  scale=0.000364024599548;
  else if(pt<1300.0)  scale=0.000348520145053;
  else if(pt<1310.0)  scale=0.000337079662131;
  else if(pt<1320.0)  scale=0.000316568970447;
  else if(pt<1330.0)  scale=0.000297605525702;
  else if(pt<1340.0)  scale=0.000301068474073;
  else if(pt<1350.0)  scale=0.000274139572866;
  else if(pt<1360.0)  scale=0.000263391091721;
  else if(pt<1370.0)  scale=0.000254738202784;
  else if(pt<1380.0)  scale=0.000247712421697;
  else if(pt<1390.0)  scale=0.000229131401284;
  else if(pt<1400.0)  scale=0.00022858614102;
  else if(pt<1410.0)  scale=0.00021758134244;
  else if(pt<1420.0)  scale=0.000214653045987;
  else if(pt<1430.0)  scale=0.00021299047512;
  else if(pt<1440.0)  scale=0.000200336537091;
  else if(pt<1450.0)  scale=0.000187925325008;
  else if(pt<1460.0)  scale=0.00019087214605;
  else if(pt<1470.0)  scale=0.000175160123035;
  else if(pt<1480.0)  scale=0.000174760134541;
  else if(pt<1490.0)  scale=0.000166001002071;
  else if(pt<1500.0)  scale=0.000165543126059;
  else if(pt<1510.0)  scale=0.000166311088833;
  else if(pt<1520.0)  scale=0.000160282972502;
  else if(pt<1530.0)  scale=0.000152764521772;
  else if(pt<1540.0)  scale=0.000154395820573;
  else if(pt<1550.0)  scale=0.000145712401718;
  else if(pt<1560.0)  scale=0.000142953489558;
  else if(pt<1570.0)  scale=0.000133965630084;
  else if(pt<1580.0)  scale=0.000130124521093;
  else if(pt<1590.0)  scale=0.000125437130919;
  else if(pt<1600.0)  scale=0.00011391165026;
  else if(pt<1610.0)  scale=0.000107792839117;
  else if(pt<1620.0)  scale=0.000106998595584;
  else if(pt<1630.0)  scale=0.000109523280116;
  else if(pt<1640.0)  scale=0.000101910736703;
  else if(pt<1650.0)  scale=0.000102791389509;
  else if(pt<1660.0)  scale=9.33460396482e-05;
  else if(pt<1670.0)  scale=9.16861899896e-05;
  else if(pt<1680.0)  scale=8.14694722067e-05;
  else if(pt<1690.0)  scale=8.23729205877e-05;
  else if(pt<1700.0)  scale=7.91057973402e-05;
  else if(pt<1710.0)  scale=7.52173655201e-05;
  else if(pt<1720.0)  scale=7.31013642508e-05;
  else if(pt<1730.0)  scale=7.24549172446e-05;
  else if(pt<1740.0)  scale=6.97083014529e-05;
  else if(pt<1750.0)  scale=6.43372695777e-05;
  else if(pt<1760.0)  scale=6.12859366811e-05;
  else if(pt<1770.0)  scale=5.99907289143e-05;
  else if(pt<1780.0)  scale=5.82481843594e-05;
  else if(pt<1790.0)  scale=5.44685753994e-05;
  else if(pt<1800.0)  scale=5.57303137612e-05;
  else if(pt<1810.0)  scale=5.24424358446e-05;
  else if(pt<1820.0)  scale=4.7814410209e-05;
  else if(pt<1830.0)  scale=4.6808479965e-05;
  else if(pt<1840.0)  scale=4.5906228479e-05;
  else if(pt<1850.0)  scale=4.64542281406e-05;
  else if(pt<1860.0)  scale=4.32132037531e-05;
  else if(pt<1870.0)  scale=4.2829055019e-05;
  else if(pt<1880.0)  scale=3.90701789001e-05;
  else if(pt<1890.0)  scale=3.85470666515e-05;
  else if(pt<1900.0)  scale=3.54970252374e-05;
  else if(pt<1910.0)  scale=3.65836895071e-05;
  else if(pt<1920.0)  scale=3.45268417732e-05;
  else if(pt<1930.0)  scale=3.26428162225e-05;
  else if(pt<1940.0)  scale=3.28299684043e-05;
  else if(pt<1950.0)  scale=2.97466249322e-05;
  else if(pt<1960.0)  scale=2.90708831017e-05;
  else if(pt<1970.0)  scale=2.70920463663e-05;
  else if(pt<1980.0)  scale=2.70408363576e-05;
  else if(pt<1990.0)  scale=2.45363571594e-05;
  else if(pt<2000.0)  scale=2.44253442361e-05;
  else if(pt<2010.0)  scale=2.34342787735e-05;
  else if(pt<2020.0)  scale=2.25702351599e-05;
  else if(pt<2030.0)  scale=2.21196951316e-05;
  else if(pt<2040.0)  scale=2.23613533308e-05;
  else if(pt<2050.0)  scale=2.083405343e-05;
  else if(pt<2060.0)  scale=2.16293810809e-05;
  else if(pt<2070.0)  scale=1.94768508663e-05;
  else if(pt<2080.0)  scale=1.84446307685e-05;
  else if(pt<2090.0)  scale=1.81389568752e-05;
  else if(pt<2100.0)  scale=1.68996812135e-05;
  else if(pt<2110.0)  scale=1.57957147167e-05;
  else if(pt<2120.0)  scale=1.63931745192e-05;
  else if(pt<2130.0)  scale=1.59683750098e-05;
  else if(pt<2140.0)  scale=1.51283556988e-05;
  else if(pt<2150.0)  scale=1.53107012011e-05;
  else if(pt<2160.0)  scale=1.52769098349e-05;
  else if(pt<2170.0)  scale=1.4534550246e-05;
  else if(pt<2180.0)  scale=1.34065403472e-05;
  else if(pt<2190.0)  scale=1.31462884383e-05;
  else if(pt<2200.0)  scale=1.18678144645e-05;
  else if(pt<2210.0)  scale=1.17171175589e-05;
  else if(pt<2220.0)  scale=1.22849314721e-05;
  else if(pt<2230.0)  scale=1.1976395399e-05;
  else if(pt<2240.0)  scale=1.1290097973e-05;
  else if(pt<2250.0)  scale=1.10939172373e-05;
  else if(pt<2260.0)  scale=1.05373728729e-05;
  else if(pt<2270.0)  scale=1.06377701741e-05;
  else if(pt<2280.0)  scale=1.04850896605e-05;
  else if(pt<2290.0)  scale=9.4451042969e-06;
  else if(pt<2300.0)  scale=9.26552002056e-06;
  else if(pt<2310.0)  scale=8.54336940392e-06;
  else if(pt<2320.0)  scale=8.96707570064e-06;
  else if(pt<2330.0)  scale=8.08287586551e-06;
  else if(pt<2340.0)  scale=8.30453609524e-06;
  else if(pt<2350.0)  scale=7.18630235497e-06;
  else if(pt<2360.0)  scale=7.76852448325e-06;
  else if(pt<2370.0)  scale=7.24216079107e-06;
  else if(pt<2380.0)  scale=6.41796577838e-06;
  else if(pt<2390.0)  scale=6.59631223243e-06;
  else if(pt<2400.0)  scale=6.10308052273e-06;
  else if(pt<2410.0)  scale=6.2037911448e-06;
  else if(pt<2420.0)  scale=5.89313094679e-06;
  else if(pt<2430.0)  scale=5.63712046642e-06;
  else if(pt<2440.0)  scale=5.60624266654e-06;
  else if(pt<2450.0)  scale=5.77455830353e-06;
  else if(pt<2460.0)  scale=5.18891238244e-06;
  else if(pt<2470.0)  scale=5.12767519467e-06;
  else if(pt<2480.0)  scale=5.09131132276e-06;
  else if(pt<2490.0)  scale=5.0754433687e-06;
  else if(pt<2500.0)  scale=5.35898925591e-06;
  return scale;
}



float Flattener::GetWeight_SigToBkg_Pt_W_dR(float pt){
  float scale=1.0;
  if(pt<0)  scale=1.0;
  else if(pt<210.0)  scale=31.5444278717;
  else if(pt<220.0)  scale=23.4370708466;
  else if(pt<230.0)  scale=17.2447471619;
  else if(pt<240.0)  scale=12.2531461716;
  else if(pt<250.0)  scale=9.06955242157;
  else if(pt<260.0)  scale=6.4149222374;
  else if(pt<270.0)  scale=4.67443799973;
  else if(pt<280.0)  scale=3.34046697617;
  else if(pt<290.0)  scale=2.49373793602;
  else if(pt<300.0)  scale=2.2275762558;
  else if(pt<310.0)  scale=2.24293684959;
  else if(pt<320.0)  scale=2.25026869774;
  else if(pt<330.0)  scale=2.06107187271;
  else if(pt<340.0)  scale=1.80257511139;
  else if(pt<350.0)  scale=1.52304089069;
  else if(pt<360.0)  scale=1.21336245537;
  else if(pt<370.0)  scale=0.996882915497;
  else if(pt<380.0)  scale=0.793353617191;
  else if(pt<390.0)  scale=0.635989904404;
  else if(pt<400.0)  scale=0.556994557381;
  else if(pt<410.0)  scale=0.514405488968;
  else if(pt<420.0)  scale=0.486041814089;
  else if(pt<430.0)  scale=0.440414637327;
  else if(pt<440.0)  scale=0.384062796831;
  else if(pt<450.0)  scale=0.32581242919;
  else if(pt<460.0)  scale=0.273618608713;
  else if(pt<470.0)  scale=0.226779565215;
  else if(pt<480.0)  scale=0.192802682519;
  else if(pt<490.0)  scale=0.156552612782;
  else if(pt<500.0)  scale=0.134885624051;
  else if(pt<510.0)  scale=0.124874942005;
  else if(pt<520.0)  scale=0.115989252925;
  else if(pt<530.0)  scale=0.107440851629;
  else if(pt<540.0)  scale=0.0946399345994;
  else if(pt<550.0)  scale=0.0832767486572;
  else if(pt<560.0)  scale=0.0765700042248;
  else if(pt<570.0)  scale=0.068843126297;
  else if(pt<580.0)  scale=0.0616142712533;
  else if(pt<590.0)  scale=0.0554330609739;
  else if(pt<600.0)  scale=0.0489379614592;
  else if(pt<610.0)  scale=0.0449027344584;
  else if(pt<620.0)  scale=0.0418189503253;
  else if(pt<630.0)  scale=0.0371257811785;
  else if(pt<640.0)  scale=0.033191896975;
  else if(pt<650.0)  scale=0.0298279114068;
  else if(pt<660.0)  scale=0.0276116766036;
  else if(pt<670.0)  scale=0.0255045443773;
  else if(pt<680.0)  scale=0.0229888558388;
  else if(pt<690.0)  scale=0.0206835120916;
  else if(pt<700.0)  scale=0.018758399412;
  else if(pt<710.0)  scale=0.0172883402556;
  else if(pt<720.0)  scale=0.0161061119288;
  else if(pt<730.0)  scale=0.0146459518;
  else if(pt<740.0)  scale=0.0130680911243;
  else if(pt<750.0)  scale=0.0120052499697;
  else if(pt<760.0)  scale=0.0110932793468;
  else if(pt<770.0)  scale=0.0102724600583;
  else if(pt<780.0)  scale=0.00927197095007;
  else if(pt<790.0)  scale=0.00843192916363;
  else if(pt<800.0)  scale=0.00804095808417;
  else if(pt<810.0)  scale=0.00733111379668;
  else if(pt<820.0)  scale=0.00681340508163;
  else if(pt<830.0)  scale=0.0064379600808;
  else if(pt<840.0)  scale=0.00589688820764;
  else if(pt<850.0)  scale=0.00529107125476;
  else if(pt<860.0)  scale=0.00496367225423;
  else if(pt<870.0)  scale=0.00458449311554;
  else if(pt<880.0)  scale=0.00422638840973;
  else if(pt<890.0)  scale=0.00394458742812;
  else if(pt<900.0)  scale=0.00353167694993;
  else if(pt<910.0)  scale=0.00335501832888;
  else if(pt<920.0)  scale=0.00314279878512;
  else if(pt<930.0)  scale=0.00296063837595;
  else if(pt<940.0)  scale=0.00270106736571;
  else if(pt<950.0)  scale=0.00243301037699;
  else if(pt<960.0)  scale=0.00232150102966;
  else if(pt<970.0)  scale=0.00215570768341;
  else if(pt<980.0)  scale=0.00199331226759;
  else if(pt<990.0)  scale=0.0018254352035;
  else if(pt<1000.0)  scale=0.00173285230994;
  else if(pt<1010.0)  scale=0.0016211818438;
  else if(pt<1020.0)  scale=0.00156220665667;
  else if(pt<1030.0)  scale=0.00149908510502;
  else if(pt<1040.0)  scale=0.00142737908754;
  else if(pt<1050.0)  scale=0.00134504167363;
  else if(pt<1060.0)  scale=0.0012668952113;
  else if(pt<1070.0)  scale=0.00118799263146;
  else if(pt<1080.0)  scale=0.00112561241258;
  else if(pt<1090.0)  scale=0.00107090710662;
  else if(pt<1100.0)  scale=0.00101879076101;
  else if(pt<1110.0)  scale=0.00095444789622;
  else if(pt<1120.0)  scale=0.000903953798115;
  else if(pt<1130.0)  scale=0.000856048136484;
  else if(pt<1140.0)  scale=0.000825435912702;
  else if(pt<1150.0)  scale=0.000767483201344;
  else if(pt<1160.0)  scale=0.000727920443751;
  else if(pt<1170.0)  scale=0.00069297850132;
  else if(pt<1180.0)  scale=0.000656583812088;
  else if(pt<1190.0)  scale=0.000611032010056;
  else if(pt<1200.0)  scale=0.000576918479055;
  else if(pt<1210.0)  scale=0.00054921314586;
  else if(pt<1220.0)  scale=0.000528206466697;
  else if(pt<1230.0)  scale=0.000502397830132;
  else if(pt<1240.0)  scale=0.000473657622933;
  else if(pt<1250.0)  scale=0.000447179627372;
  else if(pt<1260.0)  scale=0.000419245945523;
  else if(pt<1270.0)  scale=0.000409624510212;
  else if(pt<1280.0)  scale=0.000393435300793;
  else if(pt<1290.0)  scale=0.000374216382625;
  else if(pt<1300.0)  scale=0.00035819032928;
  else if(pt<1310.0)  scale=0.000346726417774;
  else if(pt<1320.0)  scale=0.000325702014379;
  else if(pt<1330.0)  scale=0.000305988272885;
  else if(pt<1340.0)  scale=0.000309529306833;
  else if(pt<1350.0)  scale=0.000281980494037;
  else if(pt<1360.0)  scale=0.000270794495009;
  else if(pt<1370.0)  scale=0.000262016314082;
  else if(pt<1380.0)  scale=0.00025472138077;
  else if(pt<1390.0)  scale=0.00023567439348;
  else if(pt<1400.0)  scale=0.000235043655266;
  else if(pt<1410.0)  scale=0.000223782597459;
  else if(pt<1420.0)  scale=0.000220823916607;
  else if(pt<1430.0)  scale=0.000219044668484;
  else if(pt<1440.0)  scale=0.000206091004657;
  else if(pt<1450.0)  scale=0.000193433050299;
  else if(pt<1460.0)  scale=0.000196406443138;
  else if(pt<1470.0)  scale=0.000180293718586;
  else if(pt<1480.0)  scale=0.000179881986696;
  else if(pt<1490.0)  scale=0.00017086614389;
  else if(pt<1500.0)  scale=0.000170224695466;
  else if(pt<1510.0)  scale=0.000171185325598;
  else if(pt<1520.0)  scale=0.000164861412486;
  else if(pt<1530.0)  scale=0.000157128088176;
  else if(pt<1540.0)  scale=0.000158859504154;
  else if(pt<1550.0)  scale=0.000149982937728;
  else if(pt<1560.0)  scale=0.000147084414493;
  else if(pt<1570.0)  scale=0.000137782320962;
  else if(pt<1580.0)  scale=0.00013388354273;
  else if(pt<1590.0)  scale=0.000129113454022;
  else if(pt<1600.0)  scale=0.000117250165204;
  else if(pt<1610.0)  scale=0.000110952038085;
  else if(pt<1620.0)  scale=0.000110089291411;
  else if(pt<1630.0)  scale=0.000112733177957;
  else if(pt<1640.0)  scale=0.000104805309093;
  else if(pt<1650.0)  scale=0.000105705657916;
  else if(pt<1660.0)  scale=9.60818288149e-05;
  else if(pt<1670.0)  scale=9.42857004702e-05;
  else if(pt<1680.0)  scale=8.38206760818e-05;
  else if(pt<1690.0)  scale=8.47477640491e-05;
  else if(pt<1700.0)  scale=8.14242303022e-05;
  else if(pt<1710.0)  scale=7.73856008891e-05;
  else if(pt<1720.0)  scale=7.51713305363e-05;
  else if(pt<1730.0)  scale=7.45784200262e-05;
  else if(pt<1740.0)  scale=7.17147049727e-05;
  else if(pt<1750.0)  scale=6.62228630972e-05;
  else if(pt<1760.0)  scale=6.30195863778e-05;
  else if(pt<1770.0)  scale=6.17173645878e-05;
  else if(pt<1780.0)  scale=5.99553204665e-05;
  else if(pt<1790.0)  scale=5.60068729101e-05;
  else if(pt<1800.0)  scale=5.73636571062e-05;
  else if(pt<1810.0)  scale=5.39497596037e-05;
  else if(pt<1820.0)  scale=4.92157523695e-05;
  else if(pt<1830.0)  scale=4.81542701891e-05;
  else if(pt<1840.0)  scale=4.72516476293e-05;
  else if(pt<1850.0)  scale=4.78157053294e-05;
  else if(pt<1860.0)  scale=4.44796933152e-05;
  else if(pt<1870.0)  scale=4.40842850367e-05;
  else if(pt<1880.0)  scale=4.01918914577e-05;
  else if(pt<1890.0)  scale=3.96768009523e-05;
  else if(pt<1900.0)  scale=3.65373707609e-05;
  else if(pt<1910.0)  scale=3.76558855351e-05;
  else if(pt<1920.0)  scale=3.55387528543e-05;
  else if(pt<1930.0)  scale=3.35783261107e-05;
  else if(pt<1940.0)  scale=3.37697128998e-05;
  else if(pt<1950.0)  scale=3.06184374494e-05;
  else if(pt<1960.0)  scale=2.99228904623e-05;
  else if(pt<1970.0)  scale=2.78860607068e-05;
  else if(pt<1980.0)  scale=2.78333482129e-05;
  else if(pt<1990.0)  scale=2.52554691542e-05;
  else if(pt<2000.0)  scale=2.51412002399e-05;
  else if(pt<2010.0)  scale=2.41210891545e-05;
  else if(pt<2020.0)  scale=2.32317252085e-05;
  else if(pt<2030.0)  scale=2.2767979317e-05;
  else if(pt<2040.0)  scale=2.29825946008e-05;
  else if(pt<2050.0)  scale=2.144465725e-05;
  else if(pt<2060.0)  scale=2.226329525e-05;
  else if(pt<2070.0)  scale=2.00325921469e-05;
  else if(pt<2080.0)  scale=1.89709953702e-05;
  else if(pt<2090.0)  scale=1.86564284377e-05;
  else if(pt<2100.0)  scale=1.73818862095e-05;
  else if(pt<2110.0)  scale=1.62465858011e-05;
  else if(pt<2120.0)  scale=1.68736260093e-05;
  else if(pt<2130.0)  scale=1.64363755175e-05;
  else if(pt<2140.0)  scale=1.55717370944e-05;
  else if(pt<2150.0)  scale=1.57594258781e-05;
  else if(pt<2160.0)  scale=1.57246449817e-05;
  else if(pt<2170.0)  scale=1.4960527551e-05;
  else if(pt<2180.0)  scale=1.37994593388e-05;
  else if(pt<2190.0)  scale=1.35315785883e-05;
  else if(pt<2200.0)  scale=1.22156352518e-05;
  else if(pt<2210.0)  scale=1.20605227494e-05;
  else if(pt<2220.0)  scale=1.26449785967e-05;
  else if(pt<2230.0)  scale=1.23273994177e-05;
  else if(pt<2240.0)  scale=1.16209876069e-05;
  else if(pt<2250.0)  scale=1.14190579552e-05;
  else if(pt<2260.0)  scale=1.08352569441e-05;
  else if(pt<2270.0)  scale=1.09495413199e-05;
  else if(pt<2280.0)  scale=1.07923860924e-05;
  else if(pt<2290.0)  scale=9.72192174231e-06;
  else if(pt<2300.0)  scale=9.53707331064e-06;
  else if(pt<2310.0)  scale=8.7937578428e-06;
  else if(pt<2320.0)  scale=9.22988238017e-06;
  else if(pt<2330.0)  scale=8.319769222e-06;
  else if(pt<2340.0)  scale=8.54792506289e-06;
  else if(pt<2350.0)  scale=7.39691813578e-06;
  else if(pt<2360.0)  scale=7.99620374892e-06;
  else if(pt<2370.0)  scale=7.45441457184e-06;
  else if(pt<2380.0)  scale=6.60606383462e-06;
  else if(pt<2390.0)  scale=6.78963669998e-06;
  else if(pt<2400.0)  scale=6.28194993624e-06;
  else if(pt<2410.0)  scale=6.38561141386e-06;
  else if(pt<2420.0)  scale=6.06584671914e-06;
  else if(pt<2430.0)  scale=5.80233336223e-06;
  else if(pt<2440.0)  scale=5.77055016038e-06;
  else if(pt<2450.0)  scale=5.94379889662e-06;
  else if(pt<2460.0)  scale=5.34098899152e-06;
  else if(pt<2470.0)  scale=5.27098472958e-06;
  else if(pt<2480.0)  scale=5.240527571e-06;
  else if(pt<2490.0)  scale=5.22419395566e-06;
  else if(pt<2500.0)  scale=5.51605035071e-06;
  return scale;
}



float Flattener::GetWeight_SigToBkg_Pt_top_dR(float pt){
  float scale=1.0;
  if(pt<0)  scale=1.0;
  else if(pt<210.0)  scale=31.6470050812;
  else if(pt<220.0)  scale=23.5309848785;
  else if(pt<230.0)  scale=17.3098602295;
  else if(pt<240.0)  scale=12.2758569717;
  else if(pt<250.0)  scale=9.08821582794;
  else if(pt<260.0)  scale=6.43223333359;
  else if(pt<270.0)  scale=4.68423891068;
  else if(pt<280.0)  scale=3.34452152252;
  else if(pt<290.0)  scale=2.49505805969;
  else if(pt<300.0)  scale=2.22915291786;
  else if(pt<310.0)  scale=2.24423265457;
  else if(pt<320.0)  scale=2.25320935249;
  else if(pt<330.0)  scale=2.06357979774;
  else if(pt<340.0)  scale=1.80312991142;
  else if(pt<350.0)  scale=1.52651834488;
  else if(pt<360.0)  scale=1.21513712406;
  else if(pt<370.0)  scale=0.998011827469;
  else if(pt<380.0)  scale=0.793978691101;
  else if(pt<390.0)  scale=0.636579930782;
  else if(pt<400.0)  scale=0.557333290577;
  else if(pt<410.0)  scale=0.515080451965;
  else if(pt<420.0)  scale=0.486566990614;
  else if(pt<430.0)  scale=0.440551817417;
  else if(pt<440.0)  scale=0.384400248528;
  else if(pt<450.0)  scale=0.326174139977;
  else if(pt<460.0)  scale=0.273652404547;
  else if(pt<470.0)  scale=0.226771637797;
  else if(pt<480.0)  scale=0.192839369178;
  else if(pt<490.0)  scale=0.156573757529;
  else if(pt<500.0)  scale=0.134828194976;
  else if(pt<510.0)  scale=0.124879345298;
  else if(pt<520.0)  scale=0.115982659161;
  else if(pt<530.0)  scale=0.107450194657;
  else if(pt<540.0)  scale=0.0946476534009;
  else if(pt<550.0)  scale=0.0832729414105;
  else if(pt<560.0)  scale=0.0765482336283;
  else if(pt<570.0)  scale=0.0688411593437;
  else if(pt<580.0)  scale=0.0616124272346;
  else if(pt<590.0)  scale=0.055431496352;
  else if(pt<600.0)  scale=0.0489117987454;
  else if(pt<610.0)  scale=0.0448844321072;
  else if(pt<620.0)  scale=0.0418076254427;
  else if(pt<630.0)  scale=0.0371155515313;
  else if(pt<640.0)  scale=0.033182669431;
  else if(pt<650.0)  scale=0.0298196040094;
  else if(pt<660.0)  scale=0.0276005230844;
  else if(pt<670.0)  scale=0.025497701019;
  else if(pt<680.0)  scale=0.0229917224497;
  else if(pt<690.0)  scale=0.0206778589636;
  else if(pt<700.0)  scale=0.0187508203089;
  else if(pt<710.0)  scale=0.0172836910933;
  else if(pt<720.0)  scale=0.0160996802151;
  else if(pt<730.0)  scale=0.0146400937811;
  else if(pt<740.0)  scale=0.0130680343136;
  else if(pt<750.0)  scale=0.0120020089671;
  else if(pt<760.0)  scale=0.0110903270543;
  else if(pt<770.0)  scale=0.0102683603764;
  else if(pt<780.0)  scale=0.00926950015128;
  else if(pt<790.0)  scale=0.00842853542417;
  else if(pt<800.0)  scale=0.0080388719216;
  else if(pt<810.0)  scale=0.00732919760048;
  else if(pt<820.0)  scale=0.00680976267904;
  else if(pt<830.0)  scale=0.00643725972623;
  else if(pt<840.0)  scale=0.00589373614639;
  else if(pt<850.0)  scale=0.00528896879405;
  else if(pt<860.0)  scale=0.00496101891622;
  else if(pt<870.0)  scale=0.00458268122748;
  else if(pt<880.0)  scale=0.00422471715137;
  else if(pt<890.0)  scale=0.00394303305075;
  else if(pt<900.0)  scale=0.00352978892624;
  else if(pt<910.0)  scale=0.0033532246016;
  else if(pt<920.0)  scale=0.00314111844636;
  else if(pt<930.0)  scale=0.0029590560589;
  else if(pt<940.0)  scale=0.00270039122552;
  else if(pt<950.0)  scale=0.00243204529397;
  else if(pt<960.0)  scale=0.00232125283219;
  else if(pt<970.0)  scale=0.00215486250818;
  else if(pt<980.0)  scale=0.00199252855964;
  else if(pt<990.0)  scale=0.0018247134285;
  else if(pt<1000.0)  scale=0.00173217291012;
  else if(pt<1010.0)  scale=0.00162054714747;
  else if(pt<1020.0)  scale=0.00156137149315;
  else if(pt<1030.0)  scale=0.00149828370195;
  else if(pt<1040.0)  scale=0.00142661598511;
  else if(pt<1050.0)  scale=0.00134432257619;
  else if(pt<1060.0)  scale=0.00126621790696;
  else if(pt<1070.0)  scale=0.00118735758588;
  else if(pt<1080.0)  scale=0.00112501066178;
  else if(pt<1090.0)  scale=0.00107051234227;
  else if(pt<1100.0)  scale=0.00101824605372;
  else if(pt<1110.0)  scale=0.000953937706072;
  else if(pt<1120.0)  scale=0.000903626147192;
  else if(pt<1130.0)  scale=0.000855590449646;
  else if(pt<1140.0)  scale=0.000825142895337;
  else if(pt<1150.0)  scale=0.000767072895542;
  else if(pt<1160.0)  scale=0.000727662758436;
  else if(pt<1170.0)  scale=0.000692608009558;
  else if(pt<1180.0)  scale=0.000656354182865;
  else if(pt<1190.0)  scale=0.000610705348663;
  else if(pt<1200.0)  scale=0.00057661003666;
  else if(pt<1210.0)  scale=0.000548919546418;
  else if(pt<1220.0)  scale=0.000527924101334;
  else if(pt<1230.0)  scale=0.000502129259985;
  else if(pt<1240.0)  scale=0.000473404419608;
  else if(pt<1250.0)  scale=0.000446940568509;
  else if(pt<1260.0)  scale=0.000419021816924;
  else if(pt<1270.0)  scale=0.000409489352023;
  else if(pt<1280.0)  scale=0.000393224967411;
  else if(pt<1290.0)  scale=0.000374016351998;
  else if(pt<1300.0)  scale=0.000358076475095;
  else if(pt<1310.0)  scale=0.000346541084582;
  else if(pt<1320.0)  scale=0.000325600325596;
  else if(pt<1330.0)  scale=0.000305892492179;
  else if(pt<1340.0)  scale=0.000309363822453;
  else if(pt<1350.0)  scale=0.000281829765299;
  else if(pt<1360.0)  scale=0.000270712887868;
  else if(pt<1370.0)  scale=0.000261876237346;
  else if(pt<1380.0)  scale=0.000254585203947;
  else if(pt<1390.0)  scale=0.000235548388446;
  else if(pt<1400.0)  scale=0.000234978404478;
  else if(pt<1410.0)  scale=0.000223662980716;
  else if(pt<1420.0)  scale=0.00022070587147;
  else if(pt<1430.0)  scale=0.000218927569222;
  else if(pt<1440.0)  scale=0.000206039272598;
  else if(pt<1450.0)  scale=0.000193329644389;
  else if(pt<1460.0)  scale=0.000196301451069;
  else if(pt<1470.0)  scale=0.000180197341251;
  else if(pt<1480.0)  scale=0.00017978582764;
  else if(pt<1490.0)  scale=0.000170774801518;
  else if(pt<1500.0)  scale=0.000170133702341;
  else if(pt<1510.0)  scale=0.000171093823155;
  else if(pt<1520.0)  scale=0.000164773286087;
  else if(pt<1530.0)  scale=0.000157044094522;
  else if(pt<1540.0)  scale=0.000158774579177;
  else if(pt<1550.0)  scale=0.000149902756675;
  else if(pt<1560.0)  scale=0.000147005775943;
  else if(pt<1570.0)  scale=0.000137708673719;
  else if(pt<1580.0)  scale=0.000133811976411;
  else if(pt<1590.0)  scale=0.000129044419737;
  else if(pt<1600.0)  scale=0.000117187482829;
  else if(pt<1610.0)  scale=0.000110892731755;
  else if(pt<1620.0)  scale=0.000110030443466;
  else if(pt<1630.0)  scale=0.000112672918476;
  else if(pt<1640.0)  scale=0.000104749284219;
  else if(pt<1650.0)  scale=0.000105649160105;
  else if(pt<1660.0)  scale=9.60304678301e-05;
  else if(pt<1670.0)  scale=9.42352999118e-05;
  else if(pt<1680.0)  scale=8.37758634589e-05;
  else if(pt<1690.0)  scale=8.4702456661e-05;
  else if(pt<1700.0)  scale=8.13807055238e-05;
  else if(pt<1710.0)  scale=7.73442370701e-05;
  else if(pt<1720.0)  scale=7.51311454223e-05;
  else if(pt<1730.0)  scale=7.45385550545e-05;
  else if(pt<1740.0)  scale=7.1676375228e-05;
  else if(pt<1750.0)  scale=6.61874655634e-05;
  else if(pt<1760.0)  scale=6.2985898694e-05;
  else if(pt<1770.0)  scale=6.1684375396e-05;
  else if(pt<1780.0)  scale=5.99232698733e-05;
  else if(pt<1790.0)  scale=5.60059343115e-05;
  else if(pt<1800.0)  scale=5.73329925828e-05;
  else if(pt<1810.0)  scale=5.39209213457e-05;
  else if(pt<1820.0)  scale=4.91894425068e-05;
  else if(pt<1830.0)  scale=4.81285278511e-05;
  else if(pt<1840.0)  scale=4.72263855045e-05;
  else if(pt<1850.0)  scale=4.77901448903e-05;
  else if(pt<1860.0)  scale=4.44559154857e-05;
  else if(pt<1870.0)  scale=4.40607182099e-05;
  else if(pt<1880.0)  scale=4.01704055548e-05;
  else if(pt<1890.0)  scale=3.96555915358e-05;
  else if(pt<1900.0)  scale=3.65178384527e-05;
  else if(pt<1910.0)  scale=3.76357529603e-05;
  else if(pt<1920.0)  scale=3.5519755329e-05;
  else if(pt<1930.0)  scale=3.35603763233e-05;
  else if(pt<1940.0)  scale=3.3751657611e-05;
  else if(pt<1950.0)  scale=3.06020701828e-05;
  else if(pt<1960.0)  scale=2.99068960885e-05;
  else if(pt<1970.0)  scale=2.78711522697e-05;
  else if(pt<1980.0)  scale=2.78184688796e-05;
  else if(pt<1990.0)  scale=2.52419667959e-05;
  else if(pt<2000.0)  scale=2.51277615462e-05;
  else if(pt<2010.0)  scale=2.41081961576e-05;
  else if(pt<2020.0)  scale=2.32193051488e-05;
  else if(pt<2030.0)  scale=2.27558084589e-05;
  else if(pt<2040.0)  scale=2.29873494391e-05;
  else if(pt<2050.0)  scale=2.14331921597e-05;
  else if(pt<2060.0)  scale=2.22513936023e-05;
  else if(pt<2070.0)  scale=2.00218855753e-05;
  else if(pt<2080.0)  scale=1.89608545043e-05;
  else if(pt<2090.0)  scale=1.86464549188e-05;
  else if(pt<2100.0)  scale=1.73725966306e-05;
  else if(pt<2110.0)  scale=1.62379001267e-05;
  else if(pt<2120.0)  scale=1.68646038219e-05;
  else if(pt<2130.0)  scale=1.64275897987e-05;
  else if(pt<2140.0)  scale=1.55634133989e-05;
  else if(pt<2150.0)  scale=1.57510021381e-05;
  else if(pt<2160.0)  scale=1.57162394316e-05;
  else if(pt<2170.0)  scale=1.49525303641e-05;
  else if(pt<2180.0)  scale=1.37920815177e-05;
  else if(pt<2190.0)  scale=1.35243453769e-05;
  else if(pt<2200.0)  scale=1.22091050798e-05;
  else if(pt<2210.0)  scale=1.20540744319e-05;
  else if(pt<2220.0)  scale=1.26382183225e-05;
  else if(pt<2230.0)  scale=1.23208092191e-05;
  else if(pt<2240.0)  scale=1.16147748486e-05;
  else if(pt<2250.0)  scale=1.14129534268e-05;
  else if(pt<2260.0)  scale=1.08294643724e-05;
  else if(pt<2270.0)  scale=1.0943687812e-05;
  else if(pt<2280.0)  scale=1.07866171675e-05;
  else if(pt<2290.0)  scale=9.71672398009e-06;
  else if(pt<2300.0)  scale=9.53197559284e-06;
  else if(pt<2310.0)  scale=8.78905757418e-06;
  else if(pt<2320.0)  scale=9.22494837141e-06;
  else if(pt<2330.0)  scale=8.31532088341e-06;
  else if(pt<2340.0)  scale=8.5433557615e-06;
  else if(pt<2350.0)  scale=7.39296456231e-06;
  else if(pt<2360.0)  scale=7.99193003331e-06;
  else if(pt<2370.0)  scale=7.45042916606e-06;
  else if(pt<2380.0)  scale=6.6025322667e-06;
  else if(pt<2390.0)  scale=6.78600736137e-06;
  else if(pt<2400.0)  scale=6.2785911723e-06;
  else if(pt<2410.0)  scale=6.38219808025e-06;
  else if(pt<2420.0)  scale=6.06260437053e-06;
  else if(pt<2430.0)  scale=5.79923153055e-06;
  else if(pt<2440.0)  scale=5.76746515435e-06;
  else if(pt<2450.0)  scale=5.94062112214e-06;
  else if(pt<2460.0)  scale=5.3381336329e-06;
  else if(pt<2470.0)  scale=5.26816711499e-06;
  else if(pt<2480.0)  scale=5.23772587258e-06;
  else if(pt<2490.0)  scale=5.22140135217e-06;
  else if(pt<2500.0)  scale=5.51310176888e-06;
  return scale;
}





float Flattener::GetWeight_SigToBkg_Pt_top(float pt){
  float scale=1.0;
  if(pt<0)  scale=1.0;
  else if(pt<210.0)  scale=1984.50939941;
  else if(pt<220.0)  scale=1374.66699219;
  else if(pt<230.0)  scale=741.95715332;
  else if(pt<240.0)  scale=489.0519104;
  else if(pt<250.0)  scale=256.879058838;
  else if(pt<260.0)  scale=151.064666748;
  else if(pt<270.0)  scale=83.105682373;
  else if(pt<280.0)  scale=56.7547950745;
  else if(pt<290.0)  scale=30.3387565613;
  else if(pt<300.0)  scale=18.479429245;
  else if(pt<310.0)  scale=11.9809045792;
  else if(pt<320.0)  scale=7.71817493439;
  else if(pt<330.0)  scale=5.16129493713;
  else if(pt<340.0)  scale=3.75957298279;
  else if(pt<350.0)  scale=2.98302674294;
  else if(pt<360.0)  scale=2.34074401855;
  else if(pt<370.0)  scale=1.77051186562;
  else if(pt<380.0)  scale=1.39478731155;
  else if(pt<390.0)  scale=1.09543168545;
  else if(pt<400.0)  scale=0.871413111687;
  else if(pt<410.0)  scale=0.697568297386;
  else if(pt<420.0)  scale=0.547526896;
  else if(pt<430.0)  scale=0.442073494196;
  else if(pt<440.0)  scale=0.365600347519;
  else if(pt<450.0)  scale=0.303022503853;
  else if(pt<460.0)  scale=0.253550648689;
  else if(pt<470.0)  scale=0.213115110993;
  else if(pt<480.0)  scale=0.185814529657;
  else if(pt<490.0)  scale=0.164950758219;
  else if(pt<500.0)  scale=0.144458279014;
  else if(pt<510.0)  scale=0.128499627113;
  else if(pt<520.0)  scale=0.110319964588;
  else if(pt<530.0)  scale=0.0945233330131;
  else if(pt<540.0)  scale=0.0806181654334;
  else if(pt<550.0)  scale=0.0698932930827;
  else if(pt<560.0)  scale=0.0604978762567;
  else if(pt<570.0)  scale=0.0519542843103;
  else if(pt<580.0)  scale=0.0461522117257;
  else if(pt<590.0)  scale=0.0412563346326;
  else if(pt<600.0)  scale=0.0361113548279;
  else if(pt<610.0)  scale=0.0335105806589;
  else if(pt<620.0)  scale=0.0301274303347;
  else if(pt<630.0)  scale=0.0275048837066;
  else if(pt<640.0)  scale=0.0244421325624;
  else if(pt<650.0)  scale=0.0221522450447;
  else if(pt<660.0)  scale=0.0200087428093;
  else if(pt<670.0)  scale=0.0177013985813;
  else if(pt<680.0)  scale=0.0161077734083;
  else if(pt<690.0)  scale=0.0143390735611;
  else if(pt<700.0)  scale=0.0128144007176;
  else if(pt<710.0)  scale=0.0116808190942;
  else if(pt<720.0)  scale=0.0105799185112;
  else if(pt<730.0)  scale=0.00967391300946;
  else if(pt<740.0)  scale=0.00897024013102;
  else if(pt<750.0)  scale=0.00837801210582;
  else if(pt<760.0)  scale=0.00779046211392;
  else if(pt<770.0)  scale=0.007134267129;
  else if(pt<780.0)  scale=0.00663869129494;
  else if(pt<790.0)  scale=0.00594816403463;
  else if(pt<800.0)  scale=0.00553683750331;
  else if(pt<810.0)  scale=0.00510125653818;
  else if(pt<820.0)  scale=0.00458559580147;
  else if(pt<830.0)  scale=0.00419894000515;
  else if(pt<840.0)  scale=0.00397926103324;
  else if(pt<850.0)  scale=0.00365040497854;
  else if(pt<860.0)  scale=0.00339009636082;
  else if(pt<870.0)  scale=0.00325395958498;
  else if(pt<880.0)  scale=0.00305493874475;
  else if(pt<890.0)  scale=0.00284036900848;
  else if(pt<900.0)  scale=0.00266319536604;
  else if(pt<910.0)  scale=0.00247540674172;
  else if(pt<920.0)  scale=0.00233247596771;
  else if(pt<930.0)  scale=0.00213193078525;
  else if(pt<940.0)  scale=0.00197272095829;
  else if(pt<950.0)  scale=0.0018076779088;
  else if(pt<960.0)  scale=0.00167077057995;
  else if(pt<970.0)  scale=0.00158019200899;
  else if(pt<980.0)  scale=0.00148600712419;
  else if(pt<990.0)  scale=0.00141620088834;
  else if(pt<1000.0)  scale=0.00136592448689;
  else if(pt<1010.0)  scale=0.00127825024538;
  else if(pt<1020.0)  scale=0.00122773472685;
  else if(pt<1030.0)  scale=0.00116411887575;
  else if(pt<1040.0)  scale=0.00106923910789;
  else if(pt<1050.0)  scale=0.0010005030781;
  else if(pt<1060.0)  scale=0.000945595034864;
  else if(pt<1070.0)  scale=0.00088932242943;
  else if(pt<1080.0)  scale=0.000844793568831;
  else if(pt<1090.0)  scale=0.000802230322734;
  else if(pt<1100.0)  scale=0.000754160806537;
  else if(pt<1110.0)  scale=0.000729501538444;
  else if(pt<1120.0)  scale=0.000697303388733;
  else if(pt<1130.0)  scale=0.000667089945637;
  else if(pt<1140.0)  scale=0.000637820863631;
  else if(pt<1150.0)  scale=0.00061486044433;
  else if(pt<1160.0)  scale=0.000587635440752;
  else if(pt<1170.0)  scale=0.000558248255402;
  else if(pt<1180.0)  scale=0.000529042619746;
  else if(pt<1190.0)  scale=0.000480953225633;
  else if(pt<1200.0)  scale=0.000465116609121;
  else if(pt<1210.0)  scale=0.000443400756922;
  else if(pt<1220.0)  scale=0.000422607234214;
  else if(pt<1230.0)  scale=0.000409536325606;
  else if(pt<1240.0)  scale=0.000394853035687;
  else if(pt<1250.0)  scale=0.000390887464164;
  else if(pt<1260.0)  scale=0.000371249567252;
  else if(pt<1270.0)  scale=0.000370438443497;
  else if(pt<1280.0)  scale=0.00035482167732;
  else if(pt<1290.0)  scale=0.000345764216036;
  else if(pt<1300.0)  scale=0.000326307781506;
  else if(pt<1310.0)  scale=0.000304769782815;
  else if(pt<1320.0)  scale=0.000300823536236;
  else if(pt<1330.0)  scale=0.000286499940557;
  else if(pt<1340.0)  scale=0.000283214059891;
  else if(pt<1350.0)  scale=0.000268830597633;
  else if(pt<1360.0)  scale=0.00026004112442;
  else if(pt<1370.0)  scale=0.000259854277829;
  else if(pt<1380.0)  scale=0.000255413382547;
  else if(pt<1390.0)  scale=0.000252498924965;
  else if(pt<1400.0)  scale=0.000247396266786;
  else if(pt<1410.0)  scale=0.000238759981585;
  else if(pt<1420.0)  scale=0.000234743318288;
  else if(pt<1430.0)  scale=0.000221607391723;
  else if(pt<1440.0)  scale=0.000221785347094;
  else if(pt<1450.0)  scale=0.000213962164707;
  else if(pt<1460.0)  scale=0.000206784170587;
  else if(pt<1470.0)  scale=0.00020392749866;
  else if(pt<1480.0)  scale=0.000205175689189;
  else if(pt<1490.0)  scale=0.000208027398912;
  else if(pt<1500.0)  scale=0.000206013573916;
  else if(pt<1510.0)  scale=0.000213801569771;
  else if(pt<1520.0)  scale=0.000217561595491;
  else if(pt<1530.0)  scale=0.000217551569222;
  else if(pt<1540.0)  scale=0.000209736972465;
  else if(pt<1550.0)  scale=0.000209950827411;
  else if(pt<1560.0)  scale=0.000201928938623;
  else if(pt<1570.0)  scale=0.000194708962226;
  else if(pt<1580.0)  scale=0.000191010884009;
  else if(pt<1590.0)  scale=0.000192521911231;
  else if(pt<1600.0)  scale=0.000179416383617;
  else if(pt<1610.0)  scale=0.000168952596141;
  else if(pt<1620.0)  scale=0.0001639486145;
  else if(pt<1630.0)  scale=0.00015380133118;
  else if(pt<1640.0)  scale=0.000150902647874;
  else if(pt<1650.0)  scale=0.000145851372508;
  else if(pt<1660.0)  scale=0.000130260174046;
  else if(pt<1670.0)  scale=0.000124365222291;
  else if(pt<1680.0)  scale=0.000120508222608;
  else if(pt<1690.0)  scale=0.000113921429147;
  else if(pt<1700.0)  scale=0.000111195833597;
  else if(pt<1710.0)  scale=0.000106635219709;
  else if(pt<1720.0)  scale=0.000102284946479;
  else if(pt<1730.0)  scale=9.55136856646e-05;
  else if(pt<1740.0)  scale=8.67596536409e-05;
  else if(pt<1750.0)  scale=8.59054271132e-05;
  else if(pt<1760.0)  scale=7.95640080469e-05;
  else if(pt<1770.0)  scale=7.74684158387e-05;
  else if(pt<1780.0)  scale=7.24786150386e-05;
  else if(pt<1790.0)  scale=6.65030165692e-05;
  else if(pt<1800.0)  scale=6.64189792587e-05;
  else if(pt<1810.0)  scale=6.06312532909e-05;
  else if(pt<1820.0)  scale=5.89078081248e-05;
  else if(pt<1830.0)  scale=5.45615257579e-05;
  else if(pt<1840.0)  scale=5.19523564435e-05;
  else if(pt<1850.0)  scale=5.11119760631e-05;
  else if(pt<1860.0)  scale=4.73726213386e-05;
  else if(pt<1870.0)  scale=4.61832787551e-05;
  else if(pt<1880.0)  scale=4.37090347987e-05;
  else if(pt<1890.0)  scale=4.19829448219e-05;
  else if(pt<1900.0)  scale=4.00608987547e-05;
  else if(pt<1910.0)  scale=3.7916823203e-05;
  else if(pt<1920.0)  scale=3.65985433746e-05;
  else if(pt<1930.0)  scale=3.61280399375e-05;
  else if(pt<1940.0)  scale=3.39381913363e-05;
  else if(pt<1950.0)  scale=3.30302718794e-05;
  else if(pt<1960.0)  scale=3.28410969814e-05;
  else if(pt<1970.0)  scale=3.25522269122e-05;
  else if(pt<1980.0)  scale=3.33820098604e-05;
  else if(pt<1990.0)  scale=3.28074456775e-05;
  else if(pt<2000.0)  scale=3.47519926436e-05;
  else if(pt<2010.0)  scale=3.59692421625e-05;
  else if(pt<2020.0)  scale=3.62695100193e-05;
  else if(pt<2030.0)  scale=3.84365412174e-05;
  else if(pt<2040.0)  scale=3.79391203751e-05;
  else if(pt<2050.0)  scale=3.89106426155e-05;
  else if(pt<2060.0)  scale=3.82566904591e-05;
  else if(pt<2070.0)  scale=3.55649717676e-05;
  else if(pt<2080.0)  scale=3.73859074898e-05;
  else if(pt<2090.0)  scale=3.66192252841e-05;
  else if(pt<2100.0)  scale=3.53132345481e-05;
  else if(pt<2110.0)  scale=3.41271152138e-05;
  else if(pt<2120.0)  scale=3.34974865837e-05;
  else if(pt<2130.0)  scale=3.21029947372e-05;
  else if(pt<2140.0)  scale=3.31956725859e-05;
  else if(pt<2150.0)  scale=3.14833669108e-05;
  else if(pt<2160.0)  scale=2.90598618449e-05;
  else if(pt<2170.0)  scale=2.80254444078e-05;
  else if(pt<2180.0)  scale=2.53482503467e-05;
  else if(pt<2190.0)  scale=2.79283576674e-05;
  else if(pt<2200.0)  scale=2.44549592026e-05;
  else if(pt<2210.0)  scale=2.47466796282e-05;
  else if(pt<2220.0)  scale=2.41733105213e-05;
  else if(pt<2230.0)  scale=2.25269777729e-05;
  else if(pt<2240.0)  scale=2.07747798413e-05;
  else if(pt<2250.0)  scale=1.94206932065e-05;
  else if(pt<2260.0)  scale=2.00598897209e-05;
  else if(pt<2270.0)  scale=1.96469991351e-05;
  else if(pt<2280.0)  scale=1.73597782123e-05;
  else if(pt<2290.0)  scale=1.7457839931e-05;
  else if(pt<2300.0)  scale=1.60453419085e-05;
  else if(pt<2310.0)  scale=1.62283213285e-05;
  else if(pt<2320.0)  scale=1.61269890668e-05;
  else if(pt<2330.0)  scale=1.48107483255e-05;
  else if(pt<2340.0)  scale=1.3473436411e-05;
  else if(pt<2350.0)  scale=1.32404957185e-05;
  else if(pt<2360.0)  scale=1.25230180856e-05;
  else if(pt<2370.0)  scale=1.28127676362e-05;
  else if(pt<2380.0)  scale=1.18702737382e-05;
  else if(pt<2390.0)  scale=1.18161606224e-05;
  else if(pt<2400.0)  scale=1.05398094092e-05;
  else if(pt<2410.0)  scale=1.07210689748e-05;
  else if(pt<2420.0)  scale=9.91116758087e-06;
  else if(pt<2430.0)  scale=1.0304672287e-05;
  else if(pt<2440.0)  scale=1.04875516627e-05;
  else if(pt<2450.0)  scale=9.58452983468e-06;
  else if(pt<2460.0)  scale=9.91696106212e-06;
  else if(pt<2470.0)  scale=1.07976848085e-05;
  else if(pt<2480.0)  scale=1.05346043711e-05;
  else if(pt<2490.0)  scale=1.13886599138e-05;
  else if(pt<2500.0)  scale=1.28485171444e-05;
  return scale;
}



float Flattener :: GetWeight_Bkg(float mc_weight, float xsec, float filteff, float numevents){

  float final_weight = 1000000 * mc_weight * xsec * filteff / numevents;
//  float final_weight = mc_weight * xsec * filteff / numevents;

  return final_weight;

}

float Flattener:: AddWeightXsecFilterEffNumEvents (float xsec, float filteff, float numevents){

 float factor = 1000000 * xsec * filteff / numevents;
// float factor = xsec * filteff / numevents;
 return factor;

}


double Flattener:: calculateCombinedMass( double mCalo, double mTA, double recopt, bool Wprime){

  double caloweight = -999;
  double taweight = -999;

  double mCalo_overpt = mCalo / recopt;
  double mTA_overpt   = mTA / recopt;

  double jet_pt = recopt/1000;
  
  if (jet_pt <= 200) jet_pt = 201;
  if (jet_pt >= 3000) jet_pt = 2999;
  if ( mCalo_overpt >= 1 ) mCalo_overpt = 0.999;
  if ( mTA_overpt >= 1 ) mTA_overpt = 0.999;

  

  int bin_calo = (h2_CaloResolution_map.at(0))-> FindBin(jet_pt, mCalo_overpt);
  int bin_TA = (h2_TAresolution_map.at(0))->FindBin(jet_pt, mTA_overpt);
  int bin_corr = (h2_TAbinned_correlation_map.at(0))->FindBin(jet_pt, mTA_overpt); // Should be same as bin_TA - being pedantic

  double caloRes = -999;
  double taRes = -999;
  double corr = -999;

  if (Wprime == false){
    caloRes = (h2_CaloResolution_map.at(0))->GetBinContent(bin_calo);
    taRes = (h2_TAresolution_map.at(0))->GetBinContent(bin_TA);
    corr = (h2_TAbinned_correlation_map.at(0))->GetBinContent(bin_corr);
  }
  
  else {
    caloRes = (h2_CaloResolution_mapWprime.at(0))->GetBinContent(bin_calo);
    taRes = (h2_TAresolution_mapWprime.at(0))->GetBinContent(bin_TA);
    corr = (h2_TAbinned_correlation_mapWprime.at(0))->GetBinContent(bin_corr);
  }

  caloweight = ( taRes*taRes - corr*taRes*caloRes ) / ( caloRes*caloRes + taRes*taRes - 2*corr*caloRes*taRes );
  taweight = 1 - caloweight; 
  
  double combinedmass = (mCalo*caloweight) + (mTA*taweight);

  /*
  if (combinedmass < 0) {
    std::cout << "Negative combined mass!!!" << std::endl; 
    std::cout << "calo weight: " << caloweight << std::endl;
    std::cout << "ta weight: " << taweight << std::endl;   
    std::cout << "jet pt: " << jet_pt << std::endl;
    std::cout << "m Calo: " << mCalo << std::endl;
    std::cout << "m TA: " << mTA << std::endl;
    std::cout << "sum of weights: " << caloweight + taweight << std::endl; 
    std::cout << "mTA over pt: " << mTA_overpt << std::endl;
    std::cout << "mCalo over pt: " << mCalo_overpt << std::endl;
    std::cout << "combined mass: " << combinedmass << std::endl; 

    std::cout << "****************************************" << std::endl;
  }
  */

  return combinedmass;

}

double Flattener:: unCorrelatedcalculateCombinedMass( double mCalo, double mTA, double recopt, bool Wprime){

  double caloweight = -999;
  double taweight = -999;

  double mCalo_overpt = mCalo / recopt;
  double mTA_overpt   = mTA / recopt;

  double jet_pt = recopt/1000;

  if (jet_pt <= 200) jet_pt = 201;
  if (jet_pt >= 3000) jet_pt = 2999;
  if ( mCalo_overpt >= 1 ) mCalo_overpt = 0.999;
  if ( mTA_overpt >= 1 ) mTA_overpt = 0.999;

  int bin_calo = (h2_CaloResolution_map.at(0))-> FindBin(jet_pt, mCalo_overpt);
  int bin_TA = (h2_TAresolution_map.at(0))->FindBin(jet_pt, mTA_overpt);

  double caloRes = 0;
  double taRes = 0;

  if (Wprime == false){
    caloRes = (h2_CaloResolution_map.at(0))->GetBinContent(bin_calo);
    taRes = (h2_TAresolution_map.at(0))->GetBinContent(bin_TA);
  }

  else {
    caloRes = (h2_CaloResolution_mapWprime.at(0))->GetBinContent(bin_calo);
    taRes = (h2_TAresolution_mapWprime.at(0))->GetBinContent(bin_TA);
  }

  double Csqd = 1.0 / (caloRes * caloRes);
  double Tsqd = 1.0 / (taRes * taRes);
  caloweight = Csqd / ( Csqd + Tsqd );

  taweight = 1 - caloweight;

  double combinedmass = (mCalo*caloweight) + (mTA*taweight);

  if (combinedmass != combinedmass) {
    std::cout << "combined mass is nan" << std::endl;
    std::cout << "calo resolution: " << caloRes << std::endl;
    std::cout << "TA resolution: " << taRes << std::endl;
    std::cout << "calo weight: " << caloweight << std::endl;
    std::cout << "ta weight: " << taweight << std::endl;
    std::cout << "jet pt: " << jet_pt << std::endl;
    std::cout << "m Calo: " << mCalo << std::endl;
    std::cout << "m TA: " << mTA << std::endl;
    std::cout << "sum of weights: " << caloweight + taweight << std::endl;
    std::cout << "mTA over pt: " << mTA_overpt << std::endl;
    std::cout << "mCalo over pt: " << mCalo_overpt << std::endl;
    std::cout << "combined mass: " << combinedmass << std::endl;
    
    std::cout << "****************************************" << std::endl;
  }

  return combinedmass;

}
