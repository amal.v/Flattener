#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "SampleHandler/Sample.h"
#include <SampleHandler/MetaDataSample.h>

#include "EventLoopGrid/PrunDriver.h"

#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>
#include "NTupleFlattener/Flattener.h"

int main( int argc, char* argv[] ) {


  //===========================================
  // FOR ROOT6 WE DO NOT PUT THIS LINE
  // (ROOT6 uses Cling instead of CINT)
  // Load the libraries for all packages
  // gROOT->Macro("$ROOTCOREDIR/scripts/load_packages.C");
  // Instead on command line do:
  // > root -l '$ROOTCOREDIR/scripts/load_packages.C' 'ATestRun.cxx ("submitDir")'
  // The above works for ROOT6 and ROOT5
  //==========================================


  // Take the submit directory from the input if provided:                                                                                                  
  
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];
  //Set up the job for xAOD access:                                                                                                                         

  /*
  if( argc < 3 ){
    std::cout << "incorrect number of parameters" << std::endl;
    return 0;
  
  }
  */

  // std::string submitDir = "submitDir"; 
  std::string filename = "filename";
  std::string number = "number";

  //number = argv[ 1 ];
  //filename = argv[ 2 ];
  
  number = "W";

  std::cout << "filename: " << filename << std::endl;
  std::cout << "number: " << number << std::endl;
  

  // Set up the job for xAOD access:
  xAOD::Init().ignore();
  
            
  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:

  /*
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime3000_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime2500_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime400_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime5000_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime1750_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime2000_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime1000_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime2250_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime750_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime1500_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime2750_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime4000_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime500_tt.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime1250_tt.merge.dumperout_040417_tree.root" );
  */

  //SH::scanRucio (sh, filename);

  /*
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime1750_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime750_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime4000_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime1500_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime3000_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime1000_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime500_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime2500_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime5000_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime1250_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime2000_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime2750_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime400_tt.merge.topSD_01_3_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_zprime2250_tt.merge.topSD_01_3_tree.root" );
  */

  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m1100.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m1300.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m3600.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m1500.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m1900.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m1400.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m4000.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m2200.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m1800.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m2400.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m2300.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m3400.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m3000.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m2700.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m4600.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m2000.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m4400.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m4200.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m5000.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m4800.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m3200.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m400.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m3800.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m600.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m2600.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m2800.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m1600.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m800.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m2500.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m2100.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m1200.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m1700.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m2900.merge.dumperout_040417_tree.root" );
  SH::scanRucio (sh, "user.avaidya.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m1000.merge.dumperout_040417_tree.root" );


  // For now test with only one file:
  // Later see how to extend this to work with multiple files

  // set the name of the tree in our files
  sh.setMetaString ("nc_tree","SubstructureJetTree/nominal");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  // Don't forget to change this later!
  //job.options()->setDouble (EL::Job::optMaxEvents, 100); // for testing purposes, limit to run over the first 100 events only!

  // define an output and an ntuple associated to that output

  EL::OutputStream out ("tree");
  job.outputAdd (out);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("tree");
  job.algsAdd (ntuple);

  // add our algorithm to the job
  Flattener *alg = new Flattener;
  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (alg);
//  alg->outfilename = "tree"; // give the name of the output to our algorithm
  alg->jet_signal_flavor = number;

  EL::PrunDriver driver;
  //EL::DirectDriver driver;

  // REMEMBER TO SET SIGNAL FLAVOUR 
  driver.options()->setString("nc_outputSampleName", "user.avaidya.FlatSamples19_05.%in:name[3]%");

  // process the job using the driver
  driver.submitOnly (job, submitDir);

  return 0;

}
