# Description
This framework is used in conjunction with the [WTopTaggingDAODDumper](https://gitlab.cern.ch/JSSProjects/WTopTaggingDAODDumper) in order to produce flattened ntuples. This part of the framework is run directly on the output of the WTopTaggingDAODDumper.

Additional kinematic weights are added for pT re scaling and are calculated using the [KinematicWeightingCalculator](https://gitlab.cern.ch/avaidya/KinematicWeightingCalculator) scripts.

*NOTE* : The current version is designed to run on the output of the latest DAODDumper and may not run on older version

# Contact

Please contact either 

- Ece Akilli <ece.akilli@cern.ch>
- Amal Vaidya <amal.vaidya@cern.ch>

for any questions.

# Setup

Use the following instructions in order to setup the package locally

```
git clone https://gitlab.cern.ch/JSSProjects/WTopTaggingFlattener.git
cd WTopTaggingFlattener
source setup_env.sh
```

This will download the Shower Deconstruction package (which is by default not run in this part of the framework currently) and compile the package.


# Local Running

The latest set of intermediate ntuples (the output from the DAODDumper) can be found on eos

```
/eos/atlas/atlascerngroupdisk/perf-jets/JSS/TopBosonTagAnalysis2016/DumperOutOct31/
```

In order to run the flattener use the following script

```
Run/run_the_short_test.sh
```

where it will be necesary to change the path to the input file. 

