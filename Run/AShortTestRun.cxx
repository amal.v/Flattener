void AShortTestRun (const std::string& submitDir, const char * input_directory_name, const char * input_file_name, std::string str_jet_signal_flavor)
{
  //===========================================
  // FOR ROOT6 WE DO NOT PUT THIS LINE
  // (ROOT6 uses Cling instead of CINT)
  // Load the libraries for all packages
  // gROOT->Macro("$ROOTCOREDIR/scripts/load_packages.C");
  // Instead on command line do:
  // > root -l '$ROOTCOREDIR/scripts/load_packages.C' 'ATestRun.cxx ("submitDir")'
  // The above works for ROOT6 and ROOT5
  //==========================================
  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;
  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.
  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  // For now test with only one file:
  // Later see how to extend this to work with multiple files
  const char* inputFilePath = gSystem->ExpandPathName(input_directory_name);
  SH::ScanDir().scan(sh, inputFilePath);
  // set the name of the tree in our files
  sh.setMetaString ("nc_tree","SubstructureJetTree/nominal");
  // further sample handler configuration may go here
  // print out the samples we found
  sh.print ();
  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  // Don't forget to change this later!
  //job.options()->setDouble (EL::Job::optMaxEvents, 300); // for testing purposes, limit to run over the first 100 events only!
  // define an output and an ntuple associated to that output
  EL::OutputStream out ("tree");
  job.outputAdd (out);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("tree");
  job.algsAdd (ntuple);
  // add our algorithm to the job
  Flattener *alg = new Flattener;
  // later on we'll add some configuration options for our algorithm that go here
  job.algsAdd (alg);
  //  alg->outfilename = "tree"; // give the name of the output to our algorithm
  alg->jet_signal_flavor = str_jet_signal_flavor;
  // make the driver we want to use:
  // this one works by running the algorithm directly:
  EL::DirectDriver driver;
  // we can use other drivers to run things on the Grid, with PROOF, etc.
  // process the job using the driver
  driver.submit (job, submitDir);
}
