# Need to export home as the user's home directory on the UniGe cluster
export HOME=/atlas/users/eakilli

# Set the environment
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

cd /atlas/users/eakilli/RunFlattener/Flattener
source rcSetup.sh

cd /atlas/users/eakilli/RunFlattener/Flattener/Run
source run_jz2w.sh
